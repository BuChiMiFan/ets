<%@ page import="java.util.List" %>
<%@ page import="ets.pojo.Orders" %>
<%@ page import="java.util.Calendar" %><%--
  Created by IntelliJ IDEA.
  User: YI
  Date: 2018/5/8
  Time: 14:55
  To change this template use File | Settings | File Templates.
--%>
<% Calendar c=Calendar.getInstance();
    String time=Integer.toString(c.get(Calendar.YEAR))+"-"+Integer.toString(c.get(Calendar.MONTH)+1)+"-"+Integer.toString(c.get(Calendar.DATE))
            +" "+Integer.toString(c.get(Calendar.HOUR))+":"+Integer.toString(c.get(Calendar.MINUTE))+":"+Integer.toString(c.get(Calendar.SECOND));%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link  href="../../css/common.css" type="text/css" rel="stylesheet" />
    <link href="../../css/style.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript" src="../../js/jquery.SuperSlide.2.1.1.js"></script>
    <script type="text/javascript" src="../../js/jquery.imagezoom.min.js"></script>
    <script type="text/javascript" src="../../js/layer/layer.js"></script>
    <script src="../../js/html5shiv.js" type="text/javascript"></script>
    <script src="../../js/respond.min.js"></script>
    <script src="../../js/css3-mediaqueries.js"  type="text/javascript"></script>
    <link rel="stylesheet" href="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="http://cdn.static.runoob.com/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <title>购物车</title>
</head>

<body>
<div class="shopping_cart">
    <div class="cart_top clearfix"><a href="/jumpMainPage"><img src="../../images/logo.jpg" /></a><span class="title_name">购物车</span></div>

    <!--购物车-->

    <%List<Orders> data=(List<Orders>)session.getAttribute("SeeAllOrdersdata");%>

    <div class="cart_style Shopping_list">
        <table class="table">
            <thead>
            <tr class="label_name"><%--<th width="70"><label><input name=""  class="checkbox" type="checkbox" value="" />全选</label></th>--%><th width="430">商品</th><th width="100"></th><th width="120">数量</th><th width="100">价格（元）</th><th width="80">操作</th></tr>
            </thead>
        </table>
        <%if (data==null){}else{%>
    <%for (Orders orders:data){%>
        <table class="cart_pic table_list">
            <tr>
                <%--<td width="70" valign="top"><input name=""  class="checkbox" type="checkbox" value="" /></td>--%>
                <td width="430" valign="top">
                    <p class="img"><img src="../../<%=orders.getGoodsByGoodsid().getGoodsimg()%>" width="80px" height="80px" /></p>
                    <p class="name"><a ><%=orders.getGoodsByGoodsid().getGoodsname()%>&nbsp; <%=orders.getGoodsByGoodsid().getGoodtext()%></a></p>
                    <p class="classification">商品分类：<%=orders.getGoodsByGoodsid().getGoodstypeByGoodstypeid().getGoodstypename()%> </p>
                </td>
                <td width="100" valign="top" class="cart_price"></td>
                <td width="120" valign="top">
                    <div class="Numbers">

                        <p>1</p>

                    </div>
                </td>
                <td width="100" valign="top" class="statistics">￥<%=orders.getGoodsByGoodsid().getGoodsprice()%></td>
                <td width="80" valign="top" class="operating"><a href="/delOrders/<%=orders.getOrderid()%>">删除</a></td>
            </tr><%}%>
        </table>
 <%}%>
        <div class="Settlement clearfix">
            <div class="select-all clearfix">
               <%-- <div class="cart-checkbox"><input type="checkbox" id="CheckedAll" name="toggle-checkboxes" class="jdcheckbox" clstag="clickcart">全选</div>--%>
                <%--<div class="operation"><a href="javascript:void(0);" id="send">删除选中的商品</a></div>--%>
            </div>
            <div class="toolbar_right clearfix">
                <div class="Quantity l_f marginright">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                <%int total=0;
                    for (int i=0;i<data.size();i++){
                    total=total+Integer.parseInt(data.get(i).getGoodsByGoodsid().getGoodsprice());
                }%>
                <div class="l_f">总价：<em class="Total_price">￥<%=total%></em></div>
            </div>
<%--获取订单里的买家id和和总价格--%>
            <%if(data.size()==0){}else {%>    <button class="Submit_billing" data-toggle="modal" data-target="#myModal" onclick="getNumber('<%=data.get(0).getUserid()%>','<%=total%>')"   >去结算</button>  <%}%>
        </div>
    </div>
</div>
<div class="footer clearfix" style="margin-top: 23%">
    <div class="footer_spacing clearfix" >
        <span class="left_link l_f"><a href="/jumpMainPage">首页</a></span>
        <span class="copyright r_f">copyright©YI</span>
    </div>
</div>
<%if(data.size()==0){}else {%>
<!-- 修改模态框（Modal） START-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form id="theform" action="/OrdersPaid" method="post">
        <%int t =data.get(0).getUsersByUserid().getUserCredit()-total;%>
        <input id="uid"  name="uid" type="hidden" class="form-control" placeholder=""/>
        <input  name="sumprice" type="hidden" value="<%=total%>" class="form-control" placeholder=""/>
        <input  name="uuname" type="hidden" value="<%=data.get(0).getUsersByUserid().getUserName()%>" class="form-control" placeholder=""/>
        <input  name="jinbishengyu" type="hidden" value="<%=t%>" class="form-control" placeholder=""/>
        <input  name="bookTime" type="hidden" value="<%=time%>" class="form-control" placeholder=""/>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="myModalLabel">
                        账单
                    </h4>
                </div>
                <div class="modal-body">

                    <p>总价格：<%=total%></p>
                    <p>购买人：<%=data.get(0).getUsersByUserid().getUserName()%></p>
                    <p>金币：<%=data.get(0).getUsersByUserid().getUserCredit()%></p>
                    <p>购买后剩余：<%=t%></p>
                    <p>下单时间：<%=time%></p>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">确定支付</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal -->
    </form>
</div>
<!-- 修改模态框（Modal）END -->
<%}%>

</body>
</html>

<script type="text/javascript">
    function getNumber(id,price){
        $("#uid").val(id);
        $("#sumprice").val(price);
    }
</script>


<script>
    $(".add_cart_btn ").hover(function(){
            $(this).addClass("hover");
        },function(){
            $(this).removeClass("hover");

        }
    );
</script>
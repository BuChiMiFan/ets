<%--
  Created by IntelliJ IDEA.
  User: YI
  Date: 5/1/2018
  Time: 3:23 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>添加公告</title>
    <link rel="stylesheet" href="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="http://cdn.static.runoob.com/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
        div{
            width: 50%;
            margin: auto;
        }
        input{
            height: 50px;
        }
    </style>
</head>
<body>
    <form id="form" role="form" action="/addNotice" method="post">
        <div class="form-group">
            <label for="name">公告内容</label>
            <input type="text" class="form-control" id="name" name="addnoticecontent" value=""
                   placeholder="请输入公告内容">

        </div>
        <div id="error" class="alert alert-danger alert-dismissable" style="display: none">
            <p>不能为空</p>
        </div>
        <div>
            <button type="submit" class="btn btn-primary">提交</button>
        </div>

    </form>
</body>
</html>

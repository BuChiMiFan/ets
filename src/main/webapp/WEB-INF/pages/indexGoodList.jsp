<%@ page import="ets.pojo.Users" %><%--
  Created by IntelliJ IDEA.
  User: YI
  Date: 2018/4/24
  Time: 10:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../js/jquery.SuperSlide.2.1.1.js"></script>
    <link  href="../../css/common.css" type="text/css" rel="stylesheet" />
    <link href="../../css/style.css" type="text/css" rel="stylesheet" />
    <!--[if lt IE 9]>
    <script src="../../js/html5shiv.js" type="text/javascript"></script>
    <script src="../../js/respond.min.js"></script>
    <script src="../../js/css3-mediaqueries.js"  type="text/javascript"></script>
    <script src="../../js/jquery.js"  type="text/javascript"></script>
    <![endif]-->
    <title>首页</title>
</head>
<script type="text/javascript">
    $(document).ready(function(){
        $(".q_code ").hover(function(){
                $(this).find(".q_code_layer").addClass("hover").css("display","block");
            },function(){
                $(this).find(".q_code_layer").removeClass("hover").css("display","none");

            }
        );
        $(".diagram ").hover(function(){
                $(this).addClass("hover");
            },function(){
                $(this).removeClass("hover");

            }
        );
    })
</script>
<body>
<div class="header">
    <div class="header_top">
        <div class="top_info clearfix">
            <div class="logo_style l_f"><a href="#"><img src="../../images/logo.jpg" /></a></div>
            <div class="Search_style l_f">
                <form action="/MOHU"  method="post">
                    <div class="select">
                        <select name="" size="1">
                            <option value="1">找一找</option>
                        </select>
                    </div>
                    <input id="mohuname" name="mohuname" type="text"  required="required"  class="add_Search"/>
                    <input name="" type="submit"  value="" class="submit_Search"/>
                </form>
            </div>
            <div class="Cart_user r_f">
                <%Users users=(Users)session.getAttribute("usersInfo");%>
                <%if(users==null){%> <%}else {%>
                <div class="Cart_Quantity "><span class="number"><A href="/SeeAllOrders/<%=users.getUserId()%>">0</A></span></div>
                <%}%>
                <div class="header_operating l_f">
                    <span class="header_touxiang"><img src="../../images/touxiang_03.png" /></span>

                    <%if(users==null){%>
                    <a href='/userlogin'>登录</a><a href="/userReg">注册</a><%}else {%>
                    <a href="/cxUserInfo/<%=users.getUserId()%>"><%=users.getUserName()%></a><a href="/userlogin">登出</a>
                    <%}%>
                </div>
            </div>
        </div>
        <div class="header_menu" >
            <!--菜单导航栏-->
            <ul class="menu" id="nav">
                <li class="nLi"><a href="#">首页</a></li>

            </ul>

        </div>
    </div>
</div>
<div class="content_style clearfix">
    <!--幻灯片-->
    <div class="focus" id="focus">
        <div class="hd">
            <div class="direction">
                <a class="prev" href="javascript:void(0)"><i class="icon_z"></i></a>
                <a class="next" href="javascript:void(0)"><i class="icon_y"></i></a>
            </div>
            <span class="pageState"></span>
        </div>
        <div class="bd">
            <ul class="pic">
                <li><a href="#"><img src="images/bannaer_23.jpg" /></a></li>
                <li><a href="#"><img src="images/bannaer_1.jpg" /></a> </li>
            </ul>
            <div class="txt">
                <ul>
                    <li  class="Introduction"><a href="#"><h2>梦幻清新田园美家</h2><h5>有别于以往擅长的华丽风格，此次尝试以木皮、大理石、文化石等元素，为屋主全新打造出有休闲度假感的乡村混搭。</h5></a></li>
                    <li  class="Introduction"><a href="#"><h2>梦幻清新田园美家</h2><h5>有别于以往擅长的华丽风格，此次尝试以木皮、大理石、文化石等元素，为屋主全新打造出有休闲度假感的乡村混搭。</h5></a></li>								</ul>
            </div>
        </div>
        <script>
            jQuery(".focus").slide({ mainCell:".pic",effect:"fold", autoPlay:true,trigger:"click",startFun:function(i){
                jQuery(".focus .txt li").eq(i).animate({"bottom":0}).siblings().animate({"bottom":-80});
            }
            });
        </script>
    </div>

    <!---->

    <div class="clearfix list_Display">
        <ul  id="showgoods" class="Effect_diagram">
            <%--<li class="clearfix diagram">
                <p class="img"><a  href="#"class=""><img src="images/img_26.jpg"  width="390" height="390"/></a></p>
                <p class="name"><a href="#">现代简约设计卧室效果图</a></p>
            </li>

            --%>
        </ul>
    </div>
</div>
<div class="footer clearfix">
    <div class="footer_spacing clearfix">
        <span class="left_link l_f"><a href="#">首页</a></span>
        <span class="copyright r_f">copyright©YI</span>
    </div>
</div>
</body>
<script>
    $(function () {
        $.ajax({
            url:'showGoodsbyType',
            type:'post',
            async:true,
            dataType:'json',
            success: function (data) {
                var count=0;
                $.each(data.list,function (i,item) {
                    $("#showgoods").append("<li class=\"clearfix diagram\"><p class=\"img\"><a  href=\"#\"class=\"\"><img src=\"images/img_26.jpg\" width=\"390px\" height=\"390px\"/><p class=\"name\"><a href=\"#\">"+item.goodsname+"</a></p></li>");
                });
                $.each(data.type,function (i,item) {
                    count++;
                    if(count>=6){
                        if(count==6){
                            $("#nav").append("<li id=\"more\" class=\"nLi Down\" onmouseover=\'changeforMore(this)\' onmouseleave=\'returnforMore(this)\' style=\"margin-top: -3px\"><a href=\"#\">更多</a><em class=\"icon_jiantou\"></em>"+
                                                    "<ul id=\"one_more\"class=\"sub\">"+
                                                    "</ul>"+
                                                "</li>");
                        }
                        $("#one_more").append("<li id=\"li"+count+"\" class=\"nLi\"><a href=\"#\">"+item.goodstypename+"</a></li>");
                    }else{
                        $("#nav").append("<li id=\"li"+count+"\" class=\"nLi\" onmouseover=\'changeColor(this)\' onmouseleave=\'returnColor(this)\' style=\"margin-top: -3px\"><a href=\"#\">"+item.goodstypename+"</a></li>");
                    }
                });
            }
        });

    });
    function changeforMore(item){
        $("#one_more").slideDown();
    }
    function returnforMore(item) {
        $("#one_more").slideUp();
    }
    function changeColor(item){
        item.style.backgroundColor="#88c523";
    }
    function returnColor(item) {
        item.style.backgroundColor="#FFF";
    }
</script>
</html>
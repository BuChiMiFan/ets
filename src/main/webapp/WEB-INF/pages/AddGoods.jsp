<%@ page import="java.util.List" %>
<%@ page import="ets.pojo.Goodstype" %>
<%@ page import="ets.pojo.Users" %><%--
  Created by IntelliJ IDEA.
  User: YI
  Date: 5/14/2018
  Time: 12:26 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%List<Goodstype> data = (List<Goodstype>)session.getAttribute("goodstypes"); %>
<%Users users=(Users)session.getAttribute("usersInfo");%>
<html>
<head>
    <title>上架商品</title>
    <link rel="stylesheet" href="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="http://cdn.static.runoob.com/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
        div{
            width: 50%;
            margin: auto;
        }
        input{
            height: 50px;
        }
    </style>
</head>
<body>
<form id="form" role="form" action="/addGoods/<%=users.getUserId()%>" method="post">
    <div class="form-inline">
        <label>商品名称</label>
        <input type="text" class="form-control" id="name" name="addgoodsname" value=""
               placeholder="请输入商品名称">
    </div><br>
    <div class="form-inline">
        <label>商品类型</label>
        <select id="type" class="form-control" name="addgoodstypeid">
            <%for(int i=0;i<data.size();i++){%>
                <option value="<%=data.get(i).getGoodstypeId()%>"><%=data.get(i).getGoodstypename()%></option>
            <%}%>
        </select>
    </div><br>
    <div class="form-inline">
        <label>商品价格</label>
        <input type="text" class="form-control" id="price" name="addgoodsprice" value=""
               placeholder="请输入商品价格">
    </div><br>
    <div class="form-inline">
        <label>商品库存: 1 件（个）</label>
        <input type="hidden" class="form-control" id="number" name="addgoodsnum" value="1"
               placeholder="请输入商品库存">
    </div><br>
    <div class="form-inline">
        <label>上传图片</label>
        <input id="imgsrc" class="form-control" type="text" name="addgoodsimg">
        <a class="btn btn-info" onclick="$('input[id=src]').click();">上传</a>
        <input type="file" class="form-control" id="src"
               placeholder="" style="display:none">
    </div><br>
    <div class="form-inline">
        <label>商品说明</label>
        <textarea class="form-control" id="text" name="addgoodtext" rows="4" cols="30"
                  placeholder="请输入商品说明"></textarea>
    </div><br>
    <div id="error" class="alert alert-danger alert-dismissable" style="display: none">
        <p>不能为空</p>
    </div>
    <div>
        <button type="submit" class="btn btn-primary" style="margin-left: 25%">提交</button>
        <button type="button" class="btn btn-default" onclick="reset()">重置</button>
    </div>

</form>
</body>
<script>
    $('input[id=src]').change(function() {
        $('#imgsrc').val($(this).val());
    });
    //重置
    function reset() {
        var first='<%=data.get(0).getGoodstypeId()%>';
        $("#name").val("");
        $("#type").val(first);
        $("#price").val("");
        $("#number").val("");
        $("#imgsrc").val("");
        $("#text").val("");
    }
</script>
</html>

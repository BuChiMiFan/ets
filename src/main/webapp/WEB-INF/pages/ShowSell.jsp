<%--
  Created by IntelliJ IDEA.
  User: YI
  Date: 5/5/2018
  Time: 10:10 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>销售记录</title>
    <link rel="stylesheet" href="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="http://cdn.static.runoob.com/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="../../js/echarts.js"></script>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12 column">
                <div id="echarts" style="width: 100%;height: 500px"></div>
            </div>
        </div>
    </div>
</body>

<script>
    $(function () {
        var myChart = echarts.init(document.getElementById('echarts'));
        $.ajax({
            url:"/findAllSales",
            type:"post",
            async:true,
            dataType: "json",
            success:function (item) {
                    var option = {
                        color: ['#3398DB'],
                        tooltip : {
                            trigger: 'axis',
                            axisPointer : {            // 坐标轴指示器，坐标轴触发有效
                                type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
                            }
                        },
                        grid: {
                            left: '3%',
                            right: '4%',
                            bottom: '3%',
                            containLabel: true
                        },
                        xAxis : [
                            {
                                type : 'category',
                                data : item.types,
                                axisTick: {
                                    alignWithLabel: true
                                }
                            }
                        ],
                        yAxis : [
                            {
                                type : 'value'
                            }
                        ],
                        series : [
                            {
                                name:'销量',
                                type:'bar',
                                barWidth: '60%',
                                data:item.number
                            }
                        ]
                    };
                    myChart.setOption(option);
            },
            error:function (textStatus, errorThrown) {
                alert(errorThrown);
            }
        });
    });
</script>
</html>

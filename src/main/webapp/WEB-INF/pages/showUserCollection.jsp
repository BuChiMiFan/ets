<%@ page import="ets.pojo.Users" %>
<%@ page import="java.util.List" %>
<%@ page import="ets.pojo.Orders" %>
<%@ page import="ets.pojo.Collection" %><%--
  Created by IntelliJ IDEA.
  User: YI
  Date: 2018/5/11
  Time: 15:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%List<Collection> data =(List<Collection>)session.getAttribute("ALLCollectionByUid");%>
<html>
<head>
    <title>我的收藏</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="stylesheet" href="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="http://cdn.static.runoob.com/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style type="text/css">
        .table th, .table td {
            text-align: center;
            vertical-align: middle!important;
            font-size: smaller;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row clearfix">
        <div class="row clearfix">
            <div class="col-md-12 column">
                <table id="monitor" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>收藏ID</th>
                        <th>商品名字</th>
                        <th>卖家</th>
                        <th>售卖原因</th>
                        <th>总价</th>
                        <th>操作</th>

                    </tr>
                    </thead>
                    <tbody>
                  <%--  <%for(int i=0;i<data.size();i++){%>--%>


                    <%--------------------------------分页开始代码-----------------------------------%>
                    <%  int pages2 = 1;//当前页
                        String paget2 = request.getParameter("page");//参数页
                        if(paget2 != null){
                            pages2 = Integer.parseInt(paget2);//设置当前页为参数页
                        }
                        int pageNum2 = 5;//每页显示的数量
                        int pageCount2 = data.size()/pageNum2;
                        if(data.size()%pageNum2!=0){
                            pageCount2 = pageCount2 + 1;
                        }
                        if(pages2>pageCount2){
                            pages2=pageCount2;
                        }
                        if(pages2<1){
                            pages2=1;
                        }
                        int startNum2 = (pages2-1)*pageNum2;//页面的起始序号
                        int endNum2 = startNum2+pageNum2;//页面的结束序号
                        if(endNum2>data.size()){
                            endNum2 = data.size();
                        }
                        for(int j=startNum2;j<endNum2;j++){
                    %>
                    <tr>
                        <td class="info"><%=data.get(j).getCollectId()%></td>
                        <td class="info"><%=data.get(j).getGoodsByGoodsId().getGoodsname()%></td>
                        <td class="info"><%=data.get(j).getGoodsByGoodsId().getUsersByUserid().getUserName()%></td>
                        <td class="info"><%=data.get(j).getGoodsByGoodsId().getGoodtext()%></td>
                        <td class="info"><a>￥</a><%=data.get(j).getGoodsByGoodsId().getGoodsprice()%></td>


                        <td class="info">
                            <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal" onclick="getNumber('<%=data.get(j).getCollectId()%>')">取消收藏</button>
                        </td>
                    </tr>
                    <% }%>

                    </tbody>
                </table>
                <div style="position: absolute;top: 560px;left: 530px">
                    当前页<%=pages2 %>
                    <a  href = '/showUserCollection?page=<%=(pages2-1)%>'>上一页</a>
                    <a style="margin-left: 20px" href = '/showUserCollection?page=<%=(pages2+1)%>'>下一页</a>
                </div>
                <%----------------------------------------分页结束代码-------------------------------------------------%>

<%--
                <tr>
                    <td class="info"><%=data.get(i).getCollectId()%></td>
                    <td class="info"><%=data.get(i).getGoodsByGoodsId().getGoodsname()%></td>
                    <td class="info"><%=data.get(i).getGoodsByGoodsId().getUsersByUserid().getUserName()%></td>
                    <td class="info"><%=data.get(i).getGoodsByGoodsId().getGoodtext()%></td>
                    <td class="info"><a>￥</a><%=data.get(i).getGoodsByGoodsId().getGoodsprice()%></td>


                    <td class="info">
                        <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal" onclick="getNumber('<%=data.get(i).getCollectId()%>')">取消收藏</button>
                    </td>
                </tr>
                    <%} %>--%>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>
<!-- 修改模态框（Modal） START-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form id="theform" action="/DElcollection" method="post">
        <input id="collectionid" name="cid" type="hidden" class="form-control" placeholder=""/>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="myModalLabel">
                        提示
                    </h4>
                </div>
                <div class="modal-body">
                    确认取消吗?
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">确定</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal -->
    </form>
</div>
<!-- 修改模态框（Modal）END -->
</body>
<script type="text/javascript">
    function getNumber(id){
        $("#collectionid").val(id);
    }
</script>
</html>

<%@ page import="ets.pojo.Goods" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: YI
  Date: 5/5/2018
  Time: 8:49 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<% List<Goods> data=(List<Goods>)session.getAttribute("checkdata"); %>
<html>
<head>
    <title>审核</title>
    <link rel="stylesheet" href="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="http://cdn.static.runoob.com/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style type="text/css">
        .table th, .table td {
            text-align: center;
            vertical-align: middle!important;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row clearfix">
        <div class="row clearfix">
            <div class="col-md-12 column">
                <table id="monitor" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>序号</th>
                        <th>商品名称</th>
                        <th>商品类型</th>
                        <th>卖家</th>
                        <th>价格</th>
                        <th>商品说明</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                <%--    <%for(int i=0;i<data.size();i++){%>--%>


                    <%--------------------------------分页开始代码-----------------------------------%>
                    <%  int pages2 = 1;//当前页
                        String paget2 = request.getParameter("page");//参数页
                        if(paget2 != null){
                            pages2 = Integer.parseInt(paget2);//设置当前页为参数页
                        }
                        int pageNum2 = 5;//每页显示的数量
                        int pageCount2 = data.size()/pageNum2;
                        if(data.size()%pageNum2!=0){
                            pageCount2 = pageCount2 + 1;
                        }
                        if(pages2>pageCount2){
                            pages2=pageCount2;
                        }
                        if(pages2<1){
                            pages2=1;
                        }
                        int startNum2 = (pages2-1)*pageNum2;//页面的起始序号
                        int endNum2 = startNum2+pageNum2;//页面的结束序号
                        if(endNum2>data.size()){
                            endNum2 = data.size();
                        }
                        for(int j=startNum2;j<endNum2;j++){
                    %>


                    <tr>
                        <td class="info" id="thegid"><%=data.get(j).getGoodsId() %></td>
                        <td class="info" id="thegname"><%=data.get(j).getGoodsname() %></td>
                        <td class="info" id="theguserid" style="display: none"><%=data.get(j).getUsersByUserid().getUserId() %></td>
                        <td class="info"><%=data.get(j).getUsersByUserid().getUserName() %></td>
                        <td class="info" id="thegtypeid" style="display: none"><%=data.get(j).getGoodstypeByGoodstypeid().getGoodstypeId() %></td>
                        <td class="info"><%=data.get(j).getGoodstypeByGoodstypeid().getGoodstypename() %></td>
                        <td class="info" id="thegprice"><%=data.get(j).getGoodsprice() %></td>
                        <td class="info" id="thegtext"><%=data.get(j).getGoodtext() %></td>

                        <td class="info" id="thegimg" style="display: none"><%=data.get(j).getGoodsimg() %></td>

                        <td class="info"><%--id,name,userid,typeid,price,text,img,i--%>
                            <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal"
                                    onclick="getNumber('<%=data.get(j).getGoodsId() %>','<%=data.get(j).getGoodsname() %>',
                                            '<%=data.get(j).getUsersByUserid().getUserId() %>','<%=data.get(j).getGoodstypeByGoodstypeid().getGoodstypeId() %>'
                                            ,'<%=data.get(j).getGoodsprice() %>','<%=data.get(j).getGoodtext() %>','<%=data.get(j).getGoodsimg() %>',1)">审核通过</button>
                            <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#myModal" onclick="getNumber('<%=data.get(j).getGoodsId() %>','<%=data.get(j).getGoodsname() %>',
                                    '<%=data.get(j).getUsersByUserid().getUserId() %>','<%=data.get(j).getGoodstypeByGoodstypeid().getGoodstypeId() %>'
                                    ,'<%=data.get(j).getGoodsprice() %>','<%=data.get(j).getGoodtext() %>','<%=data.get(j).getGoodsimg() %>',4)">否决</button>
                        </td>
                    </tr>
                    <% }%>

                    </tbody>
                </table>
                <div style="position: absolute;top: 560px;left: 530px">
                    当前页<%=pages2 %>
                    <a  href = '/ShowCheck?page=<%=(pages2-1)%>'>上一页</a>
                    <a style="margin-left: 20px" href = '/ShowCheck?page=<%=(pages2+1)%>'>下一页</a>
                </div>
                <%----------------------------------------分页结束代码-------------------------------------------------%>

<%--
                <tr>
                        <td class="info" id="thegid"><%=data.get(i).getGoodsId() %></td>
                        <td class="info" id="thegname"><%=data.get(i).getGoodsname() %></td>
                        <td class="info" id="theguserid" style="display: none"><%=data.get(i).getUsersByUserid().getUserId() %></td>
                        <td class="info"><%=data.get(i).getUsersByUserid().getUserName() %></td>
                        <td class="info" id="thegtypeid" style="display: none"><%=data.get(i).getGoodstypeByGoodstypeid().getGoodstypeId() %></td>
                        <td class="info"><%=data.get(i).getGoodstypeByGoodstypeid().getGoodstypename() %></td>
                        <td class="info" id="thegprice"><%=data.get(i).getGoodsprice() %></td>
                        <td class="info" id="thegtext"><%=data.get(i).getGoodtext() %></td>

                        <td class="info" id="thegimg" style="display: none"><%=data.get(i).getGoodsimg() %></td>

                        <td class="info">&lt;%&ndash;id,name,userid,typeid,price,text,img,i&ndash;%&gt;
                            <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal"
                                    onclick="getNumber('<%=data.get(i).getGoodsId() %>','<%=data.get(i).getGoodsname() %>',
                                            '<%=data.get(i).getUsersByUserid().getUserId() %>','<%=data.get(i).getGoodstypeByGoodstypeid().getGoodstypeId() %>'
                                            ,'<%=data.get(i).getGoodsprice() %>','<%=data.get(i).getGoodtext() %>','<%=data.get(i).getGoodsimg() %>',1)">审核通过</button>
                            <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#myModal" onclick="getNumber('<%=data.get(i).getGoodsId() %>','<%=data.get(i).getGoodsname() %>',
                                    '<%=data.get(i).getUsersByUserid().getUserId() %>','<%=data.get(i).getGoodstypeByGoodstypeid().getGoodstypeId() %>'
                                    ,'<%=data.get(i).getGoodsprice() %>','<%=data.get(i).getGoodtext() %>','<%=data.get(i).getGoodsimg() %>',4)">否决</button>
                        </td>
                    </tr>
                    <%} %>--%>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>
<!-- 模态框（Modal） START-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form id="theform" action="/updateCheck" method="post">
        <input id="checkgid" name="checkgid" type="hidden" class="form-control" placeholder=""/>
        <input id="checkgname" name="checkgname" type="hidden" class="form-control" placeholder=""/>
        <input id="checkguserid" name="checkguserid" type="hidden" class="form-control" placeholder=""/>
        <input id="checkgtypeid" name="checkgtypeid" type="hidden" class="form-control" placeholder=""/>
        <input id="checkgprice" name="checkgprice" type="hidden" class="form-control" placeholder=""/>
        <input id="checkgtext" name="checkgtext" type="hidden" class="form-control" placeholder=""/>
        <input id="checkgimg" name="checkgimg" type="hidden" class="form-control" placeholder=""/>
        <input id="checkgflag" name="checkgflag" type="hidden" class="form-control" placeholder=""/>

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="myModalLabel">
                        提示
                    </h4>
                </div>
                <div class="modal-body">
                    <p>确认该操作吗?</p>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">确定</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal -->
    </form>
</div>
<!-- 模态框（Modal）END -->
</body>
<script type="text/javascript">
    function getNumber(id,name,userid,typeid,price,text,img,i){
        $("#checkgid").val(id);
        $("#checkgname").val(name);
        $("#checkguserid").val(userid);
        $("#checkgtypeid").val(typeid);
        $("#checkgprice").val(price);
        $("#checkgtext").val(text);
        $("#checkgimg").val(img);
        $("#checkgflag").val(i);
    }
</script>
</html>

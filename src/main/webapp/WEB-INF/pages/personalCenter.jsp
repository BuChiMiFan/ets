<%@ page import="ets.pojo.Users" %><%--
  Created by IntelliJ IDEA.
  User: YI
  Date: 2018/5/11
  Time: 15:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%Users users=(Users)session.getAttribute("usersInfo");%>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="" type="image/x-icon" />
    <link rel="stylesheet" href="../../css/common1.css" />
    <link rel="stylesheet" href="../../css/shopsManager1.css" />
    <script type="text/javascript" src="../../js/jquery-1.8.0.min1.js"></script>
    <script type="text/javascript" src="../../js/common1.js" ></script>
    <script type="text/javascript" src="../../js/navTop1.js"></script>
    <script type="text/javascript" src="../../js/jquery.circliful.min1.js"></script>
    <title>个人管理中心</title>
</head>

<body>
<!--[if lte IE 7]>


<!-- 会员公共头部  开始-->
<div class="shop_wrap">
    <div class="c100 nav_wrap">
        <div class="fl shop_logo">
            <a href="/jumpMainPage"><img src="../../images/logo.jpg" /></a>
        </div>
        <div class="fl nav act_nav">
            <ul>
                <li class="index-page-link hide"><a href="#" class="on">个人中心</a></li>

            </ul>
        </div>
    </div>
</div>
<!-- 会员公共头部  结束-->

<!-- 内容  开始-->
<div class="wrap">
    <div class="vip_cont c100 clearfix">
        <!--左边列表导航  开始-->
        <div class="fl vip_left vip_magLeft">
            <dl>
                <dt>个人中心</dt>
                <dd>
                    <p><a href="/jumpShowPersonalInfo" target="content">个人资料</a></p>
                    <p><a href="/jumpShowUserAllOrders" target="content">购买查询</a></p>
                    <p><a href="/jumpShowUserAllOrdersSELL" target="content">售卖查询</a></p>
                    <p><a href="/jumpShowGoods/<%=users.getUserId()%>" target="content">我的上架</a></p>
                    <p><a href="/jumpAddGoods" target="content">上架商品</a></p>
                    <p><a href="/jumpAllCollectionByUid" target="content">我的收藏</a></p>
                    <p><a href="/jumpShowGoodsCheck/<%=users.getUserId()%>" target="content">商品情况</a></p>
                </dd>
            </dl>

        </div>
        <!--左边列表导航  结束-->

        <!--右边列表内容  开始-->
        <div class="fr vip_right vip_magRight">
            <!--用户信息  开始 -->
            <iframe name="content" src="/jumpShowPersonalInfo" frameborder="0" id="mainframe" scrolling="yes" marginheight="0" marginwidth="0" width="100%" style="height: 700px;"></iframe>

        </div>
        <!--右边列表内容  结束-->
    </div>
</div>

<!-- 内容  结束-->

<!-- footer start -->
<div class="footer_new">
    <div class="footer_new_g c150">
        <div class="foot-mleft clearfix">

            <div class="footer_bottem">
                <p>Copyright © YI</p>
            </div>
        </div>
        <div class="foot-mright">
            <div class="foot_telnumber">
                客户服务热线：123 456 789
            </div>
        </div>
    </div>

</div>
<!-- footer end -->

</body>

</html>
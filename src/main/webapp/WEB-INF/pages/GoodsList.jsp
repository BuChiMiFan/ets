<%@ page import="ets.pojo.Users" %>
<%@ page import="java.util.List" %>
<%@ page import="ets.pojo.Goods" %>
<%@ page import="ets.pojo.Goodstype" %><%--
  Created by IntelliJ IDEA.
  User: YI
  Date: 2018/5/2
  Time: 11:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<%  String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link  href="<%=basePath%>../../css/common.css" type="text/css" rel="stylesheet" />
    <link href="<%=basePath%>../../css/style.css" type="text/css" rel="stylesheet" />
    <script src="<%=basePath%>../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>../../js/jquery.SuperSlide.2.1.1.js"></script>
    <script type="text/javascript" src="<%=basePath%>../../js/layer/layer.js"></script>
    <script type="text/javascript" src="<%=basePath%>../../js/common.js"></script>
    <script src="<%=basePath%>../../js/html5shiv.js" type="text/javascript"></script>
    <script src="<%=basePath%>../../js/respond.min.js"></script>
    <script src="<%=basePath%>../../js/css3-mediaqueries.js"  type="text/javascript"></script>
    <![endif]-->
    <title>商品分类</title>
    <style>
        #result{
            position:fixed;
            margin-top: 25%;
            margin-left: 45%;
            height: 52px;
            width: 115px;
            border-radius:8px;
            background-image: url("../../images/succ.png");
            background-repeat:no-repeat;
            display: none;
            z-index: 9999;
        }
    </style>
</head>
<script type="text/javascript">
    $(document).ready(function(){
        $(".q_code ").hover(function(){
                $(this).find(".q_code_layer").addClass("hover").css("display","block");
            },function(){
                $(this).find(".q_code_layer").removeClass("hover").css("display","none");

            }
        );
        $(".diagram").hover(function(){
                $(this).addClass("hover");
            },function(){
                $(this).removeClass("hover");

            }
        );
        $(".product").hover(function(){
                $(this).addClass("hover");
                $(this).find(".operating").animate({bottom: "0", }, "fast");
            },function(){
                $(this).removeClass("hover");
                $(this).find(".operating").animate({bottom: "-30",}, "fast");

            }
        );

        $('.pagination').css({
            "margin-left":($(".page_Style").width() - $('.pagination').outerWidth())/2,
        });
        $(window).resize(function(){
            $('.pagination').css({
                "margin-left":($(".page_Style").width() - $('.pagination').outerWidth())/2,
            });
        });
    });

</script>
<body>
<div id="result"></div>
<div class="Background_color">
    <div class="header">
        <div class="header_top">
            <div class="top_info clearfix">
                <div class="logo_style l_f"><a href="/jumpMainPage"><img src="<%=basePath%>../../images/logo.jpg" /></a></div>
                <div class="Search_style l_f">
                    <form   action="/MOHU"  method="post" >
                        <div class="select">
                            <select name="" size="1">
                                <option value="1">找一找</option>
                            </select>
                        </div>
                        <input id="mohuname" name="mohuname"  type="text"  required="required"   class="add_Search"/>
                        <input name="" type="submit"  value="" class="submit_Search"/>
                    </form>
                </div>
                <div class="Cart_user r_f">
                    <%Users users=(Users)session.getAttribute("usersInfo");%>

                    <%if(users==null){%> <%}else {%>
                    <div class="Cart_Quantity "><span class="number"><A href="/SeeAllOrders/<%=users.getUserId()%>">0</A></span></div>
                    <%}%>

                    <div class="header_operating l_f">
                        <span class="header_touxiang"><img src="<%=basePath%>../../images/touxiang_03.png" /></span>

                        <%if(users==null){%>
                        <a href='/userlogin'>登录</a><a href="/userReg">注册</a><%}else {%>
                        <a href="/cxUserInfo/<%=users.getUserId()%>"><%=users.getUserName()%></a><a href="/userlogin">登出</a>
                        <%}%>
                    </div>
                </div>
            </div>
            <div class="header_menu">

                <script>jQuery("#nav").slide({ type:"menu", titCell:".nLi", targetCell:".sub",effect:"slideDown",delayTime:300,triggerTime:0,returnDefault:false,trigger:"click"});</script>

            </div>
        </div>
    </div>
    <!--产品列表-->

    <div class="content_style clearfix">
        <div class="page_Style">
      <%List<Goods> data1 =(List<Goods>)session.getAttribute("mohuGoods");%>
            <!--条件筛选-->

            <div class="filter_style clearfix">
                <ul>
                    <li><label class="filter_name">商品分类</label>
                        <% List<Goodstype> goodstypeData=(List<Goodstype>)session.getAttribute("showALLGoodsType");%>
                        <div class="filter_link">
                            <%for (Goodstype goodstype: goodstypeData  ){%>  <a href="/showALLGoodsByType/<%=goodstype.getGoodstypeId()%>" ><%=goodstype.getGoodstypename()%></a><%}%>    </div>

                    </li>
                </ul>
            </div>
            <!--产品列表-->
            <div class="prodcuts_list clearfix">
                <ul class="prodcuts_style clearfix">

                  <%--  <li><%for(Goods goods : data1){%>
                    <a href=""></a>--%>

                        <%--------------------------------分页开始代码-----------------------------------%>
                            <%  int pages2 = 1;//当前页
                        String paget2 = request.getParameter("page");//参数页
                        if(paget2 != null){
                            pages2 = Integer.parseInt(paget2);//设置当前页为参数页
                        }
                        int pageNum2 = 8;//每页显示的数量
                        int pageCount2 = data1.size()/pageNum2;
                        if(data1.size()%pageNum2!=0){
                            pageCount2 = pageCount2 + 1;
                        }
                        if(pages2>pageCount2){
                            pages2=pageCount2;
                        }
                        if(pages2<1){
                            pages2=1;
                        }
                        int startNum2 = (pages2-1)*pageNum2;//页面的起始序号
                        int endNum2 = startNum2+pageNum2;//页面的结束序号
                        if(endNum2>data1.size()){
                            endNum2 = data1.size();
                        }
                        for(int j=startNum2;j<endNum2;j++){
                    %>
                    <li class="product">
                        <div class="pic_img textalign"><a href="/jumpShowGoodsDetail/<%=data1.get(j).getGoodsId()%>"><img src="<%=basePath%>../../<%=data1.get(j).getGoodsimg()%>" /></a>
                            <div class="operating"><%if(users==null){%><a>若想操作,请先登录</a><%}else {%><a style="cursor: pointer" <%--href="/CreateOrder/<%=goods.getGoodsId()%>/<%=users.getUserId()%>"--%> class="pic_cart" onclick="showResult('<%=data1.get(j).getGoodsId()%>','<%=users.getUserId()%>')">加入购物车</a><a href="/collection/<%=data1.get(j).getGoodsId()%>/<%=users.getUserId()%>" class="Collection">收藏</a><%}%></div>
                        </div>
                        &nbsp;  &nbsp;<a href=""><%=data1.get(j).getGoodsname()%></a>
                        <p class="pic_nme"><a><%=data1.get(j).getGoodtext()%></a></p>
                        <p class="pic_price">￥<%=data1.get(j).getGoodsprice()%></p>
                    </li>
                            <% }%>

                        </tbody>
                        </table>
                       <%-- <div style="position: absolute;top: 560px;left: 530px">
                            当前页<%=pages2 %>
                            <a  href = '/GoodsList?page=<%=(pages2-1)%>'>上一页</a>
                            <a style="margin-left: 20px" href = '/GoodsList?page=<%=(pages2+1)%>'>下一页</a>
                        </div>--%>
                        <%----------------------------------------分页结束代码-------------------------------------------------%>

                  <%--  <li class="product">
                        <div class="pic_img textalign"><a href="/jumpShowGoodsDetail/<%=goods.getGoodsId()%>"><img src="<%=basePath%>../../<%=goods.getGoodsimg()%>" /></a>
                            <div class="operating"><%if(users==null){%><a>若想操作,请先登录</a><%}else {%><a style="cursor: pointer" &lt;%&ndash;href="/CreateOrder/<%=goods.getGoodsId()%>/<%=users.getUserId()%>"&ndash;%&gt; class="pic_cart" onclick="showResult('<%=goods.getGoodsId()%>','<%=users.getUserId()%>')">加入购物车</a><a href="/collection/<%=goods.getGoodsId()%>/<%=users.getUserId()%>" class="Collection">收藏</a><%}%></div>
                        </div>
                        &nbsp;  &nbsp;<a href=""><%=goods.getGoodsname()%></a>
                        <p class="pic_nme"><a><%=goods.getGoodtext()%></a></p>
                        <p class="pic_price">￥<%=goods.getGoodsprice()%></p>
                    </li> <%}%>--%>
                </ul>
                <div class="pic_page_style clearfix">
                    <ul class="page_example pagination">
                        <li><li class="first" data-page="1"><a href="/GoodsList?page=<%=(pages2-1)%>"> 〈 上一页 </a></li></li>
                        <li class="last" data-page="35"><a href="/GoodsList?page=<%=(pages2+1)%>">下一页 〉</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="footer clearfix">
    <div class="footer_spacing clearfix">
        <span class="left_link l_f"><a href="/jumpMainPage">首页</a></span>
        <span class="copyright r_f">copyright©YI</span>
    </div>
</div>
<script>
    function showResult(goodid,userid) {
        $.ajax({
            url:'/CreateOrder/'+goodid+'/'+userid,
            type:'post',
            async:true,
            dataType:'json',
            success:function (data) {
                if(data.res=="succ"){
                    $("#result").css("display","block");
                    $("#result").fadeOut(3000);
                }
                if(data.res=="repeated"){
                    $("#result").css("background-image","url(../../images/failed.png)")
                    $("#result").css("display","block");
                    $("#result").fadeOut(3000);
                }
            }
        });
    }
</script>
</body>
</html>
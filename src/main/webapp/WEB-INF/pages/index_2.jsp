<%@ page import="ets.pojo.Users" %>
<%@ page import="ets.pojo.Goodstype" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: YI
  Date: 2018/4/24
  Time: 10:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%List<Goodstype> data=(List<Goodstype>)session.getAttribute("showGoodsType");
    if(data!=null){
        session.removeAttribute("showGoodsType");
    }%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../js/jquery.SuperSlide.2.1.1.js"></script>
    <link href="../../css/common.css" type="text/css" rel="stylesheet" />
    <link href="../../css/style.css" type="text/css" rel="stylesheet" />
    <!--[if lt IE 9]>
    <script src="../../js/html5shiv.js" type="text/javascript"></script>
    <script src="../../js/respond.min.js"></script>
    <script src="../../js/css3-mediaqueries.js"  type="text/javascript"></script>
    <script src="../../js/jquery.js"  type="text/javascript"></script>
    <![endif]-->
    <title>首页</title>
</head>
<script type="text/javascript">
    $(document).ready(function(){
        $(".q_code ").hover(function(){
                $(this).find(".q_code_layer").addClass("hover").css("display","block");
            },function(){
                $(this).find(".q_code_layer").removeClass("hover").css("display","none");

            }
        );
        $(".diagram ").hover(function(){
                $(this).addClass("hover");
            },function(){
                $(this).removeClass("hover");

            }
        );
    })
</script>
<body>
<jsp:include page="header.jsp"></jsp:include>
<div class="content_style clearfix">
    <!--幻灯片-->
    <div class="focus" id="focus">
        <div class="hd">
            <div class="direction">
                <a class="prev" href="javascript:void(0)"><i class="icon_z"></i></a>
                <a class="next" href="javascript:void(0)"><i class="icon_y"></i></a>
            </div>
            <span class="pageState"></span>
        </div>
        <div class="bd">
            <ul id="adverts" class="pic">
                <li><a href="#"><img id="pic1"src="../../images/bannaer_23.jpg" /></a></li>
                <li><a href="#"><img id="pic2" src="../../images/bannaer_1.jpg" /></a> </li>
            </ul>
            <div class="txt">
                <ul id="words">
                   <%-- <li  class="Introduction"><a href="#"><h2>梦幻清新田园美家</h2><h5>有别于以往擅长的华丽风格，此次尝试以木皮、大理石、文化石等元素，为屋主全新打造出有休闲度假感的乡村混搭。</h5></a></li>
                    <li  class="Introduction"><a href="#"><h2>梦幻清新田园美家</h2><h5>有别于以往擅长的华丽风格，此次尝试以木皮、大理石、文化石等元素，为屋主全新打造出有休闲度假感的乡村混搭。</h5></a></li>--%>								</ul>
            </div>
        </div>
        <script>
            jQuery(".focus").slide({ mainCell:".pic",effect:"fold", autoPlay:true,trigger:"click",startFun:function(i){
                jQuery(".focus .txt li").eq(i).animate({"bottom":0}).siblings().animate({"bottom":-80});
            }
            });
        </script>
    </div>

    <!---->

    <div class="clearfix list_Display">
        <ul  id="showgoods" class="Effect_diagram">
            <%--<li class="clearfix diagram">
                <p class="img"><a  href="#"class=""><img src="images/img_26.jpg"  width="390" height="390"/></a></p>
                <p class="name"><a href="#">现代简约设计卧室效果图</a></p>
            </li>
            --%>
        </ul>
        <button id="prePage">上一页</button>
        <button id="nextPage">下一页</button>
    </div>
</div>
<div class="footer clearfix">
    <div class="footer_spacing clearfix">
        <span class="left_link l_f"><a href="#">首页</a></span>
        <span class="copyright r_f">copyright©YI</span>
    </div>
</div>
</body>
<script>
    var startIndex=0;//起始查询索引
    var page=1;//每页显示个数
    var total;//总条数
    var pageNum;//总页数

    $("#prePage").click(function () {
        if(startIndex!=0){
            startIndex=startIndex-page;
            showAllGood(startIndex,page);
        }else{
            showAllGood(startIndex,page);
        }
    });
    $("#nextPage").click(function () {
        if(startIndex<=total-startIndex){
            startIndex=startIndex+page;
        }else{
            startIndex=total-startIndex;
        }
        showAllGood(startIndex,page);
    });
    $(function () {
        showAllGood(startIndex,page);
    });
    function showAllGood(start,nuumber){
        $.ajax({
            url:'showGoodsbyType',
            type:'post',
            data:{
                "startIndex":start,
                "page":nuumber
            },
            async:true,
            dataType:'json',
            success: function (data) {
                var count=0;
                total=data.total;
                if(total%page==0){
                    pageNum=total/page;
                }else{
                    pageNum=Math.ceil(total/page)+1;//取整
                }
                $.each(data.list,function (i,item) {
                    $("#showgoods").append("<li class=\"clearfix diagram\"><p class=\"img\"><a  href=\"/jumpShowGoodsDetail/"+item.goodsId+"\"class=\"\"><img src=\"../../"+item.goodsimg+"\" width=\"390px\" height=\"390px\"/><p class=\"name\"><a href=\"/jumpShowGoodsDetail/"+item.goodsId+"\">"+item.goodsname+"</a></p></li>");
                });
                $.each(data.advert,function (i,item) {
                    $("#pic"+i).attr("src",""+item.adimg+"");
                    /*$("#adverts").append("<li><a href=\"#\"><img src=\"../../"+item.adimg+"\" /></a></li>");*/
                    $("#words").append("<li class=\"Introduction\"><a href=\"#\"><h2>梦幻清新田园美家</h2><h5>有别于以往擅长的华丽风格，此次尝试以木皮、大理石、文化石等元素，为屋主全新打造出有休闲度假感的乡村混搭。</h5></a></li>");
                });
                $.each(data.type,function (i,item) {
                    count++;
                    if(count>=6){
                        if(count==6){
                            $("#nav").append("<li id=\"more\" class=\"nLi Down\" onmouseover=\'changeforMore(this)\' onmouseleave=\'returnforMore(this)\' style=\"margin-top: -3px\"><a href=\"#\">更多</a><em class=\"icon_jiantou\"></em>"+
                                "<ul id=\"one_more\"class=\"sub\">"+
                                "</ul>"+
                                "</li>");
                        }
                        $("#one_more").append("<li id=\"li"+count+"\" class=\"nLi\"><a href=\"/showALLGoodsByType/"+item.goodstypeId+"\">"+item.goodstypename+"</a></li>");
                    }else{
                        $("#nav").append("<li id=\"li"+count+"\" class=\"nLi\" onmouseover=\'changeColor(this)\' onmouseleave=\'returnColor(this)\' style=\"margin-top: -3px\"><a href=\"/showALLGoodsByType/"+item.goodstypeId+"\">"+item.goodstypename+"</a></li>");
                    }
                });
            }
        });
    }
</script>
</html>
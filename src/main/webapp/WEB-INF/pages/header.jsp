<%@ page import="ets.pojo.Users" %>
<%@ page import="ets.pojo.Goodstype" %>
<%@ page import="java.util.List" %>
<%--
  Created by IntelliJ IDEA.
  User: YI
  Date: 5/14/2018
  Time: 3:15 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%List<Goodstype> data=(List<Goodstype>)session.getAttribute("showGoodsType");%>
<html>
<head>
    <title>header</title>
    <link href="../../css/common.css" type="text/css" rel="stylesheet" />
    <link href="../../css/style.css" type="text/css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../js/jquery.SuperSlide.2.1.1.js"></script>
    <script type="text/javascript" src="../../js/jquery.imagezoom.min.js"></script>
    <script type="text/javascript" src="../../js/layer/layer.js"></script>
</head>
<body>
<div class="header">
    <div class="header_top">
        <div class="top_info clearfix">
            <div class="logo_style l_f"><a href="/jumpMainPage"><img src="../../images/logo.jpg" /></a></div>
            <div class="Search_style l_f">
                <form action="/MOHU"  method="post">
                    <div class="select">
                        <select name="" size="1">
                            <option value="1">找一找</option>
                        </select>
                    </div>
                    <input id="mohuname" name="mohuname" type="text"  required="required"  class="add_Search"/>
                    <input name="" type="submit"  value="" class="submit_Search"/>
                </form>
            </div>
            <div class="Cart_user r_f">
                <%Users users=(Users)session.getAttribute("usersInfo");%>
                <%if(users==null){%> <%}else {%>
                <div class="Cart_Quantity "><span class="number"><a href="/SeeAllOrders/<%=users.getUserId()%>">0</a></span></div>
                <%}%>
                <div class="header_operating l_f">
                    <span class="header_touxiang"><img src="../../images/touxiang_03.png" /></span>
                    <%if(users==null){%>
                    <a href='/userlogin'>登录</a><a href="/userReg">注册</a><%}else {%>
                    <a href="/cxUserInfo/<%=users.getUserId()%>"><%=users.getUserName()%></a><a href="/userlogin">登出</a>
                    <%}%>
                </div>
            </div>
        </div>
        <div class="header_menu" >
            <!--菜单导航栏-->
            <ul class="menu" id="nav">
                <li class="nLi" onmouseover='changeforMore(this)' onmouseleave='returnforMore(this)'><a href="/jumpMainPage">首页</a></li>
                <%if(data!=null){
                    for(int i=0;i<data.size();i++){
                        if(data.size()>=6){%>
                            <%if(i<5){%>
                                <li class="nLi" onmouseover='changeColor(this)' onmouseleave='returnColor(this)' style="margin-top: -3px"><a href="/showALLGoodsByType/<%=data.get(i).getGoodstypeId()%>"><%=data.get(i).getGoodstypename()%></a></li>
                            <%}%>
                            <%if(i==5){%>
                                <li id="more" class="nLi Down" onmouseover='changeforMore(this)' onmouseleave='returnforMore(this)' style="margin-top: -3px"><a href="#">更多</a><em class="icon_jiantou"></em>
                                    <ul id="one_more"class="sub">
                                        <%for(int j=5;j<data.size();j++){%>
                                            <li class="nLi"><a href="/showALLGoodsByType/<%=data.get(j).getGoodstypeId()%>"><%=data.get(j).getGoodstypename()%></a></li>
                                        <%}%>
                                    </ul>
                                </li>
                            <%}%>
                        <%}else{%>
                            <li class="nLi" onmouseover='changeColor(this)' onmouseleave='returnColor(this)' style="margin-top: -3px"><a href="#"><%=data.get(i).getGoodstypename()%></a></li>
                        <%}
                    }
                }%>
            </ul>
        </div>
    </div>
</div>
</body>
<script>
    function changeforMore(item){
        $("#one_more").slideDown();
    }
    function returnforMore(item) {
        $("#one_more").slideUp();
    }
    function changeColor(item){
        item.style.backgroundColor="#88c523";
    }
    function returnColor(item) {
        item.style.backgroundColor="#FFF";
    }
</script>
</html>

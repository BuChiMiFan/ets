<%@ page import="ets.pojo.Users" %>
<%@ page import="java.util.List" %>
<%@ page import="ets.pojo.Orders" %><%--
  Created by IntelliJ IDEA.
  User: YI
  Date: 2018/5/11
  Time: 15:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%List<Orders> data =(List<Orders>)session.getAttribute("OrdersInfoByUID");%>
<%Users users=(Users)session.getAttribute("usersInfo");%>
<html>
<head>
    <title>买家订单</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="stylesheet" href="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="http://cdn.static.runoob.com/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style type="text/css">
        .table th, .table td {
            text-align: center;
            vertical-align: middle!important;
            font-size: smaller;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row clearfix">
        <div class="row clearfix">
            <div class="col-md-12 column">
                <table id="monitor" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>订单号</th>
                        <th>商品名</th>
                        <th>卖家</th>
                        <th>买家</th>
                        <th>总价</th>
                        <th>下单时间</th>
                        <th>状态</th>
                        <th>订单状态</th>
                    </tr>
                    </thead>
                    <tbody>
                    <%--<%for(int i=0;i<data.size();i++){%>--%>


                    <%--------------------------------分页开始代码-----------------------------------%>
                    <%  int pages2 = 1;//当前页
                        String paget2 = request.getParameter("page");//参数页
                        if(paget2 != null){
                            pages2 = Integer.parseInt(paget2);//设置当前页为参数页
                        }
                        int pageNum2 = 5;//每页显示的数量
                        int pageCount2 = data.size()/pageNum2;
                        if(data.size()%pageNum2!=0){
                            pageCount2 = pageCount2 + 1;
                        }
                        if(pages2>pageCount2){
                            pages2=pageCount2;
                        }
                        if(pages2<1){
                            pages2=1;
                        }
                        int startNum2 = (pages2-1)*pageNum2;//页面的起始序号
                        int endNum2 = startNum2+pageNum2;//页面的结束序号
                        if(endNum2>data.size()){
                            endNum2 = data.size();
                        }
                        for(int j=startNum2;j<endNum2;j++){
                    %>
                    <tr>
                        <td class="info"><%=data.get(j).getOrderid()%></td>
                        <td class="info"><%=data.get(j).getGoodsByGoodsid().getGoodsname()%></td>
                        <td class="info"><%=data.get(j).getGoodsByGoodsid().getUsersByUserid().getUserName()%></td>
                        <td class="info"><%=data.get(j).getUsersByUserid().getUserName()%></td>
                        <td class="info"><%=data.get(j).getOrderSuMprice()%></td>
                        <td class="info"><%=data.get(j).getOrderTime()%></td>
                        <td class="info">
                            <%if(data.get(j).getOrderFlag()==0){%>
                            未付款
                            <%}else if(data.get(j).getOrderFlag()==1){%>
                            已付款
                            <%}else if(data.get(j).getOrderFlag()==2){%>
                            已发货
                            <%}else if(data.get(j).getOrderFlag()==3){%>
                            已到货
                            <%}%>
                        </td>
                        <td class="info">
                            <%if(data.get(j).getOrderFlag()!=3){%>
                            <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal" onclick="getNumber('<%=data.get(j).getOrderid()%>')">到货确认</button>
                            <%}else{%>
                            <span>已完成订单</span>
                            <%}%>
                        </td>
                    </tr>
                    <% }%>

                    </tbody>
                </table>
                <div style="position: absolute;top: 560px;left: 530px">
                    当前页<%=pages2 %>
                    <a  href = '/showUserAllOrders?page=<%=(pages2-1)%>'>上一页</a>
                    <a style="margin-left: 20px" href = '/showUserAllOrders?page=<%=(pages2+1)%>'>下一页</a>
                </div>
                <%----------------------------------------分页结束代码-------------------------------------------------%>


                <%--<tr>--%>
                        <%--<td class="info"><%=data.get(i).getOrderid()%></td>--%>
                        <%--<td class="info"><%=data.get(i).getGoodsByGoodsid().getGoodsname()%></td>--%>
                        <%--<td class="info"><%=data.get(i).getGoodsByGoodsid().getUsersByUserid().getUserName()%></td>--%>
                        <%--<td class="info"><%=data.get(i).getUsersByUserid().getUserName()%></td>--%>
                        <%--<td class="info"><%=data.get(i).getOrderSuMprice()%></td>--%>
                        <%--<td class="info"><%=data.get(i).getOrderTime()%></td>--%>
                        <%--<td class="info">--%>
                            <%--<%if(data.get(i).getOrderFlag()==0){%>--%>
                                <%--未付款--%>
                            <%--<%}else if(data.get(i).getOrderFlag()==1){%>--%>
                                <%--已付款--%>
                            <%--<%}else if(data.get(i).getOrderFlag()==2){%>--%>
                                <%--已发货--%>
                            <%--<%}else if(data.get(i).getOrderFlag()==3){%>--%>
                                <%--已到货--%>
                            <%--<%}%>--%>
                        <%--</td>--%>
                        <%--<td class="info">--%>
                            <%--<%if(data.get(i).getOrderFlag()!=3){%>--%>
                                <%--<button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal" onclick="getNumber('<%=data.get(i).getOrderid()%>')">到货确认</button>--%>
                            <%--<%}else{%>--%>
                                <%--<span>已完成订单</span>--%>
                            <%--<%}%>--%>
                        <%--</td>--%>
                    <%--</tr>--%>
                    <%--<%} %>--%>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>
<!-- 修改模态框（Modal） START-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form id="theform" action="/arrivedConfirm/<%=users.getUserId()%>" method="post">
        <input id="ordersid" name="updatearrivedConfirmOrderId" type="hidden" class="form-control" placeholder=""/>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="myModalLabel">
                        提示
                    </h4>
                </div>
                <div class="modal-body">
                    确认到货吗?
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">确定</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal -->
    </form>
</div>
<!-- 修改模态框（Modal）END -->
</body>
<script type="text/javascript">
    function getNumber(id){
        $("#ordersid").val(id);
    }
</script>
</html>

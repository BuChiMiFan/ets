<%--
  Created by IntelliJ IDEA.
  User: YI
  Date: 2018/4/25
  Time: 14:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link  href="../../css/common.css" type="text/css" rel="stylesheet" />
    <link href="../../css/style.css" type="text/css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../js/jquery.SuperSlide.2.1.1.js"></script>
    <script type="text/javascript" src="../../js/layer/layer.js"></script>
    <script type="text/javascript" src="../../js/common.js"></script>
    <title>个人注册详细</title>
</head>
<!--[if lt IE 9]>
<script src="../../js/html5shiv.js" type="text/javascript"></script>
<script src="../../js/respond.min.js"></script>
<script src="../../js/css3-mediaqueries.js"  type="text/javascript"></script>
<![endif]-->
<body>
<div class="Reg_header">
    <div class="login_top">
        <div class="content_style">
            <a href="/jumpMainPage"><img src="images/logo.jpg"  /></a><span class="title">个人注册详细</span></div>
    </div>
</div>
<div class="clearfix content_style">
    <div class=" clearfix content_style">
        <div class="Registration_details ">
            <div class="prompt_style">
                <span>注：</span>为保证审核及时通过，请尽量完善信息内容。请乎填写虚假信息，所填信息将进行审核。所有信息未必填项，填写完整方能提交审核。否则无法提交审核，请配合完善信息  </div>
            <div class="details_content clearfix margin_sx">
                    <div class="label_info">
                        <span class="title_name">个人基本信息</span>

                        <form id="" method="post" action="/addUsers">

                                <ul style="margin-left:38%"class="details_info">
                                    <li> <A>用户账户</A> <input name="account" type="text"  class="text_add"   required="required" style="width:250px;"/><em style="color: #ffa200">&nbsp;登录网站用</em></li>
                                    <li> <a>用户密码</a> <input name="pwd" type="password"  class="text_add"   required="required" style="width:250px;"/></li>
                                    <li> <a>用户性别</a>
                                        <input type="radio" name="sex" value="保密" checked> 保密
                                        <input type="radio" name="sex" value="男">男
                                         <input type="radio" name="sex" value="女">女</li>
                                    <li> <a>用户学号</a> <input name="xuehao" type="number"  class="text_add"  style="width:250px;"/></li>

                            </ul>

                            <HR style="FILTER: alpha(opacity=100,finishopacity=0,style=3)" width="100%" color=#987cb9 SIZE=3>

                            <ul style="margin-left:38%"class="details_info">
                         <li>  <a>真实姓名</a> <input name="truename" type="text"  class="text_add"  style="width:250px;"/></li>
                         <li>  <a>用户Q Q</a> <input name="qq" type="number"  class="text_add"  style="width:250px;"/></li>
                         <li>  <a>用户班级</a> <input name="clazz" type="text"  class="text_add"  style="width:250px;"/></li>
                         <li>  <a>用户邮箱</a> <input name="email" type="text"  class="text_add"  style="width:250px;"/></li>
                         <li>  <a>用户地址</a> <input name="address" type="text"  class="text_add"  style="width:250px;"/></li>
                            </ul>
                            <HR style="FILTER: alpha(opacity=100,finishopacity=0,style=3)" width="100%" color=#987cb9 SIZE=3>

                            <div class="Submit_operation textalign">
                                <input name="" type="submit"  class="btn submit_btn" value="提交注册信息" />
                            </div>
                        </form>


                </center>
            </div>
        </div>
        <!--按钮操作-->

    </div>
</div>
<div class="footer clearfix re_footer">
    <div class="footer_spacing clearfix">
        <span class="left_link l_f"><a href="/jumpMainPage">首页</a></span>
        <span class="copyright r_f">copyright©YI</span>
    </div>
</div>
</body>
</html>

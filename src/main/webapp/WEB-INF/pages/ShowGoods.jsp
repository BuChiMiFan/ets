<%@ page import="ets.pojo.Goods" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: YI
  Date: 5/14/2018
  Time: 12:25 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%List<Goods> data=(List<Goods>)session.getAttribute("allgoods");%>
<html>
<head>
    <title>查看所有审核通过的商品</title>
    <link rel="stylesheet" href="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="http://cdn.static.runoob.com/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style type="text/css">
        .table th, .table td {
            text-align: center;
            vertical-align: middle!important;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row clearfix">
        <div class="row clearfix">
            <div class="col-md-12 column">
                <table id="monitor" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>序号</th>
                        <th>商品名称</th>
                        <th>商品类型</th>
                        <th>价格</th>
                        <th>库存</th>
                        <th>说明</th>
                        <th>上架时间</th>
                    </tr>
                    </thead>
                    <tbody>
                  <%--  <%for(int i=0;i<data.size();i++){%>--%>


                    <%--------------------------------分页开始代码-----------------------------------%>
                    <%  int pages2 = 1;//当前页
                        String paget2 = request.getParameter("page");//参数页
                        if(paget2 != null){
                            pages2 = Integer.parseInt(paget2);//设置当前页为参数页
                        }
                        int pageNum2 = 5;//每页显示的数量
                        int pageCount2 = data.size()/pageNum2;
                        if(data.size()%pageNum2!=0){
                            pageCount2 = pageCount2 + 1;
                        }
                        if(pages2>pageCount2){
                            pages2=pageCount2;
                        }
                        if(pages2<1){
                            pages2=1;
                        }
                        int startNum2 = (pages2-1)*pageNum2;//页面的起始序号
                        int endNum2 = startNum2+pageNum2;//页面的结束序号
                        if(endNum2>data.size()){
                            endNum2 = data.size();
                        }
                        for(int j=startNum2;j<endNum2;j++){
                    %>

                    <tr>
                        <td class="info"><%=data.get(j).getGoodsId() %></td>
                        <td class="info"><%=data.get(j).getGoodsname() %></td>
                        <td class="info"><%=data.get(j).getGoodstypeByGoodstypeid().getGoodstypename() %></td>
                        <td class="info"><%=data.get(j).getGoodsprice() %></td>
                        <td class="info"><%=data.get(j).getGoodsnum() %></td>
                        <td class="info"><%=data.get(j).getGoodtext() %></td>
                        <td class="info"><%=data.get(j).getGoodsListingTime() %></td>
                        <%-- <td class="info"><img style="height: 50px;width: 200px" id="ImageSrc" src="<%=data.get(i).getAdimg() %>"></td>--%>
                    </tr>
                    <% }%>

                    </tbody>
                </table>
                <div style="position: absolute;top: 560px;left: 530px">
                    当前页<%=pages2 %>
                    <a  href = '/ShowGoods?page=<%=(pages2-1)%>'>上一页</a>
                    <a style="margin-left: 20px" href = '/ShowGoods?page=<%=(pages2+1)%>'>下一页</a>
                </div>
                <%----------------------------------------分页结束代码-------------------------------------------------%>


               <%-- <tr>
                        <td class="info"><%=data.get(i).getGoodsId() %></td>
                        <td class="info"><%=data.get(i).getGoodsname() %></td>
                        <td class="info"><%=data.get(i).getGoodstypeByGoodstypeid().getGoodstypename() %></td>
                        <td class="info"><%=data.get(i).getGoodsprice() %></td>
                        <td class="info"><%=data.get(i).getGoodsnum() %></td>
                        <td class="info"><%=data.get(i).getGoodtext() %></td>
                        <td class="info"><%=data.get(i).getGoodsListingTime() %></td>
                       &lt;%&ndash; <td class="info"><img style="height: 50px;width: 200px" id="ImageSrc" src="<%=data.get(i).getAdimg() %>"></td>&ndash;%&gt;
                    </tr>
                    <%} %>--%>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>
</body>
</body>
</html>

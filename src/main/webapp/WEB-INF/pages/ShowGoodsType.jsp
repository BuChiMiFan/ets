<%@ page import="java.util.List" %>
<%@ page import="ets.pojo.Goodstype" %><%--
  Created by IntelliJ IDEA.
  User: YI
  Date: 5/1/2018
  Time: 12:33 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<% List<Goodstype> data=(List<Goodstype>)session.getAttribute("gsdata"); %>
<html>
<head>
    <title>修改|删除</title>
    <link rel="stylesheet" href="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="http://cdn.static.runoob.com/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style type="text/css">
        .table th, .table td {
            text-align: center;
            vertical-align: middle!important;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row clearfix">
        <div class="row clearfix">
            <div class="col-md-12 column">
                <table id="monitor" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>序号</th>
                        <th>类别名称</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                <%--    <%for(int i=0;i<data.size();i++){%>--%>

                    <%--------------------------------分页开始代码-----------------------------------%>
                    <%  int pages2 = 1;//当前页
                        String paget2 = request.getParameter("page");//参数页
                        if(paget2 != null){
                            pages2 = Integer.parseInt(paget2);//设置当前页为参数页
                        }
                        int pageNum2 = 5;//每页显示的数量
                        int pageCount2 = data.size()/pageNum2;
                        if(data.size()%pageNum2!=0){
                            pageCount2 = pageCount2 + 1;
                        }
                        if(pages2>pageCount2){
                            pages2=pageCount2;
                        }
                        if(pages2<1){
                            pages2=1;
                        }
                        int startNum2 = (pages2-1)*pageNum2;//页面的起始序号
                        int endNum2 = startNum2+pageNum2;//页面的结束序号
                        if(endNum2>data.size()){
                            endNum2 = data.size();
                        }
                        for(int j=startNum2;j<endNum2;j++){
                    %>
                    <tr>
                        <td class="info" id="thegtid"><%=data.get(j).getGoodstypeId() %></td>
                        <td class="info" id="thegtname"><%=data.get(j).getGoodstypename() %></td>
                        <td class="info">
                            <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal" onclick="getNumber('<%=data.get(j).getGoodstypeId() %>','<%=data.get(j).getGoodstypename() %>')">修改</button>
                            <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#myModal2" onclick="getdelNumber('<%=data.get(j).getGoodstypeId() %>','<%=data.get(j).getGoodstypename() %>')">删除</button>
                        </td>
                    </tr>
                    <% }%>

                    </tbody>
                </table>
                <div style="position: absolute;top: 560px;left: 530px">
                    当前页<%=pages2 %>
                    <a  href = '/ShowGoodsType?page=<%=(pages2-1)%>'>上一页</a>
                    <a style="margin-left: 20px" href = '/ShowGoodsType?page=<%=(pages2+1)%>'>下一页</a>
                </div>
                <%----------------------------------------分页结束代码-------------------------------------------------%>


                 <%--   <tr>
                        <td class="info" id="thegtid"><%=data.get(i).getGoodstypeId() %></td>
                        <td class="info" id="thegtname"><%=data.get(i).getGoodstypename() %></td>
                        <td class="info">
                            <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal" onclick="getNumber('<%=data.get(i).getGoodstypeId() %>','<%=data.get(i).getGoodstypename() %>')">修改</button>
                            <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#myModal2" onclick="getdelNumber('<%=data.get(i).getGoodstypeId() %>','<%=data.get(i).getGoodstypename() %>')">删除</button>
                        </td>
                    </tr>
                    <%} %>--%>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>
<!-- 同意模态框（Modal） START-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form id="theform" action="/updateGoodsType" method="post">
        <input id="gtid" name="updategtid" type="hidden" class="form-control" placeholder=""/>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="myModalLabel">
                        提示
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>类别名称</label>
                        <input type="text" class="form-control" id="gtname" name="updategtname" value=""
                               placeholder="请输入名称">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">确定</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal -->
    </form>
</div>
<!-- 同意模态框（Modal）END -->
<!-- 否决模态框（Modal） START-->
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form action="/delGoodsType" method="post">
        <input id="delgtid" name="delgtid" type="text" class="form-control" placeholder=""/>
        <input id="delgtname" name="delgtname" type="text" class="form-control" placeholder=""/>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="myModalLabe2">
                        提示
                    </h4>
                </div>
                <div class="modal-body">
                    <p>确认该操作吗?</p>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">确定</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal -->
    </form>
</div>
<!-- 否决模态框（Modal）END -->
</body>
<script type="text/javascript">
    function getNumber(id,name){
        $("#gtid").val(id);
        $("#gtname").val(name);
    }
    function getdelNumber(id,name){
        $("#delgtid").val(id);
        $("#delgtname").val(name);
    }
</script>
</html>

<%@ page import="java.util.List" %>
<%@ page import="ets.pojo.Goods" %><%--
  Created by IntelliJ IDEA.
  User: YI
  Date: 5/14/2018
  Time: 12:26 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%List<Goods> data=(List<Goods>)session.getAttribute("allgoodscheck");%>
<html>
<head>
    <title>查看所有自己的商品</title>
    <link rel="stylesheet" href="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="http://cdn.static.runoob.com/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style type="text/css">
        .table th, .table td {
            text-align: center;
            vertical-align: middle!important;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row clearfix">
        <div class="row clearfix">
            <div class="col-md-12 column">
                <table id="monitor" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>序号</th>
                        <th>商品名称</th>
                        <th>商品类型</th>
                        <th>价格</th>
                        <th>库存</th>
                        <th>说明</th>
                        <th>上架时间</th>
                        <th>状态</th>
                    </tr>
                    </thead>
                    <tbody>
               <%--     <%for(int i=0;i<data.size();i++){%>--%>

                    <%--------------------------------分页开始代码-----------------------------------%>
                    <%  int pages2 = 1;//当前页
                        String paget2 = request.getParameter("page");//参数页
                        if(paget2 != null){
                            pages2 = Integer.parseInt(paget2);//设置当前页为参数页
                        }
                        int pageNum2 = 5;//每页显示的数量
                        int pageCount2 = data.size()/pageNum2;
                        if(data.size()%pageNum2!=0){
                            pageCount2 = pageCount2 + 1;
                        }
                        if(pages2>pageCount2){
                            pages2=pageCount2;
                        }
                        if(pages2<1){
                            pages2=1;
                        }
                        int startNum2 = (pages2-1)*pageNum2;//页面的起始序号
                        int endNum2 = startNum2+pageNum2;//页面的结束序号
                        if(endNum2>data.size()){
                            endNum2 = data.size();
                        }
                        for(int j=startNum2;j<endNum2;j++){
                    %>

                    <tr>
                        <td class="info"><%=data.get(j).getGoodsId() %></td>
                        <td class="info"><%=data.get(j).getGoodsname() %></td>
                        <td class="info"><%=data.get(j).getGoodstypeByGoodstypeid().getGoodstypename() %></td>
                        <td class="info"><%=data.get(j).getGoodsprice() %></td>
                        <td class="info"><%=data.get(j).getGoodsnum() %></td>
                        <td class="info"><%=data.get(j).getGoodtext() %></td>
                        <td class="info"><%=data.get(j).getGoodsListingTime() %></td>
                        <td class="info">
                            <%if(data.get(j).getGoodsflag()==0){%>
                            审核中
                            <%}else if(data.get(j).getGoodsflag()==1){%>
                            售卖中
                            <%}else if(data.get(j).getGoodsflag()==2){%>
                            已下架
                            <%}else if(data.get(j).getGoodsflag()==3){%>
                            已锁定
                            <%}else if(data.get(j).getGoodsflag()==4){%>
                            审核失败
                            <%}%>
                        </td>
                        <%-- <td class="info"><img style="height: 50px;width: 200px" id="ImageSrc" src="<%=data.get(i).getAdimg() %>"></td>--%>
                    </tr>
                    <% }%>

                    </tbody>
                </table>
                <div style="position: absolute;top: 560px;left: 530px">
                    当前页<%=pages2 %>
                    <a  href = '/ShowGoodsCheck?page=<%=(pages2-1)%>'>上一页</a>
                    <a style="margin-left: 20px" href = '/ShowGoodsCheck?page=<%=(pages2+1)%>'>下一页</a>
                </div>
                <%----------------------------------------分页结束代码-------------------------------------------------%>

<%--
                <tr>
                        <td class="info"><%=data.get(i).getGoodsId() %></td>
                        <td class="info"><%=data.get(i).getGoodsname() %></td>
                        <td class="info"><%=data.get(i).getGoodstypeByGoodstypeid().getGoodstypename() %></td>
                        <td class="info"><%=data.get(i).getGoodsprice() %></td>
                        <td class="info"><%=data.get(i).getGoodsnum() %></td>
                        <td class="info"><%=data.get(i).getGoodtext() %></td>
                        <td class="info"><%=data.get(i).getGoodsListingTime() %></td>
                        <td class="info">
                            <%if(data.get(i).getGoodsflag()==0){%>
                                审核中
                            <%}else if(data.get(i).getGoodsflag()==1){%>
                                售卖中
                            <%}else if(data.get(i).getGoodsflag()==2){%>
                                已下架
                            <%}else if(data.get(i).getGoodsflag()==3){%>
                                已锁定
                            <%}else if(data.get(i).getGoodsflag()==4){%>
                                审核失败
                            <%}%>
                        </td>
                        &lt;%&ndash; <td class="info"><img style="height: 50px;width: 200px" id="ImageSrc" src="<%=data.get(i).getAdimg() %>"></td>&ndash;%&gt;
                    </tr>
                    <%} %>--%>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>
<!-- 修改模态框（Modal） START-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form id="theform" action="/updateAds" method="post">
        <input id="adsid" name="updateadsid" type="hidden" class="form-control" placeholder=""/>
        <input id="adstime" name="updateadstime" type="hidden" class="form-control" placeholder=""/>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="myModalLabel">
                        提示
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>广告名称</label>
                        <input type="text" class="form-control" id="adsname" name="updateadsname" value=""
                               placeholder="请输入名称">
                    </div>
                    <div class="form-group">
                        <label>更改广告图片</label><br>
                        <input type="file" class="form-control" id="src"
                               placeholder="" style="display:none">
                        <input id="adssrc" class="form-control" type="text" name="updateadssrc" required="required"><br>
                        <a class="btn btn-default" onclick="$('input[id=src]').click();">上传</a>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">确定</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal -->
    </form>
</div>
<!-- 修改模态框（Modal）END -->
</body>
<script type="text/javascript">
    function getNumber(id,time){
        $("#adsid").val(id);
        $("#adstime").val(time);
    }
    $('input[id=src]').change(function() {
        $('#adssrc').val($(this).val());
    });
</script>
</body>
</html>

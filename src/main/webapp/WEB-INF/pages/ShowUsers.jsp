<%@ page import="ets.pojo.Users" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: YI
  Date: 5/14/2018
  Time: 10:14 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<% List<Users> data=(List<Users>)session.getAttribute("allusers"); %>
<html>
<head>
    <title>用户管理</title>
    <link rel="stylesheet" href="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="http://cdn.static.runoob.com/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style type="text/css">
        .table th, .table td {
            text-align: center;
            vertical-align: middle!important;
        }
    </style>
</head>
<body>





<div class="container">
    <div class="row clearfix">
        <div class="row clearfix">
            <div class="col-md-12 column">
                <table id="monitor" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>用户ID</th>
                        <th>用户账户</th>
                        <th>真实姓名</th>
                        <th>性别</th>
                        <th>学校班级</th>
                        <th>学号</th>
                        <th>用户邮箱</th>
                        <th>用户地址</th>
                        <th>创建时间</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                 <%--   <%for(int i=0;i<data.size();i++){%>--%>
                <%--------------------------------分页开始代码-----------------------------------%>
                    <%  int pages2 = 1;//当前页
                        String paget2 = request.getParameter("page");//参数页
                        if(paget2 != null){
                            pages2 = Integer.parseInt(paget2);//设置当前页为参数页
                        }
                        int pageNum2 = 5;//每页显示的数量
                        int pageCount2 = data.size()/pageNum2;
                        if(data.size()%pageNum2!=0){
                            pageCount2 = pageCount2 + 1;
                        }
                        if(pages2>pageCount2){
                            pages2=pageCount2;
                        }
                        if(pages2<1){
                            pages2=1;
                        }
                        int startNum2 = (pages2-1)*pageNum2;//页面的起始序号
                        int endNum2 = startNum2+pageNum2;//页面的结束序号
                        if(endNum2>data.size()){
                            endNum2 = data.size();
                        }
                        for(int j=startNum2;j<endNum2;j++){
                    %>
                    <tr>
                        <td class="info" ><%=data.get(j).getUserId() %></td>
                        <td class="info" ><%=data.get(j).getUserName() %></td>
                        <td class="info"><%=data.get(j).getUserturename() %></td>
                        <td class="info" ><%=data.get(j).getUsersex() %></td>
                        <td class="info" ><%=data.get(j).getUserschool() %></td>
                        <td class="info" ><%=data.get(j).getUserStuid() %></td>
                        <td class="info" ><%=data.get(j).getUsermail() %></td>
                        <td class="info" ><%=data.get(j).getUseraddress() %></td>
                        <td class="info" ><%=data.get(j).getUserRegtime() %></td>
                        <td class="info">
                            <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#myModal" onclick="getNumber('<%=data.get(j).getUserId() %>')">冻结</button>
                        </td>
                    </tr>
                    <% }%>

                    </tbody>
                </table>
                <div style="position: absolute;top: 560px;left: 530px">
                    当前页<%=pages2 %>
                    <a  href = '/ShowUsers?page=<%=(pages2-1)%>'>上一页</a>
                    <a style="margin-left: 20px" href = '/ShowUsers?page=<%=(pages2+1)%>'>下一页</a>
                </div>
                <%----------------------------------------分页结束代码-------------------------------------------------%>

                 <%--   <tr>
                        <td class="info" ><%=data.get(i).getUserStuid() %></td>
                        <td class="info" ><%=data.get(i).getUserturename() %></td>
                        <td class="info"><%=data.get(i).getUsersex() %></td>
                        <td class="info" ><%=data.get(i).getUserschool() %></td>
                        <td class="info" ><%=data.get(i).getUsermail() %></td>
                        <td class="info" ><%=data.get(i).getUseraddress() %></td>
                        <td class="info" ><%=data.get(i).getUserRegtime() %></td>
                        <td class="info">
                            <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#myModal" onclick="getNumber('<%=data.get(i).getUserId() %>')">冻结</button>
                        </td>
                    </tr>--%>
                    <%--<%} %>--%>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>
<!-- 冻结模态框（Modal） START-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form id="theform" action="/updateFreezeUser" method="post">
        <input id="usersid" name="freezeuserid" type="hidden" class="form-control" placeholder=""/>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="myModalLabel">
                        提示
                    </h4>
                </div>
                <div class="modal-body">
                    确认该操作吗?
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">确定</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal -->
    </form>
</div>
<!-- 冻结模态框（Modal）END -->
</body>
<script type="text/javascript">
    function getNumber(id){
        $("#usersid").val(id);
    }
</script>
</html>

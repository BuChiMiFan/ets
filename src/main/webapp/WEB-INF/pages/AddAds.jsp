<%--
  Created by IntelliJ IDEA.
  User: YI
  Date: 5/1/2018
  Time: 11:15 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>添加广告</title>
    <link rel="stylesheet" href="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="http://cdn.static.runoob.com/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
        div{
            width: 50%;
            margin: auto;
        }
        input{
            height: 50px;
        }
    </style>
</head>
<body>
<form id="form" role="form" action="/addAds" method="post">
    <div class="form-group">
        <label for="name">广告名称</label>
        <input type="text" class="form-control" id="name" name="adsname" value=""
               placeholder="请输入广告名称">
    </div>
    <div class="form-group">
        <label for="name">上传图片</label><br>
        <input type="file" class="form-control" id="src"
               placeholder="" style="display:none">
        <input id="imgsrc" class="form-control" type="text" name="adssrc"><br>
        <a class="btn btn-default" onclick="$('input[id=src]').click();">上传</a>
    </div>
    <div id="error" class="alert alert-danger alert-dismissable" style="display: none">
        <p>不能为空</p>
    </div>
    <div>
        <button type="submit" class="btn btn-primary">提交</button>
    </div>

</form>
</body>
<script>
    $("form").submit(function(e){
        var name=$("#name").val();
        if(name==""){
            $("#error").css("display","block");
            return false;
        }else{
            return true;
        }
    });
    $('input[id=src]').change(function() {
        $('#imgsrc').val($(this).val());
    });
</script>
</html>

<%--
  Created by IntelliJ IDEA.
  User: YI
  Date: 2018/4/24
  Time: 9:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <link rel="stylesheet" href="../../css/reset.css" />
    <link rel="stylesheet" href="../../css/login.css" />
</head>
<body>
<div class="page">
    <div class="loginwarrp">
        <div class="logo">用户登陆</div>
        <div class="login_form">
            <form id="Login" name="Login" method="post" onsubmit="" action="/login">
                <li class="login-item">
                    <span>用户名：</span>
                    <input type="text" name="uname" class="login_input">
                </li>
                <li class="login-item">
                    <span>密　码：</span>
                    <input type="password" name="upwd" class="login_input">
                </li>
                <li class="login-item verify">
                    <span>身 份：</span>
                    <select id="sfbz" name="sfbz">
                        <option value ="1">普通用户</option>
                        <option value ="2">管理员</option>
                    </select>
                   <%-- <input type="text" name="CheckCode" class="login_input verify_input">--%>
                </li>

                <% String s=(String)session.getAttribute("LoginInfo");%>
                <%if(s==null){

                }else {%>
                <a><%=s%></a>
                <%}%>

                <%--<img src="../../images/verify.PNG" border="0" class="verifyimg" />--%>
                <div class="clearfix"></div>
                <li class="login-sub">
                    <input type="submit" name="Submit" value="登录" />
                </li>
            </form>
        </div>
    </div>
</div>
</body>
</html>
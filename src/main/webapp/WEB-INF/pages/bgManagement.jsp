<%@ page import="ets.pojo.Admin" %><%--
  Created by IntelliJ IDEA.
  User: YI
  Date: 2018/4/24
  Time: 15:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <link rel="stylesheet" href="../../css/reset.css">
    <link rel="stylesheet" href="../../css/public.css">
</head>
<body>
<div class="public-header-warrp">
    <div class="public-header">
        <div class="content">
            <div class="public-header-logo"><a ><i>校</i><h3>二手后台管理</h3></a></div>
            <div class="public-header-admin fr">
           <% Admin  admin =(Admin)session.getAttribute("adminInfo");%>
                <p class="admin-name"><a style="color: #3b4249"><%=admin.getAdminName()%></a>管理员 您好！</p>
                <div class="public-header-fun fr">
                    <a href="/jumpMainPage" class="public-header-loginout">安全退出</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<!-- 内容展示 -->
<div class="public-ifame mt20">
    <div class="content">
        <div class="clearfix"></div>
        <!-- 左侧导航栏 -->
        <div class="public-ifame-leftnav">
            <div class="public-title-warrp">
                <div class="public-ifame-title ">
                    <a href="/jumpMainPage">首页</a>
                </div>
            </div>
            <ul class="left-nav-list">
                <li class="public-ifame-item">
                    <a href="/findAllUser" target="content">用户冻结</a>
                    <div class="ifame-item-sub">
                    </div>
                </li>
                <li class="public-ifame-item">
                    <a href="javascript:;">公告管理</a>
                    <div class="ifame-item-sub">
                        <ul>
                            <li class="active"><a href="jumpAddNotice" target="content">添加</a></li>
                            <li><a href="/findAllGonggao" target="content">修改|删除</a></li>
                        </ul>
                    </div>
                </li>
                <li class="public-ifame-item">
                    <a href="javascript:;">广告管理</a>
                    <div class="ifame-item-sub">
                        <ul>
                            <li><a href="/jumpAddAds" target="content">添加</a></li>
                            <li><a href="/findAllAds" target="content">修改|删除</a></li>
                        </ul>
                    </div>
                </li>
                <li class="public-ifame-item">
                    <a href="javascript:;">类别</a>
                    <div class="ifame-item-sub">
                        <ul>
                            <li><a href="/jumpAddGoodsType" target="content">添加</a></li>
                            <li><a href="/findGoodsType" target="content">修改|删除</a></li>
                        </ul>
                    </div>
                </li>
                <li class="public-ifame-item">
                    <a href="/findAllCheck" target="content">审核管理</a>
                    <div class="ifame-item-sub">
                    </div>
                </li>
                <li class="public-ifame-item">
                    <a href="/jumpShowSell" target="content">销售统计</a>
                    <div class="ifame-item-sub">
                    </div>
                </li>
            </ul>
        </div>
        <!-- 右侧内容展示部分 -->
        <div class="public-ifame-content">
            <iframe name="content" src="/findAllUser" frameborder="0" id="mainframe" scrolling="yes" marginheight="0" marginwidth="0" width="100%" style="height: 700px;"></iframe>
        </div>
    </div>
</div>
<script src="../../js/jquery.min.js"></script>
<script>
    $().ready(function(){
        var item = $(".public-ifame-item");

        for(var i=0; i < item.length; i++){
            $(item[i]).on('click',function(){
                $(".ifame-item-sub").hide();
                if($(this.lastElementChild).css('display') == 'block'){
                    $(this.lastElementChild).hide()
                    $(".ifame-item-sub li").removeClass("active");
                }else{
                    $(this.lastElementChild).show();
                    $(".ifame-item-sub li").on('click',function(){
                        $(".ifame-item-sub li").removeClass("active");
                        $(this).addClass("active");
                    });
                }
            });
        }
    });
</script>
</body>
</html>
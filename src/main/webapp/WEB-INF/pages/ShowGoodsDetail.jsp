<%@ page import="ets.pojo.Goods" %>
<%@ page import="ets.pojo.Goodstype" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: YI
  Date: 5/14/2018
  Time: 3:06 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%Goods goods=(Goods)session.getAttribute("goodsdetails");%>
<html>
<head>
    <title>商品详情</title>
    <link href="../../css/common.css" type="text/css" rel="stylesheet" />
    <link href="../../css/style.css" type="text/css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../js/jquery.SuperSlide.2.1.1.js"></script>
    <script type="text/javascript" src="../../js/jquery.imagezoom.min.js"></script>
    <script type="text/javascript" src="../../js/layer/layer.js"></script>
    <style>
        #h1{
            margin-right: 20px;
            color:#999;
            cursor:pointer
        }
        #h1 .icon_Collection{
            background-image: url(../../images/images_album.png);
            background-repeat: no-repeat;
            background-position: -45px -145px;
        }
        #h1 .icon_Collection.icon{background-position: -60px -145px;}
    </style>
</head>
<body>
<jsp:include page="header.jsp"></jsp:include>
<!--产品详细介绍-->
<div class="content_style clearfix">
    <div class="page_Style">
        <div class="ptc_detailed clearfix">
            <div class="left_pic_ad">
                <div class="pro_detail_img float-lt">
                    <div class="gallery">
                        <div class="tb-booth tb-pic tb-s310"> <a href="../../<%=goods.getGoodsimg()%>"><img src="../../<%=goods.getGoodsimg()%>" alt="展品细节展示放大镜特效" rel="../../<%=goods.getGoodsimg()%>" class="jqzoom" /></a> </div>
                    </div>
                </div>

            </div>
            <!--购买信息-->
            <div class="pic_Purchase_operation">

                <div class="pic_title_name">
                    <h2><%=goods.getGoodsname()%></h2>
                    <h5><%=goods.getGoodtext()%></h5>
                </div>
                <dl class="pic_price putong clearfix"><dt class="label_name">价格</dt><dd class="price content"><em>￥</em><%=goods.getGoodsprice()%>.00</dd></dl>
                <!-- 合作商家  <dl class="Cooperation clearfix">合作商家：简约装饰旗舰店</dl>-->
                <dl class="Deadline clearfix"><dt class="label_name">供货期</dt><dd class="Description content">下单后三天内发货</dd></dl>
                <dl class="Deadline clearfix"><dt class="label_name">上架日期</dt><dd class="Description content"><%=goods.getGoodsListingTime()%></dd></dl>
                <dl class="tb-prop clearfix">
                    <dt class="label_name">类别</dt>
                    <dd class="content">
                        <ul>
                            <li class="tb-selected"><a href="#" role="button" tabindex="0" data-spm-anchor-id=""><span><%=goods.getGoodstypeByGoodstypeid().getGoodstypename()%></span></a><i>已选中</i></li>
                        </ul>
                    </dd>
                </dl>
                <div class="purchasing_btn clearfix">
                    <div class="tb-btn-basket tb-btn-sku ">
                        <a href="#" rel="nofollow" id="J_LinkBasket" role="button"><i class="icon_shop"></i>加入购物车</a>
                    </div>
                </div>
                <dl class="clearfix">
                    <dt class="label_name ">承诺</dt>
                    <dd class="content color" >质量保证</dd>
                </dl>
                </div>
                <!--结束-->
            </div>
        </div>
            <DIV class="right_detailed r_f">
                <div class="slideTxtBox">
                    <div class="hd">
                        <ul><li><em class="jt"></em>用户评论<span class="Quantity">(345)</span></li></ul>
                    </div>
                    <div class="bd">
                        <ul class="comment_style">
                            <li class="comment_list clearfix">
                                <div class="comment_Avatar">
                                    <div class="user_Avatar"><div class="Avatar_bg"></div>
                                        <img src="../../images/touxiang.jpg" width="60" height="60"></div>
                                    <h3>张天师</h3>
                                </div>
                                <div class="comment_info">
                                    <p class="comments">非常不错的手机，做工质感极好，颜值爆表，而且在一众去掉logo基本分不出区别的手机中绝对鹤立鸡群。另外个人觉得后背的moto蝙蝠标识有些不容易看到，如果弄成类似macbook那样发光logo的话，个人愿意多出一千大洋～系统方面确实更加偏向原生android,在易用性方面跟国内一众rom有差距，不过至少对我是绝对够用了。相机启动拍照保存都很快。电池不出众不过快冲非常好用。</p>
                                    <p class="Basic_Information">
                                        <span>件数：1件</span>&nbsp;&nbsp;&nbsp;&nbsp;<span>商品名称：摩托罗拉 Moto Z(XT1650-05) 模块化手机 流金黑 移动联通电信4G手机 双卡双待</span>
                                    </p>
                                </div>
                                <div class="comment_time">2018-5-14</div>
                            </li>
                        </ul>
                    </div>
                </div>
            </DIV>
        </div>
    </div>
</div>
<div class="footer clearfix">
    <div class="footer_spacing clearfix">
        <span class="left_link l_f"><a href="/jumpMainPage">首页</a></span>
        <span class="copyright r_f">copyright©YI</span>
    </div>
</div>
<script type="text/javascript">
    /*$(document).ready(function(){
        $(".jqzoom").imagezoom();
        $("#thumblist li a").mousemove(function(){
            $(this).parents("li").addClass("tb-selected").siblings().removeClass("tb-selected");
            $(".jqzoom").attr('src',$(this).find("img").attr("mid"));
            $(".jqzoom").attr('rel',$(this).find("img").attr("big"));
        });
    });
    jQuery(".picScroll-top").slide({titCell:".hd ul",mainCell:".bd ul",autoPage:true,effect:"top",autoPlay:true,vis:3,trigger:"click"});
    jQuery(".slideTxtBox").slide({trigger:"click"});*/
</script>
</body>
</html>

</body>
</html>

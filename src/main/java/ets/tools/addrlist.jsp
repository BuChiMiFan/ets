<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="${pageContext.request.contextPath }/css/bootstrap.min.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath }/css/bootstrap-table.css" rel="stylesheet">
<title>Insert title here</title>
</head>
<body>
	<table class="table table-hover table-bordered table-condensed table-striped" id="table">
  		
  	</table>
  	<script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery.min.js"></script>
	 <script src="${pageContext.request.contextPath }/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath }/js/bootstrap-table.js"></script>
     <script src="${pageContext.request.contextPath }/js/bootstrap-table-zh-CN.min.js"></script>
     <script type="text/javascript">
     	$(function(){
     		querysome();
     	});
     	 var start="";
         var tb="";
         var tableobject={
         	    
         	    initO:function(){	
         	    	$("#table").bootstrapTable({
         	    		url:"/queryshbyuid",
         	    		queryParams:queryParams,
         	    		pagination:true,
         	    		sidePagination:"server",
         	    		pageSize:3,
         	    		columns:[
         	    		         {
         	    		        	 field:"id",
         	    		        	 title:"编号",
         	    		        	 formatter:function(value,row,index){
         	    		        		 
         	    		        		 return index+1+start;
         	    		        	 }
         	    		        	 
         	    		         },
         	    		         {
         	    		        	 field:"shadd",
         	    		        	 title:"收货地址"
         	    		         },
         	    		         {
         	    		        	
         	    		        	 title:"操作",
         	    		        	
         	    		        	 formatter:function(value,row,index){
         	    		        		 var a="<a id='dele' href='javascript:void(0);'>删除</a>";
         	    		        		 return a;
         	    		        	 },
         	    		        	 events:etevents
         	    		         } 
         	    		         ]
         	    		
         	    		
         	    	});
         	    }
         	    };
         function queryParams(params){
         	start=params.offset;
         	limit=params.limit;
     		var temp={
     				
     				limit:params.limit,
     				 uid: "${user.id}", 
     				 offset:params.offset
     		};
     		return temp;
     	} 
     	 function querysome(){
     		/* $.ajax({
    			url:"${pageContext.request.contextPath }/queryshbyuid",
    			data:"uid=${user.id}",
    			dataType:"json",
    			type:"post",
    			success:function(param){
    				console.log(param);
    			}
    			
    		});  */
     		tableobject.initO();
     	}
     	window.etevents={
     	    	'click #dele':function(events,value,row,index){
     	    		if(confirm("确认删除")){
     	    		 $.ajax({
     	    			url:"${pageContext.request.contextPath}/del",
     	    			data:"id="+row.id,
     	    			dataType:"json",
     	    			type:"post",
     	    			success:function(param){
     	    				if(param.code==200){
     	    					alert("删除成功");
     	    					$("#table").bootstrapTable("refresh");
     	    					return;
     	    				}
     	    				alert("删除失败");
     	    			}
     	    			
     	    		}); 
     	    		}
     	    	}	
     	    };
     	/* function del(value){
     		if(confirm("确认删除")){
     		$.ajax({
    			url:"${pageContext.request.contextPath }/del",
    			data:"id="+value,
    			dataType:"json",
    			type:"post",
    			success:function(param){
    				
    			}
    			
    		});
     		}
     	} */
     </script>
</body>
</html>
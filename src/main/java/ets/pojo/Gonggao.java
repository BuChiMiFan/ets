package ets.pojo;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Timestamp;

/**
 * Created by YI on 2018/5/2.
 */
@Entity
public class Gonggao {
    private int ggId;
    private String ggText;
    private Timestamp ggTime;
    private Integer ggflag;

    @Id
    @Column(name = "ggID")
    public int getGgId() {
        return ggId;
    }

    public void setGgId(int ggId) {
        this.ggId = ggId;
    }

    @Basic
    @Column(name = "ggText")
    public String getGgText() {
        return ggText;
    }

    public void setGgText(String ggText) {
        this.ggText = ggText;
    }

    @Basic
    @Column(name = "ggTime")
    public Timestamp getGgTime() {
        return ggTime;
    }

    public void setGgTime(Timestamp ggTime) {
        this.ggTime = ggTime;
    }

    @Basic
    @Column(name = "ggflag")
    public Integer getGgflag() {
        return ggflag;
    }

    public void setGgflag(Integer ggflag) {
        this.ggflag = ggflag;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Gonggao gonggao = (Gonggao) o;

        if (ggId != gonggao.ggId) return false;
        if (ggText != null ? !ggText.equals(gonggao.ggText) : gonggao.ggText != null) return false;
        if (ggTime != null ? !ggTime.equals(gonggao.ggTime) : gonggao.ggTime != null) return false;
        if (ggflag != null ? !ggflag.equals(gonggao.ggflag) : gonggao.ggflag != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = ggId;
        result = 31 * result + (ggText != null ? ggText.hashCode() : 0);
        result = 31 * result + (ggTime != null ? ggTime.hashCode() : 0);
        result = 31 * result + (ggflag != null ? ggflag.hashCode() : 0);
        return result;
    }
}

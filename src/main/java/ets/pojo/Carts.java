package ets.pojo;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by YI on 2018/4/10.
 */
@Entity
public class Carts {
    private int cartsId;
    private String ordersName;
    private BigDecimal cartsSuMprice;
    private String cartsFlag;
    private Integer ordersId;
    private Integer usersId;
    private String cartsPayT;
    private String cartAttr1;
    private String cartAttr2;
    private String cartAttr3;
    private String cartAttr4;
    private Orders ordersByOrdersId;
    private Users usersByUsersId;

    @Id
    @Column(name = "cartsID")
    public int getCartsId() {
        return cartsId;
    }

    public void setCartsId(int cartsId) {
        this.cartsId = cartsId;
    }

    @Basic
    @Column(name = "ordersName")
    public String getOrdersName() {
        return ordersName;
    }

    public void setOrdersName(String ordersName) {
        this.ordersName = ordersName;
    }

    @Basic
    @Column(name = "cartsSUMprice")
    public BigDecimal getCartsSuMprice() {
        return cartsSuMprice;
    }

    public void setCartsSuMprice(BigDecimal cartsSuMprice) {
        this.cartsSuMprice = cartsSuMprice;
    }

    @Basic
    @Column(name = "cartsFLAG")
    public String getCartsFlag() {
        return cartsFlag;
    }

    public void setCartsFlag(String cartsFlag) {
        this.cartsFlag = cartsFlag;
    }

    @Basic
    @Column(name = "ordersID")
    public Integer getOrdersId() {
        return ordersId;
    }

    public void setOrdersId(Integer ordersId) {
        this.ordersId = ordersId;
    }

    @Basic
    @Column(name = "usersID")
    public Integer getUsersId() {
        return usersId;
    }

    public void setUsersId(Integer usersId) {
        this.usersId = usersId;
    }

    @Basic
    @Column(name = "cartsPayT")
    public String getCartsPayT() {
        return cartsPayT;
    }

    public void setCartsPayT(String cartsPayT) {
        this.cartsPayT = cartsPayT;
    }

    @Basic
    @Column(name = "cartAttr1")
    public String getCartAttr1() {
        return cartAttr1;
    }

    public void setCartAttr1(String cartAttr1) {
        this.cartAttr1 = cartAttr1;
    }

    @Basic
    @Column(name = "cartAttr2")
    public String getCartAttr2() {
        return cartAttr2;
    }

    public void setCartAttr2(String cartAttr2) {
        this.cartAttr2 = cartAttr2;
    }

    @Basic
    @Column(name = "cartAttr3")
    public String getCartAttr3() {
        return cartAttr3;
    }

    public void setCartAttr3(String cartAttr3) {
        this.cartAttr3 = cartAttr3;
    }

    @Basic
    @Column(name = "cartAttr4")
    public String getCartAttr4() {
        return cartAttr4;
    }

    public void setCartAttr4(String cartAttr4) {
        this.cartAttr4 = cartAttr4;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Carts carts = (Carts) o;

        if (cartsId != carts.cartsId) return false;
        if (ordersName != null ? !ordersName.equals(carts.ordersName) : carts.ordersName != null) return false;
        if (cartsSuMprice != null ? !cartsSuMprice.equals(carts.cartsSuMprice) : carts.cartsSuMprice != null)
            return false;
        if (cartsFlag != null ? !cartsFlag.equals(carts.cartsFlag) : carts.cartsFlag != null) return false;
        if (ordersId != null ? !ordersId.equals(carts.ordersId) : carts.ordersId != null) return false;
        if (usersId != null ? !usersId.equals(carts.usersId) : carts.usersId != null) return false;
        if (cartsPayT != null ? !cartsPayT.equals(carts.cartsPayT) : carts.cartsPayT != null) return false;
        if (cartAttr1 != null ? !cartAttr1.equals(carts.cartAttr1) : carts.cartAttr1 != null) return false;
        if (cartAttr2 != null ? !cartAttr2.equals(carts.cartAttr2) : carts.cartAttr2 != null) return false;
        if (cartAttr3 != null ? !cartAttr3.equals(carts.cartAttr3) : carts.cartAttr3 != null) return false;
        if (cartAttr4 != null ? !cartAttr4.equals(carts.cartAttr4) : carts.cartAttr4 != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = cartsId;
        result = 31 * result + (ordersName != null ? ordersName.hashCode() : 0);
        result = 31 * result + (cartsSuMprice != null ? cartsSuMprice.hashCode() : 0);
        result = 31 * result + (cartsFlag != null ? cartsFlag.hashCode() : 0);
        result = 31 * result + (ordersId != null ? ordersId.hashCode() : 0);
        result = 31 * result + (usersId != null ? usersId.hashCode() : 0);
        result = 31 * result + (cartsPayT != null ? cartsPayT.hashCode() : 0);
        result = 31 * result + (cartAttr1 != null ? cartAttr1.hashCode() : 0);
        result = 31 * result + (cartAttr2 != null ? cartAttr2.hashCode() : 0);
        result = 31 * result + (cartAttr3 != null ? cartAttr3.hashCode() : 0);
        result = 31 * result + (cartAttr4 != null ? cartAttr4.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "ordersID", referencedColumnName = "orderid")
    public Orders getOrdersByOrdersId() {
        return ordersByOrdersId;
    }

    public void setOrdersByOrdersId(Orders ordersByOrdersId) {
        this.ordersByOrdersId = ordersByOrdersId;
    }

    @ManyToOne
    @JoinColumn(name = "usersID", referencedColumnName = "userID")
    public Users getUsersByUsersId() {
        return usersByUsersId;
    }

    public void setUsersByUsersId(Users usersByUsersId) {
        this.usersByUsersId = usersByUsersId;
    }
}

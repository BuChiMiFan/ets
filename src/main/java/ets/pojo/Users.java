package ets.pojo;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.*;
import java.util.Collection;

/**
 * Created by YI on 2018/4/10.
 */
@Entity
public class Users {
    private int userId;
    private String userName;
    private String userPwd;
    private Timestamp userRegtime;
    private String usersex;
    private String userschool;
    private String userturename;
    private String usermail;
    private String useraddress;
    private Integer userQq;
    private Integer userStuid;
    private Integer userCredit;
    private Integer userFlag;
    private String userAttr1;
    private String userAttr2;
    private String userAttr3;
    private String userAttr4;
    private Collection<Carts> cartsByUserId;
    private Collection<ets.pojo.Collection> collectionsByUserId;
    private Collection<Goods> goodsByUserId;
    private Collection<Leavemsg> leavemsgsByUserId;
    private Collection<Orders> ordersByUserId;
    private Collection<Reply> repliesByUserId;
    private Collection<Sales> salesByUserId;
    private Collection<Sales> salesByUserId_0;

    @Id
    @Column(name = "userID")
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "userNAME")
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Basic
    @Column(name = "userPWD")
    public String getUserPwd() {
        return userPwd;
    }

    public void setUserPwd(String userPwd) {
        this.userPwd = userPwd;
    }

    @Basic
    @Column(name = "userREGTIME")
    public Timestamp getUserRegtime() {
        return userRegtime;
    }

    public void setUserRegtime(Timestamp userRegtime) {
        this.userRegtime = userRegtime;
    }

    @Basic
    @Column(name = "usersex")
    public String getUsersex() {
        return usersex;
    }

    public void setUsersex(String usersex) {
        this.usersex = usersex;
    }

    @Basic
    @Column(name = "userschool")
    public String getUserschool() {
        return userschool;
    }

    public void setUserschool(String userschool) {
        this.userschool = userschool;
    }

    @Basic
    @Column(name = "userturename")
    public String getUserturename() {
        return userturename;
    }

    public void setUserturename(String userturename) {
        this.userturename = userturename;
    }

    @Basic
    @Column(name = "usermail")
    public String getUsermail() {
        return usermail;
    }

    public void setUsermail(String usermail) {
        this.usermail = usermail;
    }

    @Basic
    @Column(name = "useraddress")
    public String getUseraddress() {
        return useraddress;
    }

    public void setUseraddress(String useraddress) {
        this.useraddress = useraddress;
    }

    @Basic
    @Column(name = "userQQ")
    public Integer getUserQq() {
        return userQq;
    }

    public void setUserQq(Integer userQq) {
        this.userQq = userQq;
    }

    @Basic
    @Column(name = "userSTUID")
    public Integer getUserStuid() {
        return userStuid;
    }

    public void setUserStuid(Integer userStuid) {
        this.userStuid = userStuid;
    }

    @Basic
    @Column(name = "userCREDIT")
    public Integer getUserCredit() {
        return userCredit;
    }

    public void setUserCredit(Integer userCredit) {
        this.userCredit = userCredit;
    }

    @Basic
    @Column(name = "userFlag")
    public Integer getUserFlag() {
        return userFlag;
    }

    public void setUserFlag(Integer userFlag) {
        this.userFlag = userFlag;
    }

    @Basic
    @Column(name = "userAttr1")
    public String getUserAttr1() {
        return userAttr1;
    }

    public void setUserAttr1(String userAttr1) {
        this.userAttr1 = userAttr1;
    }

    @Basic
    @Column(name = "userAttr2")
    public String getUserAttr2() {
        return userAttr2;
    }

    public void setUserAttr2(String userAttr2) {
        this.userAttr2 = userAttr2;
    }

    @Basic
    @Column(name = "userAttr3")
    public String getUserAttr3() {
        return userAttr3;
    }

    public void setUserAttr3(String userAttr3) {
        this.userAttr3 = userAttr3;
    }

    @Basic
    @Column(name = "userAttr4")
    public String getUserAttr4() {
        return userAttr4;
    }

    public void setUserAttr4(String userAttr4) {
        this.userAttr4 = userAttr4;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Users users = (Users) o;

        if (userId != users.userId) return false;
        if (userName != null ? !userName.equals(users.userName) : users.userName != null) return false;
        if (userPwd != null ? !userPwd.equals(users.userPwd) : users.userPwd != null) return false;
        if (userRegtime != null ? !userRegtime.equals(users.userRegtime) : users.userRegtime != null) return false;
        if (usersex != null ? !usersex.equals(users.usersex) : users.usersex != null) return false;
        if (userschool != null ? !userschool.equals(users.userschool) : users.userschool != null) return false;
        if (userturename != null ? !userturename.equals(users.userturename) : users.userturename != null) return false;
        if (usermail != null ? !usermail.equals(users.usermail) : users.usermail != null) return false;
        if (useraddress != null ? !useraddress.equals(users.useraddress) : users.useraddress != null) return false;
        if (userQq != null ? !userQq.equals(users.userQq) : users.userQq != null) return false;
        if (userStuid != null ? !userStuid.equals(users.userStuid) : users.userStuid != null) return false;
        if (userCredit != null ? !userCredit.equals(users.userCredit) : users.userCredit != null) return false;
        if (userFlag != null ? !userFlag.equals(users.userFlag) : users.userFlag != null) return false;
        if (userAttr1 != null ? !userAttr1.equals(users.userAttr1) : users.userAttr1 != null) return false;
        if (userAttr2 != null ? !userAttr2.equals(users.userAttr2) : users.userAttr2 != null) return false;
        if (userAttr3 != null ? !userAttr3.equals(users.userAttr3) : users.userAttr3 != null) return false;
        if (userAttr4 != null ? !userAttr4.equals(users.userAttr4) : users.userAttr4 != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = userId;
        result = 31 * result + (userName != null ? userName.hashCode() : 0);
        result = 31 * result + (userPwd != null ? userPwd.hashCode() : 0);
        result = 31 * result + (userRegtime != null ? userRegtime.hashCode() : 0);
        result = 31 * result + (usersex != null ? usersex.hashCode() : 0);
        result = 31 * result + (userschool != null ? userschool.hashCode() : 0);
        result = 31 * result + (userturename != null ? userturename.hashCode() : 0);
        result = 31 * result + (usermail != null ? usermail.hashCode() : 0);
        result = 31 * result + (useraddress != null ? useraddress.hashCode() : 0);
        result = 31 * result + (userQq != null ? userQq.hashCode() : 0);
        result = 31 * result + (userStuid != null ? userStuid.hashCode() : 0);
        result = 31 * result + (userCredit != null ? userCredit.hashCode() : 0);
        result = 31 * result + (userFlag != null ? userFlag.hashCode() : 0);
        result = 31 * result + (userAttr1 != null ? userAttr1.hashCode() : 0);
        result = 31 * result + (userAttr2 != null ? userAttr2.hashCode() : 0);
        result = 31 * result + (userAttr3 != null ? userAttr3.hashCode() : 0);
        result = 31 * result + (userAttr4 != null ? userAttr4.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "usersByUsersId")
    public Collection<Carts> getCartsByUserId() {
        return cartsByUserId;
    }

    public void setCartsByUserId(Collection<Carts> cartsByUserId) {
        this.cartsByUserId = cartsByUserId;
    }

    @OneToMany(mappedBy = "usersByUserId")
    public Collection<ets.pojo.Collection> getCollectionsByUserId() {
        return collectionsByUserId;
    }

    public void setCollectionsByUserId(Collection<ets.pojo.Collection> collectionsByUserId) {
        this.collectionsByUserId = collectionsByUserId;
    }

    @OneToMany(mappedBy = "usersByUserid")
    public Collection<Goods> getGoodsByUserId() {
        return goodsByUserId;
    }

    public void setGoodsByUserId(Collection<Goods> goodsByUserId) {
        this.goodsByUserId = goodsByUserId;
    }

    @OneToMany(mappedBy = "usersByUserId")
    public Collection<Leavemsg> getLeavemsgsByUserId() {
        return leavemsgsByUserId;
    }

    public void setLeavemsgsByUserId(Collection<Leavemsg> leavemsgsByUserId) {
        this.leavemsgsByUserId = leavemsgsByUserId;
    }

    @OneToMany(mappedBy = "usersByUserid")
    public Collection<Orders> getOrdersByUserId() {
        return ordersByUserId;
    }

    public void setOrdersByUserId(Collection<Orders> ordersByUserId) {
        this.ordersByUserId = ordersByUserId;
    }

    @OneToMany(mappedBy = "usersByUserId")
    public Collection<Reply> getRepliesByUserId() {
        return repliesByUserId;
    }

    public void setRepliesByUserId(Collection<Reply> repliesByUserId) {
        this.repliesByUserId = repliesByUserId;
    }

    @OneToMany(mappedBy = "usersByUserId")
    public Collection<Sales> getSalesByUserId() {
        return salesByUserId;
    }

    public void setSalesByUserId(Collection<Sales> salesByUserId) {
        this.salesByUserId = salesByUserId;
    }

    @OneToMany(mappedBy = "usersByUserId2")
    public Collection<Sales> getSalesByUserId_0() {
        return salesByUserId_0;
    }

    public void setSalesByUserId_0(Collection<Sales> salesByUserId_0) {
        this.salesByUserId_0 = salesByUserId_0;
    }
}

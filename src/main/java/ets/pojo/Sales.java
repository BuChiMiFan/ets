package ets.pojo;

import javax.persistence.*;

/**
 * Created by YI on 2018/4/10.
 */
@Entity
public class Sales {
    private int salesId;
    private Integer goodsId;
    private Integer userId;
    private Integer userId2;
    private Integer salesFlag;
    private String salesAttr1;
    private String salesAttr2;
    private String salesAttr3;
    private String salesAttr4;
    private Goods goodsByGoodsId;
    private Users usersByUserId;
    private Users usersByUserId2;

    @Id
    @Column(name = "salesID")
    public int getSalesId() {
        return salesId;
    }

    public void setSalesId(int salesId) {
        this.salesId = salesId;
    }

    @Basic
    @Column(name = "goodsID")
    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    @Basic
    @Column(name = "userID")
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "userID2")
    public Integer getUserId2() {
        return userId2;
    }

    public void setUserId2(Integer userId2) {
        this.userId2 = userId2;
    }

    @Basic
    @Column(name = "salesFlag")
    public Integer getSalesFlag() {
        return salesFlag;
    }

    public void setSalesFlag(Integer salesFlag) {
        this.salesFlag = salesFlag;
    }

    @Basic
    @Column(name = "salesAttr1")
    public String getSalesAttr1() {
        return salesAttr1;
    }

    public void setSalesAttr1(String salesAttr1) {
        this.salesAttr1 = salesAttr1;
    }

    @Basic
    @Column(name = "salesAttr2")
    public String getSalesAttr2() {
        return salesAttr2;
    }

    public void setSalesAttr2(String salesAttr2) {
        this.salesAttr2 = salesAttr2;
    }

    @Basic
    @Column(name = "salesAttr3")
    public String getSalesAttr3() {
        return salesAttr3;
    }

    public void setSalesAttr3(String salesAttr3) {
        this.salesAttr3 = salesAttr3;
    }

    @Basic
    @Column(name = "salesAttr4")
    public String getSalesAttr4() {
        return salesAttr4;
    }

    public void setSalesAttr4(String salesAttr4) {
        this.salesAttr4 = salesAttr4;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Sales sales = (Sales) o;

        if (salesId != sales.salesId) return false;
        if (goodsId != null ? !goodsId.equals(sales.goodsId) : sales.goodsId != null) return false;
        if (userId != null ? !userId.equals(sales.userId) : sales.userId != null) return false;
        if (userId2 != null ? !userId2.equals(sales.userId2) : sales.userId2 != null) return false;
        if (salesFlag != null ? !salesFlag.equals(sales.salesFlag) : sales.salesFlag != null) return false;
        if (salesAttr1 != null ? !salesAttr1.equals(sales.salesAttr1) : sales.salesAttr1 != null) return false;
        if (salesAttr2 != null ? !salesAttr2.equals(sales.salesAttr2) : sales.salesAttr2 != null) return false;
        if (salesAttr3 != null ? !salesAttr3.equals(sales.salesAttr3) : sales.salesAttr3 != null) return false;
        if (salesAttr4 != null ? !salesAttr4.equals(sales.salesAttr4) : sales.salesAttr4 != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = salesId;
        result = 31 * result + (goodsId != null ? goodsId.hashCode() : 0);
        result = 31 * result + (userId != null ? userId.hashCode() : 0);
        result = 31 * result + (userId2 != null ? userId2.hashCode() : 0);
        result = 31 * result + (salesFlag != null ? salesFlag.hashCode() : 0);
        result = 31 * result + (salesAttr1 != null ? salesAttr1.hashCode() : 0);
        result = 31 * result + (salesAttr2 != null ? salesAttr2.hashCode() : 0);
        result = 31 * result + (salesAttr3 != null ? salesAttr3.hashCode() : 0);
        result = 31 * result + (salesAttr4 != null ? salesAttr4.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "goodsID", referencedColumnName = "goodsID")
    public Goods getGoodsByGoodsId() {
        return goodsByGoodsId;
    }

    public void setGoodsByGoodsId(Goods goodsByGoodsId) {
        this.goodsByGoodsId = goodsByGoodsId;
    }

    @ManyToOne
    @JoinColumn(name = "userID", referencedColumnName = "userID")
    public Users getUsersByUserId() {
        return usersByUserId;
    }

    public void setUsersByUserId(Users usersByUserId) {
        this.usersByUserId = usersByUserId;
    }

    @ManyToOne
    @JoinColumn(name = "userID2", referencedColumnName = "userID")
    public Users getUsersByUserId2() {
        return usersByUserId2;
    }

    public void setUsersByUserId2(Users usersByUserId2) {
        this.usersByUserId2 = usersByUserId2;
    }
}

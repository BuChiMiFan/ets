package ets.pojo;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Timestamp;

/**
 * Created by YI on 2018/5/2.
 */
@Entity
public class Advert {
    private int adId;
    private String adName;
    private Timestamp adTime;
    private String adimg;

    @Id
    @Column(name = "adID")
    public int getAdId() {
        return adId;
    }

    public void setAdId(int adId) {
        this.adId = adId;
    }

    @Basic
    @Column(name = "adName")
    public String getAdName() {
        return adName;
    }

    public void setAdName(String adName) {
        this.adName = adName;
    }

    @Basic
    @Column(name = "adTime")
    public Timestamp getAdTime() {
        return adTime;
    }

    public void setAdTime(Timestamp adTime) {
        this.adTime = adTime;
    }

    @Basic
    @Column(name = "adimg")
    public String getAdimg() {
        return adimg;
    }

    public void setAdimg(String adimg) {
        this.adimg = adimg;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Advert advert = (Advert) o;

        if (adId != advert.adId) return false;
        if (adName != null ? !adName.equals(advert.adName) : advert.adName != null) return false;
        if (adTime != null ? !adTime.equals(advert.adTime) : advert.adTime != null) return false;
        if (adimg != null ? !adimg.equals(advert.adimg) : advert.adimg != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = adId;
        result = 31 * result + (adName != null ? adName.hashCode() : 0);
        result = 31 * result + (adTime != null ? adTime.hashCode() : 0);
        result = 31 * result + (adimg != null ? adimg.hashCode() : 0);
        return result;
    }
}

package ets.pojo;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Timestamp;

/**
 * Created by YI on 2018/4/10.
 */
@Entity
public class Admin {
    private int adminId;
    private String adminName;
    private String adminPwd;
    private Timestamp adminRegtime;
    private String adminAttr1;
    private String adminAttr2;
    private String adminAttr3;
    private String adminAttr4;

    @Id
    @Column(name = "adminID")
    public int getAdminId() {
        return adminId;
    }

    public void setAdminId(int adminId) {
        this.adminId = adminId;
    }

    @Basic
    @Column(name = "adminNAME")
    public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName;
    }

    @Basic
    @Column(name = "adminPWD")
    public String getAdminPwd() {
        return adminPwd;
    }

    public void setAdminPwd(String adminPwd) {
        this.adminPwd = adminPwd;
    }

    @Basic
    @Column(name = "adminREGTIME")
    public Timestamp getAdminRegtime() {
        return adminRegtime;
    }

    public void setAdminRegtime(Timestamp adminRegtime) {
        this.adminRegtime = adminRegtime;
    }

    @Basic
    @Column(name = "adminAttr1")
    public String getAdminAttr1() {
        return adminAttr1;
    }

    public void setAdminAttr1(String adminAttr1) {
        this.adminAttr1 = adminAttr1;
    }

    @Basic
    @Column(name = "adminAttr2")
    public String getAdminAttr2() {
        return adminAttr2;
    }

    public void setAdminAttr2(String adminAttr2) {
        this.adminAttr2 = adminAttr2;
    }

    @Basic
    @Column(name = "adminAttr3")
    public String getAdminAttr3() {
        return adminAttr3;
    }

    public void setAdminAttr3(String adminAttr3) {
        this.adminAttr3 = adminAttr3;
    }

    @Basic
    @Column(name = "adminAttr4")
    public String getAdminAttr4() {
        return adminAttr4;
    }

    public void setAdminAttr4(String adminAttr4) {
        this.adminAttr4 = adminAttr4;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Admin admin = (Admin) o;

        if (adminId != admin.adminId) return false;
        if (adminName != null ? !adminName.equals(admin.adminName) : admin.adminName != null) return false;
        if (adminPwd != null ? !adminPwd.equals(admin.adminPwd) : admin.adminPwd != null) return false;
        if (adminRegtime != null ? !adminRegtime.equals(admin.adminRegtime) : admin.adminRegtime != null) return false;
        if (adminAttr1 != null ? !adminAttr1.equals(admin.adminAttr1) : admin.adminAttr1 != null) return false;
        if (adminAttr2 != null ? !adminAttr2.equals(admin.adminAttr2) : admin.adminAttr2 != null) return false;
        if (adminAttr3 != null ? !adminAttr3.equals(admin.adminAttr3) : admin.adminAttr3 != null) return false;
        if (adminAttr4 != null ? !adminAttr4.equals(admin.adminAttr4) : admin.adminAttr4 != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = adminId;
        result = 31 * result + (adminName != null ? adminName.hashCode() : 0);
        result = 31 * result + (adminPwd != null ? adminPwd.hashCode() : 0);
        result = 31 * result + (adminRegtime != null ? adminRegtime.hashCode() : 0);
        result = 31 * result + (adminAttr1 != null ? adminAttr1.hashCode() : 0);
        result = 31 * result + (adminAttr2 != null ? adminAttr2.hashCode() : 0);
        result = 31 * result + (adminAttr3 != null ? adminAttr3.hashCode() : 0);
        result = 31 * result + (adminAttr4 != null ? adminAttr4.hashCode() : 0);
        return result;
    }
}

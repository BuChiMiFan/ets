package ets.pojo;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by YI on 2018/4/10.
 */
@Entity
public class Leavemsg {
    private int leaveMsgId;
    private Integer userId;
    private String leaveMsgText;
    private Integer replyId;
    private Timestamp leaveMsgTime;
    private Integer leaveMsgFlag;
    private String leaveMsgAttr1;
    private String leaveMsgAttr2;
    private String leaveMsgAttr3;
    private String leaveMsgAttr4;
    private Users usersByUserId;
    private Reply replyByReplyId;

    @Id
    @Column(name = "leaveMsgID")
    public int getLeaveMsgId() {
        return leaveMsgId;
    }

    public void setLeaveMsgId(int leaveMsgId) {
        this.leaveMsgId = leaveMsgId;
    }

    @Basic
    @Column(name = "UserID")
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "leaveMsgTEXT")
    public String getLeaveMsgText() {
        return leaveMsgText;
    }

    public void setLeaveMsgText(String leaveMsgText) {
        this.leaveMsgText = leaveMsgText;
    }

    @Basic
    @Column(name = "replyID")
    public Integer getReplyId() {
        return replyId;
    }

    public void setReplyId(Integer replyId) {
        this.replyId = replyId;
    }

    @Basic
    @Column(name = "leaveMsgTime")
    public Timestamp getLeaveMsgTime() {
        return leaveMsgTime;
    }

    public void setLeaveMsgTime(Timestamp leaveMsgTime) {
        this.leaveMsgTime = leaveMsgTime;
    }

    @Basic
    @Column(name = "leaveMsgFlag")
    public Integer getLeaveMsgFlag() {
        return leaveMsgFlag;
    }

    public void setLeaveMsgFlag(Integer leaveMsgFlag) {
        this.leaveMsgFlag = leaveMsgFlag;
    }

    @Basic
    @Column(name = "leaveMsgAttr1")
    public String getLeaveMsgAttr1() {
        return leaveMsgAttr1;
    }

    public void setLeaveMsgAttr1(String leaveMsgAttr1) {
        this.leaveMsgAttr1 = leaveMsgAttr1;
    }

    @Basic
    @Column(name = "leaveMsgAttr2")
    public String getLeaveMsgAttr2() {
        return leaveMsgAttr2;
    }

    public void setLeaveMsgAttr2(String leaveMsgAttr2) {
        this.leaveMsgAttr2 = leaveMsgAttr2;
    }

    @Basic
    @Column(name = "leaveMsgAttr3")
    public String getLeaveMsgAttr3() {
        return leaveMsgAttr3;
    }

    public void setLeaveMsgAttr3(String leaveMsgAttr3) {
        this.leaveMsgAttr3 = leaveMsgAttr3;
    }

    @Basic
    @Column(name = "leaveMsgAttr4")
    public String getLeaveMsgAttr4() {
        return leaveMsgAttr4;
    }

    public void setLeaveMsgAttr4(String leaveMsgAttr4) {
        this.leaveMsgAttr4 = leaveMsgAttr4;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Leavemsg leavemsg = (Leavemsg) o;

        if (leaveMsgId != leavemsg.leaveMsgId) return false;
        if (userId != null ? !userId.equals(leavemsg.userId) : leavemsg.userId != null) return false;
        if (leaveMsgText != null ? !leaveMsgText.equals(leavemsg.leaveMsgText) : leavemsg.leaveMsgText != null)
            return false;
        if (replyId != null ? !replyId.equals(leavemsg.replyId) : leavemsg.replyId != null) return false;
        if (leaveMsgTime != null ? !leaveMsgTime.equals(leavemsg.leaveMsgTime) : leavemsg.leaveMsgTime != null)
            return false;
        if (leaveMsgFlag != null ? !leaveMsgFlag.equals(leavemsg.leaveMsgFlag) : leavemsg.leaveMsgFlag != null)
            return false;
        if (leaveMsgAttr1 != null ? !leaveMsgAttr1.equals(leavemsg.leaveMsgAttr1) : leavemsg.leaveMsgAttr1 != null)
            return false;
        if (leaveMsgAttr2 != null ? !leaveMsgAttr2.equals(leavemsg.leaveMsgAttr2) : leavemsg.leaveMsgAttr2 != null)
            return false;
        if (leaveMsgAttr3 != null ? !leaveMsgAttr3.equals(leavemsg.leaveMsgAttr3) : leavemsg.leaveMsgAttr3 != null)
            return false;
        if (leaveMsgAttr4 != null ? !leaveMsgAttr4.equals(leavemsg.leaveMsgAttr4) : leavemsg.leaveMsgAttr4 != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = leaveMsgId;
        result = 31 * result + (userId != null ? userId.hashCode() : 0);
        result = 31 * result + (leaveMsgText != null ? leaveMsgText.hashCode() : 0);
        result = 31 * result + (replyId != null ? replyId.hashCode() : 0);
        result = 31 * result + (leaveMsgTime != null ? leaveMsgTime.hashCode() : 0);
        result = 31 * result + (leaveMsgFlag != null ? leaveMsgFlag.hashCode() : 0);
        result = 31 * result + (leaveMsgAttr1 != null ? leaveMsgAttr1.hashCode() : 0);
        result = 31 * result + (leaveMsgAttr2 != null ? leaveMsgAttr2.hashCode() : 0);
        result = 31 * result + (leaveMsgAttr3 != null ? leaveMsgAttr3.hashCode() : 0);
        result = 31 * result + (leaveMsgAttr4 != null ? leaveMsgAttr4.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "UserID", referencedColumnName = "userID")
    public Users getUsersByUserId() {
        return usersByUserId;
    }

    public void setUsersByUserId(Users usersByUserId) {
        this.usersByUserId = usersByUserId;
    }

    @ManyToOne
    @JoinColumn(name = "replyID", referencedColumnName = "replyID")
    public Reply getReplyByReplyId() {
        return replyByReplyId;
    }

    public void setReplyByReplyId(Reply replyByReplyId) {
        this.replyByReplyId = replyByReplyId;
    }
}

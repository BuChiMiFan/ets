package ets.pojo;

import javax.persistence.*;

/**
 * Created by YI on 2018/4/10.
 */
@Entity
public class Collection {
    private int collectId;
    private Integer userId;
    private Integer goodsId;
    private Integer collectFlag;
    private String collectAttr1;
    private String collectAttr2;
    private String collectAttr3;
    private String collectAttr4;
    private Users usersByUserId;
    private Goods goodsByGoodsId;

    @Id
    @Column(name = "collectID")
    public int getCollectId() {
        return collectId;
    }

    public void setCollectId(int collectId) {
        this.collectId = collectId;
    }

    @Basic
    @Column(name = "userID")
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "goodsID")
    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    @Basic
    @Column(name = "collectFLAG")
    public Integer getCollectFlag() {
        return collectFlag;
    }

    public void setCollectFlag(Integer collectFlag) {
        this.collectFlag = collectFlag;
    }

    @Basic
    @Column(name = "collectAttr1")
    public String getCollectAttr1() {
        return collectAttr1;
    }

    public void setCollectAttr1(String collectAttr1) {
        this.collectAttr1 = collectAttr1;
    }

    @Basic
    @Column(name = "collectAttr2")
    public String getCollectAttr2() {
        return collectAttr2;
    }

    public void setCollectAttr2(String collectAttr2) {
        this.collectAttr2 = collectAttr2;
    }

    @Basic
    @Column(name = "collectAttr3")
    public String getCollectAttr3() {
        return collectAttr3;
    }

    public void setCollectAttr3(String collectAttr3) {
        this.collectAttr3 = collectAttr3;
    }

    @Basic
    @Column(name = "collectAttr4")
    public String getCollectAttr4() {
        return collectAttr4;
    }

    public void setCollectAttr4(String collectAttr4) {
        this.collectAttr4 = collectAttr4;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Collection that = (Collection) o;

        if (collectId != that.collectId) return false;
        if (userId != null ? !userId.equals(that.userId) : that.userId != null) return false;
        if (goodsId != null ? !goodsId.equals(that.goodsId) : that.goodsId != null) return false;
        if (collectFlag != null ? !collectFlag.equals(that.collectFlag) : that.collectFlag != null) return false;
        if (collectAttr1 != null ? !collectAttr1.equals(that.collectAttr1) : that.collectAttr1 != null) return false;
        if (collectAttr2 != null ? !collectAttr2.equals(that.collectAttr2) : that.collectAttr2 != null) return false;
        if (collectAttr3 != null ? !collectAttr3.equals(that.collectAttr3) : that.collectAttr3 != null) return false;
        if (collectAttr4 != null ? !collectAttr4.equals(that.collectAttr4) : that.collectAttr4 != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = collectId;
        result = 31 * result + (userId != null ? userId.hashCode() : 0);
        result = 31 * result + (goodsId != null ? goodsId.hashCode() : 0);
        result = 31 * result + (collectFlag != null ? collectFlag.hashCode() : 0);
        result = 31 * result + (collectAttr1 != null ? collectAttr1.hashCode() : 0);
        result = 31 * result + (collectAttr2 != null ? collectAttr2.hashCode() : 0);
        result = 31 * result + (collectAttr3 != null ? collectAttr3.hashCode() : 0);
        result = 31 * result + (collectAttr4 != null ? collectAttr4.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "userID", referencedColumnName = "userID")
    public Users getUsersByUserId() {
        return usersByUserId;
    }

    public void setUsersByUserId(Users usersByUserId) {
        this.usersByUserId = usersByUserId;
    }

    @ManyToOne
    @JoinColumn(name = "goodsID", referencedColumnName = "goodsID")
    public Goods getGoodsByGoodsId() {
        return goodsByGoodsId;
    }

    public void setGoodsByGoodsId(Goods goodsByGoodsId) {
        this.goodsByGoodsId = goodsByGoodsId;
    }
}

package ets.pojo;

import javax.persistence.*;

/**
 * Created by YI on 2018/4/10.
 */
@Entity
public class Goods {
    private int goodsId;
    private String goodsname;
    private Integer goodstypeid;
    private String goodsprice;
    private Integer goodsnum;
    private String goodsListingTime;
    private Integer userid;
    private String goodsimg;
    private String goodtext;
    private String goodLiuyan;
    private Integer goodsflag;
    private String goodsAttr1;
    private String goodsAttr2;
    private String goodsAttr3;
    private String goodsAttr4;
    private java.util.Collection<Collection> collectionsByGoodsId;
    private Goodstype goodstypeByGoodstypeid;
    private Users usersByUserid;
    private java.util.Collection<Orders> ordersByGoodsId;
    private java.util.Collection<Sales> salesByGoodsId;

    @Id
    @Column(name = "goodsID")
    public int getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(int goodsId) {
        this.goodsId = goodsId;
    }

    @Basic
    @Column(name = "goodsname")
    public String getGoodsname() {
        return goodsname;
    }

    public void setGoodsname(String goodsname) {
        this.goodsname = goodsname;
    }

    @Basic
    @Column(name = "goodstypeid")
    public Integer getGoodstypeid() {
        return goodstypeid;
    }

    public void setGoodstypeid(Integer goodstypeid) {
        this.goodstypeid = goodstypeid;
    }

    @Basic
    @Column(name = "goodsprice")
    public String getGoodsprice() {
        return goodsprice;
    }

    public void setGoodsprice(String goodsprice) {
        this.goodsprice = goodsprice;
    }

    @Basic
    @Column(name = "goodsnum")
    public Integer getGoodsnum() {
        return goodsnum;
    }

    public void setGoodsnum(Integer goodsnum) {
        this.goodsnum = goodsnum;
    }

    @Basic
    @Column(name = "goodsListingTime")
    public String getGoodsListingTime() {
        return goodsListingTime;
    }

    public void setGoodsListingTime(String goodsListingTime) {
        this.goodsListingTime = goodsListingTime;
    }

    @Basic
    @Column(name = "userid")
    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    @Basic
    @Column(name = "goodsimg")
    public String getGoodsimg() {
        return goodsimg;
    }

    public void setGoodsimg(String goodsimg) {
        this.goodsimg = goodsimg;
    }

    @Basic
    @Column(name = "goodtext")
    public String getGoodtext() {
        return goodtext;
    }

    public void setGoodtext(String goodtext) {
        this.goodtext = goodtext;
    }

    @Basic
    @Column(name = "goodLiuyan")
    public String getGoodLiuyan() {
        return goodLiuyan;
    }

    public void setGoodLiuyan(String goodLiuyan) {
        this.goodLiuyan = goodLiuyan;
    }

    @Basic
    @Column(name = "goodsflag")
    public Integer getGoodsflag() {
        return goodsflag;
    }

    public void setGoodsflag(Integer goodsflag) {
        this.goodsflag = goodsflag;
    }

    @Basic
    @Column(name = "goodsAttr1")
    public String getGoodsAttr1() {
        return goodsAttr1;
    }

    public void setGoodsAttr1(String goodsAttr1) {
        this.goodsAttr1 = goodsAttr1;
    }

    @Basic
    @Column(name = "goodsAttr2")
    public String getGoodsAttr2() {
        return goodsAttr2;
    }

    public void setGoodsAttr2(String goodsAttr2) {
        this.goodsAttr2 = goodsAttr2;
    }

    @Basic
    @Column(name = "goodsAttr3")
    public String getGoodsAttr3() {
        return goodsAttr3;
    }

    public void setGoodsAttr3(String goodsAttr3) {
        this.goodsAttr3 = goodsAttr3;
    }

    @Basic
    @Column(name = "goodsAttr4")
    public String getGoodsAttr4() {
        return goodsAttr4;
    }

    public void setGoodsAttr4(String goodsAttr4) {
        this.goodsAttr4 = goodsAttr4;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Goods goods = (Goods) o;

        if (goodsId != goods.goodsId) return false;
        if (goodsname != null ? !goodsname.equals(goods.goodsname) : goods.goodsname != null) return false;
        if (goodstypeid != null ? !goodstypeid.equals(goods.goodstypeid) : goods.goodstypeid != null) return false;
        if (goodsprice != null ? !goodsprice.equals(goods.goodsprice) : goods.goodsprice != null) return false;
        if (goodsnum != null ? !goodsnum.equals(goods.goodsnum) : goods.goodsnum != null) return false;
        if (goodsListingTime != null ? !goodsListingTime.equals(goods.goodsListingTime) : goods.goodsListingTime != null)
            return false;
        if (userid != null ? !userid.equals(goods.userid) : goods.userid != null) return false;
        if (goodsimg != null ? !goodsimg.equals(goods.goodsimg) : goods.goodsimg != null) return false;
        if (goodtext != null ? !goodtext.equals(goods.goodtext) : goods.goodtext != null) return false;
        if (goodLiuyan != null ? !goodLiuyan.equals(goods.goodLiuyan) : goods.goodLiuyan != null) return false;
        if (goodsflag != null ? !goodsflag.equals(goods.goodsflag) : goods.goodsflag != null) return false;
        if (goodsAttr1 != null ? !goodsAttr1.equals(goods.goodsAttr1) : goods.goodsAttr1 != null) return false;
        if (goodsAttr2 != null ? !goodsAttr2.equals(goods.goodsAttr2) : goods.goodsAttr2 != null) return false;
        if (goodsAttr3 != null ? !goodsAttr3.equals(goods.goodsAttr3) : goods.goodsAttr3 != null) return false;
        if (goodsAttr4 != null ? !goodsAttr4.equals(goods.goodsAttr4) : goods.goodsAttr4 != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = goodsId;
        result = 31 * result + (goodsname != null ? goodsname.hashCode() : 0);
        result = 31 * result + (goodstypeid != null ? goodstypeid.hashCode() : 0);
        result = 31 * result + (goodsprice != null ? goodsprice.hashCode() : 0);
        result = 31 * result + (goodsnum != null ? goodsnum.hashCode() : 0);
        result = 31 * result + (goodsListingTime != null ? goodsListingTime.hashCode() : 0);
        result = 31 * result + (userid != null ? userid.hashCode() : 0);
        result = 31 * result + (goodsimg != null ? goodsimg.hashCode() : 0);
        result = 31 * result + (goodtext != null ? goodtext.hashCode() : 0);
        result = 31 * result + (goodLiuyan != null ? goodLiuyan.hashCode() : 0);
        result = 31 * result + (goodsflag != null ? goodsflag.hashCode() : 0);
        result = 31 * result + (goodsAttr1 != null ? goodsAttr1.hashCode() : 0);
        result = 31 * result + (goodsAttr2 != null ? goodsAttr2.hashCode() : 0);
        result = 31 * result + (goodsAttr3 != null ? goodsAttr3.hashCode() : 0);
        result = 31 * result + (goodsAttr4 != null ? goodsAttr4.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "goodsByGoodsId")
    public java.util.Collection<Collection> getCollectionsByGoodsId() {
        return collectionsByGoodsId;
    }

    public void setCollectionsByGoodsId(java.util.Collection<Collection> collectionsByGoodsId) {
        this.collectionsByGoodsId = collectionsByGoodsId;
    }

    @ManyToOne
    @JoinColumn(name = "goodstypeid", referencedColumnName = "goodstypeID")
    public Goodstype getGoodstypeByGoodstypeid() {
        return goodstypeByGoodstypeid;
    }

    public void setGoodstypeByGoodstypeid(Goodstype goodstypeByGoodstypeid) {
        this.goodstypeByGoodstypeid = goodstypeByGoodstypeid;
    }

    @ManyToOne
    @JoinColumn(name = "userid", referencedColumnName = "userID")
    public Users getUsersByUserid() {
        return usersByUserid;
    }

    public void setUsersByUserid(Users usersByUserid) {
        this.usersByUserid = usersByUserid;
    }

    @OneToMany(mappedBy = "goodsByGoodsid")
    public java.util.Collection<Orders> getOrdersByGoodsId() {
        return ordersByGoodsId;
    }

    public void setOrdersByGoodsId(java.util.Collection<Orders> ordersByGoodsId) {
        this.ordersByGoodsId = ordersByGoodsId;
    }

    @OneToMany(mappedBy = "goodsByGoodsId")
    public java.util.Collection<Sales> getSalesByGoodsId() {
        return salesByGoodsId;
    }

    public void setSalesByGoodsId(java.util.Collection<Sales> salesByGoodsId) {
        this.salesByGoodsId = salesByGoodsId;
    }
}

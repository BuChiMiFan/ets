package ets.pojo;

import javax.persistence.*;
import java.util.*;
import java.util.Collection;

/**
 * Created by YI on 2018/4/10.
 */
@Entity
public class Goodstype {
    private int goodstypeId;
    private String goodstypename;
    private Integer goodstypeflag;
    private String goodstypeAttr1;
    private String goodstypeAttr2;
    private String goodstypeAttr3;
    private String goodstypeAttr4;
    private Collection<Goods> goodsByGoodstypeId;

    @Id
    @Column(name = "goodstypeID")
    public int getGoodstypeId() {
        return goodstypeId;
    }

    public void setGoodstypeId(int goodstypeId) {
        this.goodstypeId = goodstypeId;
    }

    @Basic
    @Column(name = "goodstypename")
    public String getGoodstypename() {
        return goodstypename;
    }

    public void setGoodstypename(String goodstypename) {
        this.goodstypename = goodstypename;
    }

    @Basic
    @Column(name = "goodstypeflag")
    public Integer getGoodstypeflag() {
        return goodstypeflag;
    }

    public void setGoodstypeflag(Integer goodstypeflag) {
        this.goodstypeflag = goodstypeflag;
    }

    @Basic
    @Column(name = "goodstypeAttr1")
    public String getGoodstypeAttr1() {
        return goodstypeAttr1;
    }

    public void setGoodstypeAttr1(String goodstypeAttr1) {
        this.goodstypeAttr1 = goodstypeAttr1;
    }

    @Basic
    @Column(name = "goodstypeAttr2")
    public String getGoodstypeAttr2() {
        return goodstypeAttr2;
    }

    public void setGoodstypeAttr2(String goodstypeAttr2) {
        this.goodstypeAttr2 = goodstypeAttr2;
    }

    @Basic
    @Column(name = "goodstypeAttr3")
    public String getGoodstypeAttr3() {
        return goodstypeAttr3;
    }

    public void setGoodstypeAttr3(String goodstypeAttr3) {
        this.goodstypeAttr3 = goodstypeAttr3;
    }

    @Basic
    @Column(name = "goodstypeAttr4")
    public String getGoodstypeAttr4() {
        return goodstypeAttr4;
    }

    public void setGoodstypeAttr4(String goodstypeAttr4) {
        this.goodstypeAttr4 = goodstypeAttr4;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Goodstype goodstype = (Goodstype) o;

        if (goodstypeId != goodstype.goodstypeId) return false;
        if (goodstypename != null ? !goodstypename.equals(goodstype.goodstypename) : goodstype.goodstypename != null)
            return false;
        if (goodstypeflag != null ? !goodstypeflag.equals(goodstype.goodstypeflag) : goodstype.goodstypeflag != null)
            return false;
        if (goodstypeAttr1 != null ? !goodstypeAttr1.equals(goodstype.goodstypeAttr1) : goodstype.goodstypeAttr1 != null)
            return false;
        if (goodstypeAttr2 != null ? !goodstypeAttr2.equals(goodstype.goodstypeAttr2) : goodstype.goodstypeAttr2 != null)
            return false;
        if (goodstypeAttr3 != null ? !goodstypeAttr3.equals(goodstype.goodstypeAttr3) : goodstype.goodstypeAttr3 != null)
            return false;
        if (goodstypeAttr4 != null ? !goodstypeAttr4.equals(goodstype.goodstypeAttr4) : goodstype.goodstypeAttr4 != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = goodstypeId;
        result = 31 * result + (goodstypename != null ? goodstypename.hashCode() : 0);
        result = 31 * result + (goodstypeflag != null ? goodstypeflag.hashCode() : 0);
        result = 31 * result + (goodstypeAttr1 != null ? goodstypeAttr1.hashCode() : 0);
        result = 31 * result + (goodstypeAttr2 != null ? goodstypeAttr2.hashCode() : 0);
        result = 31 * result + (goodstypeAttr3 != null ? goodstypeAttr3.hashCode() : 0);
        result = 31 * result + (goodstypeAttr4 != null ? goodstypeAttr4.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "goodstypeByGoodstypeid")
    public Collection<Goods> getGoodsByGoodstypeId() {
        return goodsByGoodstypeId;
    }

    public void setGoodsByGoodstypeId(Collection<Goods> goodsByGoodstypeId) {
        this.goodsByGoodstypeId = goodsByGoodstypeId;
    }
}

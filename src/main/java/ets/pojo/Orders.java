package ets.pojo;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.*;
import java.util.Collection;

/**
 * Created by YI on 2018/4/10.
 */
@Entity
public class Orders {
    private int orderid;
    private String ordername;
    private Integer goodsid;
    private Integer userid;
    private Integer orderSuMprice;
    private Timestamp orderTime;
    private String orderLiuyan;
    private String orderPayT;
    private Integer orderFlag;
    private String orderAttr1;
    private String orderAttr2;
    private String orderAttr3;
    private String orderAttr4;
    private Collection<Carts> cartsByOrderid;
    private Goods goodsByGoodsid;
    private Users usersByUserid;

    @Id
    @Column(name = "orderid")
    public int getOrderid() {
        return orderid;
    }

    public void setOrderid(int orderid) {
        this.orderid = orderid;
    }

    @Basic
    @Column(name = "ordername")
    public String getOrdername() {
        return ordername;
    }

    public void setOrdername(String ordername) {
        this.ordername = ordername;
    }

    @Basic
    @Column(name = "goodsid")
    public Integer getGoodsid() {
        return goodsid;
    }

    public void setGoodsid(Integer goodsid) {
        this.goodsid = goodsid;
    }

    @Basic
    @Column(name = "userid")
    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    @Basic
    @Column(name = "orderSUMprice")
    public Integer getOrderSuMprice() {
        return orderSuMprice;
    }

    public void setOrderSuMprice(Integer orderSuMprice) {
        this.orderSuMprice = orderSuMprice;
    }

    @Basic
    @Column(name = "orderTIME")
    public Timestamp getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Timestamp orderTime) {
        this.orderTime = orderTime;
    }

    @Basic
    @Column(name = "orderLiuyan")
    public String getOrderLiuyan() {
        return orderLiuyan;
    }

    public void setOrderLiuyan(String orderLiuyan) {
        this.orderLiuyan = orderLiuyan;
    }

    @Basic
    @Column(name = "orderPayT")
    public String getOrderPayT() {
        return orderPayT;
    }

    public void setOrderPayT(String orderPayT) {
        this.orderPayT = orderPayT;
    }

    @Basic
    @Column(name = "orderFlag")
    public Integer getOrderFlag() {
        return orderFlag;
    }

    public void setOrderFlag(Integer orderFlag) {
        this.orderFlag = orderFlag;
    }

    @Basic
    @Column(name = "orderAttr1")
    public String getOrderAttr1() {
        return orderAttr1;
    }

    public void setOrderAttr1(String orderAttr1) {
        this.orderAttr1 = orderAttr1;
    }

    @Basic
    @Column(name = "orderAttr2")
    public String getOrderAttr2() {
        return orderAttr2;
    }

    public void setOrderAttr2(String orderAttr2) {
        this.orderAttr2 = orderAttr2;
    }

    @Basic
    @Column(name = "orderAttr3")
    public String getOrderAttr3() {
        return orderAttr3;
    }

    public void setOrderAttr3(String orderAttr3) {
        this.orderAttr3 = orderAttr3;
    }

    @Basic
    @Column(name = "orderAttr4")
    public String getOrderAttr4() {
        return orderAttr4;
    }

    public void setOrderAttr4(String orderAttr4) {
        this.orderAttr4 = orderAttr4;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Orders orders = (Orders) o;

        if (orderid != orders.orderid) return false;
        if (ordername != null ? !ordername.equals(orders.ordername) : orders.ordername != null) return false;
        if (goodsid != null ? !goodsid.equals(orders.goodsid) : orders.goodsid != null) return false;
        if (userid != null ? !userid.equals(orders.userid) : orders.userid != null) return false;
        if (orderSuMprice != null ? !orderSuMprice.equals(orders.orderSuMprice) : orders.orderSuMprice != null)
            return false;
        if (orderTime != null ? !orderTime.equals(orders.orderTime) : orders.orderTime != null) return false;
        if (orderLiuyan != null ? !orderLiuyan.equals(orders.orderLiuyan) : orders.orderLiuyan != null) return false;
        if (orderPayT != null ? !orderPayT.equals(orders.orderPayT) : orders.orderPayT != null) return false;
        if (orderFlag != null ? !orderFlag.equals(orders.orderFlag) : orders.orderFlag != null) return false;
        if (orderAttr1 != null ? !orderAttr1.equals(orders.orderAttr1) : orders.orderAttr1 != null) return false;
        if (orderAttr2 != null ? !orderAttr2.equals(orders.orderAttr2) : orders.orderAttr2 != null) return false;
        if (orderAttr3 != null ? !orderAttr3.equals(orders.orderAttr3) : orders.orderAttr3 != null) return false;
        if (orderAttr4 != null ? !orderAttr4.equals(orders.orderAttr4) : orders.orderAttr4 != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = orderid;
        result = 31 * result + (ordername != null ? ordername.hashCode() : 0);
        result = 31 * result + (goodsid != null ? goodsid.hashCode() : 0);
        result = 31 * result + (userid != null ? userid.hashCode() : 0);
        result = 31 * result + (orderSuMprice != null ? orderSuMprice.hashCode() : 0);
        result = 31 * result + (orderTime != null ? orderTime.hashCode() : 0);
        result = 31 * result + (orderLiuyan != null ? orderLiuyan.hashCode() : 0);
        result = 31 * result + (orderPayT != null ? orderPayT.hashCode() : 0);
        result = 31 * result + (orderFlag != null ? orderFlag.hashCode() : 0);
        result = 31 * result + (orderAttr1 != null ? orderAttr1.hashCode() : 0);
        result = 31 * result + (orderAttr2 != null ? orderAttr2.hashCode() : 0);
        result = 31 * result + (orderAttr3 != null ? orderAttr3.hashCode() : 0);
        result = 31 * result + (orderAttr4 != null ? orderAttr4.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "ordersByOrdersId")
    public Collection<Carts> getCartsByOrderid() {
        return cartsByOrderid;
    }

    public void setCartsByOrderid(Collection<Carts> cartsByOrderid) {
        this.cartsByOrderid = cartsByOrderid;
    }

    @ManyToOne
    @JoinColumn(name = "goodsid", referencedColumnName = "goodsID")
    public Goods getGoodsByGoodsid() {
        return goodsByGoodsid;
    }

    public void setGoodsByGoodsid(Goods goodsByGoodsid) {
        this.goodsByGoodsid = goodsByGoodsid;
    }

    @ManyToOne
    @JoinColumn(name = "userid", referencedColumnName = "userID")
    public Users getUsersByUserid() {
        return usersByUserid;
    }

    public void setUsersByUserid(Users usersByUserid) {
        this.usersByUserid = usersByUserid;
    }
}

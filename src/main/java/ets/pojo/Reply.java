package ets.pojo;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.*;
import java.util.Collection;

/**
 * Created by YI on 2018/4/10.
 */
@Entity
public class Reply {
    private int replyId;
    private String replyText;
    private Integer userId;
    private Timestamp replyTime;
    private String replyflag;
    private String replayAttr1;
    private String replayAttr2;
    private String replayAttr3;
    private String replayAttr4;
    private Collection<Leavemsg> leavemsgsByReplyId;
    private Users usersByUserId;

    @Id
    @Column(name = "replyID")
    public int getReplyId() {
        return replyId;
    }

    public void setReplyId(int replyId) {
        this.replyId = replyId;
    }

    @Basic
    @Column(name = "replyText")
    public String getReplyText() {
        return replyText;
    }

    public void setReplyText(String replyText) {
        this.replyText = replyText;
    }

    @Basic
    @Column(name = "userID")
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "replyTime")
    public Timestamp getReplyTime() {
        return replyTime;
    }

    public void setReplyTime(Timestamp replyTime) {
        this.replyTime = replyTime;
    }

    @Basic
    @Column(name = "replyflag")
    public String getReplyflag() {
        return replyflag;
    }

    public void setReplyflag(String replyflag) {
        this.replyflag = replyflag;
    }

    @Basic
    @Column(name = "replayAttr1")
    public String getReplayAttr1() {
        return replayAttr1;
    }

    public void setReplayAttr1(String replayAttr1) {
        this.replayAttr1 = replayAttr1;
    }

    @Basic
    @Column(name = "replayAttr2")
    public String getReplayAttr2() {
        return replayAttr2;
    }

    public void setReplayAttr2(String replayAttr2) {
        this.replayAttr2 = replayAttr2;
    }

    @Basic
    @Column(name = "replayAttr3")
    public String getReplayAttr3() {
        return replayAttr3;
    }

    public void setReplayAttr3(String replayAttr3) {
        this.replayAttr3 = replayAttr3;
    }

    @Basic
    @Column(name = "replayAttr4")
    public String getReplayAttr4() {
        return replayAttr4;
    }

    public void setReplayAttr4(String replayAttr4) {
        this.replayAttr4 = replayAttr4;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Reply reply = (Reply) o;

        if (replyId != reply.replyId) return false;
        if (replyText != null ? !replyText.equals(reply.replyText) : reply.replyText != null) return false;
        if (userId != null ? !userId.equals(reply.userId) : reply.userId != null) return false;
        if (replyTime != null ? !replyTime.equals(reply.replyTime) : reply.replyTime != null) return false;
        if (replyflag != null ? !replyflag.equals(reply.replyflag) : reply.replyflag != null) return false;
        if (replayAttr1 != null ? !replayAttr1.equals(reply.replayAttr1) : reply.replayAttr1 != null) return false;
        if (replayAttr2 != null ? !replayAttr2.equals(reply.replayAttr2) : reply.replayAttr2 != null) return false;
        if (replayAttr3 != null ? !replayAttr3.equals(reply.replayAttr3) : reply.replayAttr3 != null) return false;
        if (replayAttr4 != null ? !replayAttr4.equals(reply.replayAttr4) : reply.replayAttr4 != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = replyId;
        result = 31 * result + (replyText != null ? replyText.hashCode() : 0);
        result = 31 * result + (userId != null ? userId.hashCode() : 0);
        result = 31 * result + (replyTime != null ? replyTime.hashCode() : 0);
        result = 31 * result + (replyflag != null ? replyflag.hashCode() : 0);
        result = 31 * result + (replayAttr1 != null ? replayAttr1.hashCode() : 0);
        result = 31 * result + (replayAttr2 != null ? replayAttr2.hashCode() : 0);
        result = 31 * result + (replayAttr3 != null ? replayAttr3.hashCode() : 0);
        result = 31 * result + (replayAttr4 != null ? replayAttr4.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "replyByReplyId")
    public Collection<Leavemsg> getLeavemsgsByReplyId() {
        return leavemsgsByReplyId;
    }

    public void setLeavemsgsByReplyId(Collection<Leavemsg> leavemsgsByReplyId) {
        this.leavemsgsByReplyId = leavemsgsByReplyId;
    }

    @ManyToOne
    @JoinColumn(name = "userID", referencedColumnName = "userID")
    public Users getUsersByUserId() {
        return usersByUserId;
    }

    public void setUsersByUserId(Users usersByUserId) {
        this.usersByUserId = usersByUserId;
    }
}

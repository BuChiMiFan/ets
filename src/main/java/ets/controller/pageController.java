package ets.controller;

import ets.pojo.Goods;
import ets.pojo.Users;
import ets.service.pageService;
import ets.tools.Page;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by YI on 2018/5/8.
 */
@Controller
public class pageController {
    @Autowired
    pageService  pageService;
    @ResponseBody
    @RequestMapping(value="/queryshbyuid")
    public Page<Users> queryshbyuid(String uid, String offset, String limit){
        Integer size=Integer.parseInt(limit);
        Integer start=Integer.parseInt(offset);
        Map<String,Object> map=new HashMap<>();
        map.put("offset", start);
        map.put("max", size);
        map.put("uid", uid);
        List<Users> sh=pageService.findCount();

     /*  Integer count=pageService.findCount();*/
        Page<Users> page=new Page<>();
        page.setPageSize(size);
        page.setRows(sh);
    /*    page.setTotal(count);*/
        page.setPageNumber((start+size)/size);
        return page;

    }

}

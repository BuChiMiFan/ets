package ets.controller;

import ets.pojo.Collection;
import ets.pojo.Orders;

import ets.service.BookService;
import ets.service.CartsService;
import ets.service.personService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by YI on 2018/3/29.
 */

        // 订单标识状态： 0 生成订单  1 订单已经支付  2 订单已经发货    3 货物已经到货  4 删除订单
@Controller
public class CartsController {
    @Autowired
    BookService  bookService;
    @Autowired
    CartsService cartsService;
          //订单支付
        @RequestMapping(value = "/OrdersPaid" ,method= RequestMethod.POST)
        public String OrderPaid(@RequestParam("uid")int uid,
        @RequestParam("sumprice")int sumprice, @RequestParam("uuname")String uuname,
        @RequestParam("jinbishengyu")int jinbishengyu,@RequestParam("bookTime")String bookTime, HttpSession session) {

            List<Orders> data= bookService.findOrdersByUserId(uid);//查找用户购买的全部的订单
            for(Orders orders :data ){

                orders.setOrderFlag(1);

                cartsService.updateOrdersss(orders);

            }

            return "success";

    }






}

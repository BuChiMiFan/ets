package ets.controller;
import ets.pojo.Goods;
import ets.pojo.Goodstype;
import ets.service.goodService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.io.*;
import java.util.Calendar;

/**
 * Created by YI on 2018/5/10.
 */

//商品标识状态：  0 商品处于审核  1 商品出售阶段  2 商品处于删除   3商品处于锁定状态 4未通过审核

@Controller
public class goodController {
    @Autowired
    private goodService goodService;

    //上架商品
    @RequestMapping(value = "/addGoods/{adduserid}" ,method= RequestMethod.POST)
    public String addGoods(@RequestParam( "addgoodsname") String name, @RequestParam( "addgoodstypeid") String typeid,
                           @RequestParam( "addgoodsprice") String price, @RequestParam( "addgoodsnum") String number,
                           @PathVariable( "adduserid") String userid, @RequestParam( "addgoodsimg") String src,
                           @RequestParam( "addgoodtext") String text, HttpSession session) {
        Goods goods = new Goods();
        goods.setGoodsname(name);
        goods.setGoodstypeid(Integer.parseInt(typeid));
        goods.setGoodsprice(price);
        goods.setGoodsnum(Integer.parseInt(number));
        goods.setUserid(Integer.parseInt(userid));
        /*****************************开始上传图片*****************************/
        File file=new File(src);
        InputStream in =null;
        String type=src.substring(src.lastIndexOf("."));
        if(type.equals(".jpg")||type.equals(".jpeg")||type.equals(".png")||type.equals(".bmp")) {
            try {
                in = new FileInputStream(file);
                OutputStream out = new FileOutputStream(new File("D:\\AAAbiyekeshe\\ets\\src\\main\\webapp\\images\\product\\" + file.getName()));//要放的路径,改成自己本地服务器路径
                //操作时请注意，不要在相同路径下同时进行读和写，否则文件会被损坏
                byte[] b = new byte[in.available()];
                in.read(b);
                out.write(b);
                in.close();
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            goods.setGoodsimg("images/product/"+file.getName());//为适应不同级别的目录，不加"../../"
        }
        /*****************************结束上传图片*****************************/
        goods.setGoodtext(text);
        Calendar c=Calendar.getInstance();
        String time=Integer.toString(c.get(Calendar.YEAR))+"-"+Integer.toString(c.get(Calendar.MONTH)+1)+"-"+Integer.toString(c.get(Calendar.DATE))
                +" "+Integer.toString(c.get(Calendar.HOUR))+":"+Integer.toString(c.get(Calendar.MINUTE))+":"+Integer.toString(c.get(Calendar.SECOND));
        goods.setGoodsListingTime(time);
        goods.setGoodsflag(0);
        goodService.addGoods(goods);
        return "success";
    }
}

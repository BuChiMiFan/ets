package ets.controller;

import ets.pojo.Goods;
import ets.pojo.Goodstype;
import ets.pojo.Orders;
import ets.pojo.Sales;
import ets.service.BookService;
import ets.service.indexService;
import ets.tools.JsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import org.springframework.stereotype.Controller;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by YI on 2018/5/8.
 */

// 订单标识状态： 0 生成订单  1 订单已经支付  2 订单已经发货    3 货物已经到货  4 删除订单

@Controller
public class bookController {
    @Autowired
    private BookService bookService;

    //生成订单 标识状态： 0 生成订单  1 订单已经支付  2 订单已经发货    3 货物已经到货  4 删除订单
    @RequestMapping(value = "/CreateOrder/{Goodid}/{userId}",method = RequestMethod.POST)
    public String CreateOrder(@PathVariable("Goodid")int Goodid,@PathVariable("userId")int userId, HttpSession session,HttpServletResponse response) throws IOException {

        Orders  orders =new Orders();
        orders.setGoodsid(Goodid);
        orders.setUserid(userId);
        orders.setOrderFlag(0);//未发货状态
        boolean b = bookService.findSingleOrder(userId,Goodid);  //查询是否已存在相同订单
        PrintWriter writer=response.getWriter();
        if(b){
            bookService.saveOrder(orders);
            session.setAttribute("result","succ");
            writer.write("{\"res\":\"succ\"}");
        }else{
            writer.write("{\"res\":\"repeated\"}");
        }
        writer.flush();
        writer.close();
        return "GoodsList";
    }
  //查看所有的订单 标识状态： 0 生成订单  1 订单已经支付  2 订单已经发货    3 货物已经到账  4 删除订单
  @RequestMapping(value = "/SeeAllOrders/{userId}",method = RequestMethod.GET)
  public String SeeAllOrders(@PathVariable("userId")int userId, HttpSession session) {

    List<Orders> data  =bookService.findOrdersByUserId(userId);
    session.setAttribute("SeeAllOrdersdata",data);

    return  "cartsDetail";

  }
  //用户支付所有订单  标识状态： 0 生成订单  1 订单已经支付  2 订单已经发货    3 货物已经到账  4 删除订单
  @RequestMapping(value = "/payAllOrder",method = RequestMethod.POST)
  public String payAllOrder(@RequestParam("userId")int userId, HttpSession session) {
      List<Orders> data  =bookService.findOrdersByUserId(userId);
      return  "success";

  }

  //确认发货
  @RequestMapping(value = "/startedConfirm",method = RequestMethod.POST)
  public String startedConfirm(@RequestParam("updatestartedConfirmOrderId")String orderId, HttpSession session){
      Orders orders=bookService.findSingleOrderbyID(Integer.parseInt(orderId));
      orders.setOrderFlag(2);
      bookService.updateOrder(orders);
      return "success";
  }

  //到货确认（添加销量，下架商品）
  @RequestMapping(value = "/arrivedConfirm/{userid}",method = RequestMethod.POST)
  public String arrivedConfirm(@RequestParam("updatearrivedConfirmOrderId")String orderId, @PathVariable("userid")String userid, HttpSession session){
      Orders orders=bookService.findSingleOrderbyID(Integer.parseInt(orderId));
      Goods goods=orders.getGoodsByGoodsid();
      orders.setOrderFlag(3);
      bookService.updateOrder(orders);
      //下架商品
      goods.setGoodsflag(2);
      bookService.updateAGood(goods);
      //添加销量
      Sales sales=new Sales();
      sales.setGoodsId(goods.getGoodsId());
      sales.setUserId(goods.getUserid());
      sales.setUserId2(Integer.parseInt(userid));
      sales.setSalesFlag(1);
      bookService.saveGoodsSale(sales);
      return "success";
  }

  //删除订单
  //查看所有的订单 标识状态： 0 生成订单  1 订单已经支付  2 订单已经发货    3 货物已经到账  4 删除订单
  @RequestMapping(value = "/delOrders/{oid}",method = RequestMethod.GET)
  public String delOrders(@PathVariable("oid")int oid, HttpSession session) {
      Orders orders  =bookService.findSingleOrderbyID(oid);

      bookService.delOrders(orders);

      List<Orders> data  =bookService.findOrdersByUserId(orders.getUserid());
      session.setAttribute("SeeAllOrdersdata",data);

      return  "cartsDetail";

  }


}

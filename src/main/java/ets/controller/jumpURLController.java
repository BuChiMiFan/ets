package ets.controller;

import ets.pojo.Goods;
import ets.pojo.Goodstype;
import ets.service.BookService;
import ets.service.goodService;
import ets.service.adminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;
import java.util.List;


/**
 * Created by YI on 2018/4/24.
 */
@Controller
public class jumpURLController {
    @Autowired
    private adminService adminService;
    @Autowired
    private goodService goodService;
    @Autowired
    private BookService bookService;

    //跳转主页
    @RequestMapping(value = "/jumpMainPage",method = RequestMethod.GET)
    public String jumpmainPage(){
        return "index_2";
    }

    /*跳转登录页面*/
    @RequestMapping(value = "/userlogin",method = RequestMethod.GET)
    public String userlogin(){
        return "adminlogin";
    }
  
    @RequestMapping(value = "/userReg",method = RequestMethod.GET)
    public String userReg(){
        return "regMember";
    }

    //跳转添加类别
    @RequestMapping(value = "/jumpAddGoodsType",method = RequestMethod.GET)
    public String jumpAddGoodsType(){
        return "AddGoodsType";
    }

    //跳转添加公告
    @RequestMapping(value = "/jumpAddNotice",method = RequestMethod.GET)
    public String jumpAddNotice(){
        return "AddNotice";
    }

    //跳转审核界面
    @RequestMapping(value = "/jumpShowSell",method = RequestMethod.GET)
    public String jumpShowCheck(){
        return "ShowSell";
    }

    //跳转广告添加
    @RequestMapping(value = "/jumpAddAds",method = RequestMethod.GET)
    public String jumpAddAds(){
        return "AddAds";
    }

    //跳转个人信息页面
    @RequestMapping(value = "/jumpShowPersonalInfo",method = RequestMethod.GET)
    public String jumpShowPersonalInfo(){
        return "showPersonalInfo";
    }

    //跳转已购买的商品
    @RequestMapping(value = "/jumpShowUserAllOrders",method = RequestMethod.GET)
    public String jumpShowUserAllOrders(){
        return "showUserAllOrders";
    }

    //跳转自己正在售卖的商品
    @RequestMapping(value = "/jumpShowUserAllOrdersSELL",method = RequestMethod.GET)
    public String jumpShowUserAllOrdersSELL(){
        return "showUserAllOrdersSEll";
    }

    //跳转上架商品
    @RequestMapping(value = "/jumpAddGoods",method = RequestMethod.GET)
    public String jumpAddGoods(HttpSession session){
        List<Goodstype> data=adminService.findGoodsType();
        session.setAttribute("goodstypes",data);
        return "AddGoods";
    }
    //跳转查看所有审核通过的商品
    @RequestMapping(value = "/jumpShowGoods/{userid}",method = RequestMethod.GET)
    public String jumpShowGoods(@PathVariable("userid")String id,HttpSession session){
        List<Goods> data=goodService.findAllGoods(Integer.parseInt(id));
        session.setAttribute("allgoods",data);
        return "ShowGoods";
    }
    //跳转查看所有自己的商品
    @RequestMapping(value = "/jumpShowGoodsCheck/{userid}",method = RequestMethod.GET)
    public String jumpShowGoodsCheck(@PathVariable("userid")String id,HttpSession session){
        List<Goods> data=goodService.findAllGoodsCheck(Integer.parseInt(id));
        session.setAttribute("allgoodscheck",data);
        return "ShowGoodsCheck";
    }

    //跳转到商品详情
    @RequestMapping(value = "/jumpShowGoodsDetail/{goodsdetailid}",method = RequestMethod.GET)
    public String jumpShowGoodsDetail(@PathVariable("goodsdetailid")String goodsdetailid, HttpSession session){
        Goods goods=bookService.findGoodByID(Integer.parseInt(goodsdetailid));
        session.setAttribute("goodsdetails",goods);
        List<Goodstype> types=(List<Goodstype>)adminService.findGoodsType();//类别
        session.setAttribute("showGoodsType",types);
        return "ShowGoodsDetail";
    }

    //跳转个人收藏
    @RequestMapping(value = "/jumpAllCollectionByUid",method = RequestMethod.GET)
    public String jumpAllCollectionByUid(){
        return "showUserCollection";
    }


 /*********************************************************start 分页跳转部分**************************************************************/
    //跳转到人员管理页面
    @RequestMapping(value = "/ShowUsers",method = RequestMethod.GET)
    public String ShowUsers(){
        return "ShowUsers";
    }
    //跳转商品详细
    @RequestMapping(value = "/GoodsList",method = RequestMethod.GET)
    public String GoodsList(){
        return "GoodsList";
    }
    //跳转商品类别(管理)
    @RequestMapping(value = "/ShowGoodsType",method = RequestMethod.GET)
    public String  ShowGoodsType(){
        return "ShowGoodsType";
    }

    //跳转公告(管理)
    @RequestMapping(value = "/ShowNotice",method = RequestMethod.GET)
    public String  ShowNotice(){
        return "ShowNotice";
    }

    //跳转广告(管理)
    @RequestMapping(value = "/ShowAds",method = RequestMethod.GET)
    public String  ShowAds(){
        return "ShowAds";
    }

    //跳转商品信息（个人）
    @RequestMapping(value = "/ShowGoodsCheck",method = RequestMethod.GET)
    public String  ShowGoodsCheck(){
        return "ShowGoodsCheck";
    }

    //跳转审核(管理)
    @RequestMapping(value = "/ShowCheck",method = RequestMethod.GET)
    public String  ShowCheck(){
        return "ShowCheck";
    }


    //跳转个人订单(个人)
    @RequestMapping(value = "/showUserAllOrders",method = RequestMethod.GET)
    public String  showUserAllOrders(){
        return "showUserAllOrders";
    }
    //跳转个人销售（个人）
    @RequestMapping(value = "/showUserAllOrdersSEll",method = RequestMethod.GET)
    public String  showUserAllOrdersSEll(){
        return "showUserAllOrdersSEll";
    }

    //跳转我的上架（个人）
    @RequestMapping(value = "/ShowGoods",method = RequestMethod.GET)
    public String  ShowGoods(){
        return "ShowGoods";
    }

    //跳转个人收藏（个人）
    @RequestMapping(value = "/showUserCollection",method = RequestMethod.GET)
    public String  showUserCollection(){
        return "showUserCollection";
    }
    /*********************************************************start 分页跳转部分**************************************************************/
}

package ets.controller;

import ets.pojo.*;
import ets.service.adminService;
import ets.tools.JsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by YI on 2018/3/27.
 */
@Controller
public class adminController {
    /*管理员和普通用户登录*/
  @Autowired
    private adminService  adminService;
    @RequestMapping(value = "/login" ,method= RequestMethod.POST)
    public String login(@RequestParam( "uname") String uname, @RequestParam("upwd")String upwd, @RequestParam("sfbz")int sfbz, HttpSession session) {
        Admin admin=adminService.findAdmin(uname,upwd);
        Users users=adminService.findUsers(uname,upwd);
        //保存登陆用户的状态信息
        session.setAttribute("adminInfo",admin);
        session.setAttribute("usersInfo",users);
        if (sfbz==2){
          if (admin!=null){
              if (uname.equals(admin.getAdminName())&&upwd.equals(admin.getAdminPwd())){
                  return "bgManagement";
              }else {
                  session.setAttribute("LoginInfo","用户名或者密码错误");
                  return   "adminlogin";
              }
          }else {
              session.setAttribute("LoginInfo","用户名或者密码错误");
              return   "adminlogin";
          }
        }else{
          if (users!=null){
              if (uname.equals(users.getUserName())&&upwd.equals(users.getUserPwd())){
                  return "index_2";
              }else {
                  session.setAttribute("LoginInfo","用户名或者密码错误");
                  return   "adminlogin";
              }
          }else {
              session.setAttribute("LoginInfo","用户名或者密码错误");
              return   "adminlogin";
          }
        }
    }

    /*****************************************************************类别Start*****************************************************************/
    //添加类别
    @RequestMapping(value = "/addGoodsType" ,method= RequestMethod.POST)
    public String addGoodsType(@RequestParam( "goodsname") String name, HttpSession session) {
        Goodstype goodstype=new Goodstype();
        goodstype.setGoodstypename(name);
        goodstype.setGoodstypeflag(1);
        boolean b=adminService.saveGoodsType(goodstype,name);
        if(b){
            return "success";
        }else {
            session.setAttribute("error","gtrepeat");
            return "error";
        }
    }

    //查看类别
    @RequestMapping(value = "/findGoodsType" ,method= RequestMethod.GET)
    public String findGoodsType(HttpSession session) {
        List<Goodstype> data=adminService.findGoodsType();
        session.setAttribute("gsdata",data);
        return "ShowGoodsType";
    }

    //修改类别
    @RequestMapping(value = "/updateGoodsType" ,method= RequestMethod.POST)
    public String updateGoodsType(@RequestParam( "updategtid")String id,@RequestParam( "updategtname")String name,HttpSession session) {
        Goodstype goodstype=new Goodstype();
        goodstype.setGoodstypeId(Integer.parseInt(id));
        goodstype.setGoodstypename(name);
        goodstype.setGoodstypeflag(1);
        adminService.updateGoodsType(goodstype);
        return "ShowGoodsType";
    }

    //删除类别
    @RequestMapping(value = "/delGoodsType" ,method= RequestMethod.POST)
    public String delGoodsType(@RequestParam( "delgtid")String id,@RequestParam( "delgtname")String name,HttpSession session) {
        Goodstype goodstype=new Goodstype();
        goodstype.setGoodstypeId(Integer.parseInt(id));
        goodstype.setGoodstypename(name);
        goodstype.setGoodstypeflag(0);
        adminService.delObject(goodstype);
        return "success";
    }
    /*****************************************************************类别End*****************************************************************/

    /*****************************************************************公告Start*****************************************************************/
    //添加公告
    @RequestMapping(value = "/addNotice" ,method= RequestMethod.POST)
    public String addNotice(@RequestParam( "addnoticecontent")String content,HttpSession session) {
        Gonggao gonggao=new Gonggao();
        gonggao.setGgText(content);
        Calendar c=Calendar.getInstance();
        String year=Integer.toString(c.get(Calendar.YEAR));
        String month=Integer.toString(c.get(Calendar.MONTH)+1);
        String day=Integer.toString(c.get(Calendar.DATE));
        String hour=Integer.toString(c.get(Calendar.HOUR));
        String min=Integer.toString(c.get(Calendar.MINUTE));
        String sec=Integer.toString(c.get(Calendar.SECOND));
        String date=year+"-"+month+"-"+day+" "+hour+":"+min+":"+sec;
        System.out.print(date);
        SimpleDateFormat format =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Timestamp ts = Timestamp.valueOf(date);
        gonggao.setGgTime(ts);
        adminService.saveGonggao(gonggao);
        return "success";
    }

    //修改公告
    @RequestMapping(value = "/updateNotice" ,method= RequestMethod.POST)
    public String updateNotice(@RequestParam( "updateggid")String id,@RequestParam( "updateggtext")String text,@RequestParam( "updateggtime")String time,HttpSession session) {
        Gonggao gonggao=new Gonggao();
        gonggao.setGgId(Integer.parseInt(id));
        gonggao.setGgText(text);
        Timestamp ts = Timestamp.valueOf(time);
        gonggao.setGgTime(ts);
        adminService.updateGonggao(gonggao);
        return "success";
    }

    //查看公告
    @RequestMapping(value = "/findAllGonggao" ,method= RequestMethod.GET)
    public String findAllGonggao(HttpSession session) {
        List<Gonggao> data=adminService.findAllGonggao();
        session.setAttribute("ggdata",data);
        return "ShowNotice";
    }
    /*****************************************************************公告End*****************************************************************/

    /*****************************************************************审核Start*****************************************************************/
    //查看需审核的商品
    @RequestMapping(value = "/findAllCheck" ,method= RequestMethod.GET)
    public String findAllCheck(HttpSession session) {
        List<Goods> data=adminService.findAllCheck();
        session.setAttribute("checkdata",data);
        return "ShowCheck";
    }
    //审核通过或不通过
    @RequestMapping(value = "/updateCheck" ,method= RequestMethod.POST)
    public String updateCheck(@RequestParam( "checkgid")String id,@RequestParam( "checkgname")String name,
                              @RequestParam( "checkguserid")String userid,@RequestParam( "checkgtypeid")String typeid,
                              @RequestParam( "checkgprice")String price,@RequestParam( "checkgtext")String text,
                              @RequestParam( "checkgimg")String img,@RequestParam( "checkgflag")String flag,HttpSession session) {
        Goods goods=new Goods();
        goods.setGoodsId(Integer.parseInt(id));
        goods.setGoodsname(name);
        goods.setUserid(Integer.parseInt(userid));
        goods.setGoodstypeid(Integer.parseInt(typeid));
        goods.setGoodsprice(price);
        goods.setGoodsnum(1);//商品数量为1
        goods.setGoodtext(text);
        goods.setGoodsimg(img);
        goods.setGoodsflag(Integer.parseInt(flag));
        Calendar c=Calendar.getInstance();
        String time=Integer.toString(c.get(Calendar.YEAR))+"-"+Integer.toString(c.get(Calendar.MONTH)+1)+"-"+Integer.toString(c.get(Calendar.DATE))
                +" "+Integer.toString(c.get(Calendar.HOUR))+":"+Integer.toString(c.get(Calendar.MINUTE))+":"+Integer.toString(c.get(Calendar.SECOND));
        goods.setGoodsListingTime(time);
        adminService.updateCheck(goods);
        return "success";
    }
    /*****************************************************************审核End*****************************************************************/

    /*****************************************************************广告Start*****************************************************************/
    //添加广告
    @RequestMapping(value = "/addAds" ,method= RequestMethod.POST)
    public String addAds(@RequestParam( "adsname")String name,@RequestParam( "adssrc")String src,HttpSession session) {
        Advert advert=new Advert();
        advert.setAdName(name);
        Calendar c=Calendar.getInstance();
        String time=Integer.toString(c.get(Calendar.YEAR))+"-"+Integer.toString(c.get(Calendar.MONTH)+1)+"-"+Integer.toString(c.get(Calendar.DATE))
                +" "+Integer.toString(c.get(Calendar.HOUR))+":"+Integer.toString(c.get(Calendar.MINUTE))+":"+Integer.toString(c.get(Calendar.SECOND));
        Timestamp ts = Timestamp.valueOf(time);
        advert.setAdTime(ts);
        /*****************************开始上传图片*****************************/
        File file=new File(src);
        InputStream in =null;
        String type=src.substring(src.lastIndexOf("."));
        if(type.equals(".jpg")||type.equals(".jpeg")||type.equals(".png")||type.equals(".bmp")){
            try {
                in=new FileInputStream(file);
                OutputStream out=new FileOutputStream(new File("D:\\AAAbiyekeshe\\ets\\src\\main\\webapp\\images\\ads\\"+file.getName()));//要放的路径,改成自己本地服务器路径
                //操作时请注意，不要在相同路径下同时进行读和写，否则文件会被损坏
                byte[] b=new byte[in.available()];
                in.read(b);
                out.write(b);
                in.close();
                out.close();
            }catch (IOException e){
                e.printStackTrace();
            }

            /*****************************结束上传图片*****************************/
            advert.setAdimg("images/ads/"+file.getName());
            adminService.addAds(advert);
            return "success";
        }else
            return "error";
    }

    //查看所有广告
    @RequestMapping(value = "/findAllAds" ,method= RequestMethod.GET)
    public String findAllAds(HttpSession session) {
        List<Advert> data=adminService.findAllAdvert();
        session.setAttribute("adsdata",data);
        return "ShowAds";
    }
    //修改广告
    @RequestMapping(value = "/updateAds" ,method= RequestMethod.POST)
    public String updateAds(@RequestParam( "updateadsid")String id,@RequestParam( "updateadsname")String name,
                            @RequestParam( "updateadstime")String time,@RequestParam( "updateadssrc")String src,HttpSession session) {
        Advert advert=new Advert();
        advert.setAdName(name);
        Calendar c=Calendar.getInstance();
        Timestamp ts = Timestamp.valueOf(time);
        advert.setAdTime(ts);
        /*****************************开始上传图片*****************************/
        File file=new File(src);
        InputStream in =null;
        String type=src.substring(src.lastIndexOf("."));
        if(type.equals(".jpg")||type.equals(".jpeg")||type.equals(".png")||type.equals(".bmp")){
            try {
                in=new FileInputStream(file);
                OutputStream out=new FileOutputStream(new File("D:\\AAAbiyekeshe\\ets\\src\\main\\webapp\\images\\ads\\"+file.getName()));//要放的路径,改成自己本地服务器路径
                //操作时请注意，不要在相同路径下同时进行读和写，否则文件会被损坏
                byte[] b=new byte[in.available()];
                in.read(b);
                out.write(b);
                in.close();
                out.close();
            }catch (IOException e){
                e.printStackTrace();
            }
            /*****************************结束上传图片*****************************/
            advert.setAdimg("../../images/ads/"+file.getName());
            adminService.addAds(advert);
            return "success";
        }else
            return "error";
    }
    /*****************************************************************广告End*****************************************************************/

    /*****************************************************************销售记录Start*****************************************************************/
    //获取所有的销售记录
    @RequestMapping(value = "/findAllSales" ,method= RequestMethod.POST)
    public void findAllSales(HttpSession session,HttpServletResponse response) throws IOException{
        List<Goodstype> types=adminService.findGoodsType();
        List<String> type=new ArrayList<>();
        List<Integer> numbers=new ArrayList<>();
        for(int i=0;i<types.size();i++){
            List<Sales> data=adminService.findAllSales(types.get(i).getGoodstypename());
            type.add(types.get(i).getGoodstypename());
            numbers.add(data.size());
        }
        Map<String,Object> map=new HashMap<String,Object>();
        PrintWriter writer=response.getWriter();
        map.put("number",numbers);
        map.put("types",type);
        writer.write(JsonUtil.map2json(map));
        writer.flush();
        writer.close();
    }
    /*****************************************************************销售记录End*****************************************************************/

    /*****************************************************************角色管理Start*****************************************************************/
    //查询所有用户
    @RequestMapping(value = "/findAllUser" ,method= RequestMethod.GET)
    public String findAllUser(HttpSession session,HttpServletResponse response){
        List<Users> data = adminService.findAllUsers();
        session.setAttribute("allusers",data);
        return "ShowUsers";
    }
    //冻结用户
    @RequestMapping(value = "/updateFreezeUser" ,method= RequestMethod.POST)
    public String updateFreezeUser(@RequestParam( "freezeuserid")String id,HttpSession session,HttpServletResponse response){
        Users users=adminService.findUserbyID(Integer.parseInt(id));
        users.setUserFlag(0);
        adminService.updateFreezeUser(users);
        return "success";
    }
    /*****************************************************************角色管理End*****************************************************************/
}

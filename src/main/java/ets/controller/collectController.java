package ets.controller;

import ets.pojo.Collection;
import ets.pojo.Goods;
import ets.service.BookService;
import ets.service.collectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by YI on 2018/5/12.
 */
/*收藏商品  收藏标识状态 0 删除 1 正常
* */
@Controller
public class collectController {
    @Autowired
    private collectService collectService;
    //收藏订单
    @RequestMapping(value = "/collection/{gid}/{uid}")
    public String collection(@PathVariable("gid")int gid , @PathVariable("uid")int uid, HttpSession session) {
        Collection collection=new Collection();



        collection.setGoodsId(gid);
        collection.setUserId(uid);
        collection.setCollectFlag(1);
        collectService.saveCollection(collection);


        return "GoodsList";

    }
   //删除收藏订单
   @RequestMapping(value = "/DElcollection")
   public String DElcollection(@RequestParam("cid")int cid, HttpSession session) {

       Collection collection=collectService.findOneCollectionByCid(cid);
       collection.setCollectFlag(0);
       collectService.updateCollection(collection);

       List<Collection> data3=collectService.findALLCollectionByUid(collection.getUserId());
       session.setAttribute("ALLCollectionByUid",data3);

       return "showUserCollection";

   }
}

package ets.controller;


import ets.pojo.Advert;
import ets.pojo.Goods;
import ets.pojo.Goodstype;
import ets.pojo.Users;
import ets.service.indexService;
import ets.service.adminService;
import ets.tools.JsonUtil;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by YI on 2018/5/1.
 */

//商品标识状态：  0 商品处于审核  1 商品出售阶段  2 商品处于删除   3商品处于锁定状态

@Controller
public class indexController {
    @Autowired
    private indexService indexService;
    @Autowired
    private adminService adminService;

    //展示所有类别的商品
    @RequestMapping(value = "/showGoodsbyType",method = RequestMethod.POST)
    public void showGoodsbyType(@RequestParam("startIndex")int startIndex,@RequestParam("page")int page,HttpSession session, HttpServletResponse response) throws IOException{
        List<Goods> data = (List<Goods>)indexService.findAllGoods(startIndex,page);
        int total = indexService.findAllGoods();
        List<Advert> adverts= (List<Advert>)indexService.findAllAdverts();
        List<Goodstype> types=(List<Goodstype>)adminService.findGoodsType();//类别
        Map<String,Object> map=new HashMap<String,Object>();
        PrintWriter writer=response.getWriter();
        map.put("list",data);
        map.put("type",types);
        map.put("advert",adverts);
        map.put("total",total);
        writer.write(JsonUtil.map2json(map));
        writer.flush();
        writer.close();
    }

    //模糊查询 正在售卖的商品  商品标识状态：  0 商品处于审核  1 商品出售阶段  2 商品处于删除   3商品处于锁定状态
    @RequestMapping(value = "/MOHU",method = RequestMethod.POST)
    public String MOHU(@RequestParam("mohuname")String mohuname, HttpSession session, HttpServletResponse response) {
      //模糊查询的数据写入到session
        List<Goods> data= (List<Goods>)indexService.findGoodMoHu(mohuname);
         session.setAttribute("mohuGoods",data);
     //查询的所有的类别写入到session
        List<Goodstype> data1= (List<Goodstype>)indexService.findALlGoodsType();
        session.setAttribute("showALLGoodsType",data1);
         return "GoodsList";

    }

    //查询数据库里所有的商品分类
    @RequestMapping(value = "/showALLGOODType" ,method = RequestMethod.POST)
    public String showALLGOODType( HttpSession session, HttpServletResponse response) {

        List<Goodstype> data1= (List<Goodstype>)indexService.findALlGoodsType();
        session.setAttribute("showALLGoodsType",data1);

        return "GoodsList";

    }

    //根据类别查询商品ing
    @RequestMapping(value = "/showALLGoodsByType/{TypeID}")
    public String showALLGoodsByType(@PathVariable("TypeID")int TypeId, HttpSession session, HttpServletResponse response) {
       System.out.print("++++++++++++++++++++++++++++++++"+TypeId);
        List<Goods> data1= (List<Goods>)indexService.findGoodsByGoodsType(TypeId);
        session.setAttribute("mohuGoods",data1);
        List<Goodstype> data2= (List<Goodstype>)indexService.findALlGoodsType();
        session.setAttribute("showALLGoodsType",data2);
        return "GoodsList";

    }

}

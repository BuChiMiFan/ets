package ets.controller;

import ets.pojo.Collection;
import ets.pojo.Gonggao;
import ets.pojo.Goods;
import ets.pojo.Orders;
import ets.service.collectService;
import ets.service.personService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

/**
 * Created by YI on 2018/5/10.
 */
@Controller
public class personController {
 @Autowired
      personService  personService;
    @Autowired
      collectService  collectService;

   /* 根据用户的id查询用户的信息
   */
    @RequestMapping(value = "/cxUserInfo/{uid}")
    public String cxUserInfo(@PathVariable("uid")int uid, HttpSession session) {

        List<Orders> data=personService.findOrdersInfoByUID(uid);//查找用户购买的全部的订单
        session.setAttribute("OrdersInfoByUID",data);

        List<Goods> data1=personService.findSellGoodInfoByUID(uid);//查找用户自己售卖的全部订单
        session.setAttribute("SellGoodInfoByUID",data1);

        List<Orders> data2=personService.findSEllGood(uid);//查找自己售卖的商品
        session.setAttribute("SellOrderInfoByUID",data2);

        List<Collection> data3=collectService.findALLCollectionByUid(uid);//查找自己所有的收藏
        session.setAttribute("ALLCollectionByUid",data3);

        Gonggao  data4=personService.findgonggao();
        session.setAttribute("fgonggao",data4);

        return "personalCenter";


    }

}

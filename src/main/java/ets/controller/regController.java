package ets.controller;

import ets.pojo.Users;
import ets.service.regService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

/**
 * Created by YI on 2018/4/25.
 */
@Controller
public class regController {
    @Autowired
    private regService  regService;
    //添加用户
    @RequestMapping(value = "/addUsers",method = RequestMethod.POST)
    public String addCar(@RequestParam("account")String account, @RequestParam("pwd")String pwd,
                          @RequestParam("sex")String sex,            @RequestParam("xuehao")int xuehao,
                          @RequestParam("truename")String truename, @RequestParam("qq")int qq,
                          @RequestParam("clazz")String clazz,     @RequestParam("email")String email,
                          @RequestParam("address")String address,
                 HttpSession session){

        Users users=new Users();
        users.setUserName(account);
        users.setUserPwd(pwd);
        users.setUsersex(sex);
        users.setUserStuid(xuehao);
        users.setUserturename(truename);
        users.setUserQq(qq);
        users.setUserschool(clazz);
       //信用基础分为0分
        users.setUserCredit(0);
        //用户表示0删除 1已经注册
        users.setUserFlag(1);
        users.setUsermail(email);
        users.setUseraddress(address);
        regService.saveUsers(users);

        return "success";
    }

}

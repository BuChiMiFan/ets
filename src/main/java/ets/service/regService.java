package ets.service;

import ets.pojo.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by YI on 2018/4/25.
 */
@Service
public class regService {
    @Autowired
    private HibernateTemplate hibernateTemplate;
    //添加用户
    public boolean saveUsers(Users users){
        hibernateTemplate.save(users) ;
        return true;
    }
    //查询所有用户
    public List<Users> findAllUsers(){
        String hsql="from Users ";
        List<Users>   data=(List<Users>) hibernateTemplate.find(hsql);
        return data;
    }
}

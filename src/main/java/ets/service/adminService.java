package ets.service;

import ets.pojo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by YI on 2018/3/27.
 */
@Service
public class adminService {
    @Autowired
    private HibernateTemplate hibernateTemplate;
    //根据管理员密码进行登录
    public Admin findAdmin(String uname, String upwd){
        String hsql="from Admin s where s.adminName=? and s.adminPwd=?";
        List<Admin> data=(List<Admin>) hibernateTemplate.find(hsql,uname,upwd);
        if (data.size()!=0){
            return data.get(0);
        }else {
            return null;
        }
    }
    //根据用户的账户密码登录
    public Users findUsers(String uname, String upwd){
        String hsql="from Users s where s.userName=? and s.userPwd=?";
        List<Users> data=(List<Users>) hibernateTemplate.find(hsql,uname,upwd);
        if (data.size()!=0){
            return data.get(0);
        }else {
            return null;
        }
    }

    /*****************************************************************类别Start*****************************************************************/
    //添加类别
    public boolean saveGoodsType(Goodstype goodstype, String name){
        String hsql="from Goodstype gt where gt.goodstypeflag=1 and gt.goodstypename=?";
        List<Users> data=(List<Users>) hibernateTemplate.find(hsql,name);
        if(data.size()>0){
            return false;
        }else{
            hibernateTemplate.save(goodstype) ;
            return true;
        }
    }

    //查询类别
    public List<Goodstype> findGoodsType(){
        String hsql="from Goodstype gt where gt.goodstypeflag=1";
        List<Goodstype> data=(List<Goodstype>) hibernateTemplate.find(hsql);
        if (data.size()!=0){
            return data;
        }else {
            return null;
        }
    }

    //修改类别
    public boolean updateGoodsType(Goodstype goodstype){
        hibernateTemplate.update(goodstype);
        return true;
    }
    /*****************************************************************类别End******************************************************************/

    /*****************************************************************公告Start******************************************************************/
    //添加公告
    public boolean saveGonggao(Gonggao gonggao){
        hibernateTemplate.save(gonggao);
        return true;
    }

    //修改公告
    public boolean updateGonggao(Gonggao gonggao){
        hibernateTemplate.update(gonggao);
        return true;
    }

    //获取所有公告
    public List<Gonggao> findAllGonggao(){
        String hsql="from Gonggao gg where gg.ggTime=(select max(gg.ggTime) from Gonggao)";
        List<Gonggao> data=(List<Gonggao>) hibernateTemplate.find(hsql);
        return data;
    }
    /*****************************************************************公告End******************************************************************/

    /*****************************************************************删除Start******************************************************************/
    //各种删除
    public boolean delObject(Object object){
        hibernateTemplate.update(object);
        return true;
    }
    /*****************************************************************删除End******************************************************************/

    /*****************************************************************审核Start******************************************************************/
    //查询所有未审核商品
    public List<Goods> findAllCheck(){
        String hsql="from Goods g where g.goodsflag=0";
        List<Goods> data=(List<Goods>) hibernateTemplate.find(hsql);
        return data;
    }

    //审核通过或不通过
    public boolean updateCheck(Goods goods){
        hibernateTemplate.update(goods);
        return true;
    }
    /*****************************************************************审核End******************************************************************/

    /*****************************************************************广告Start******************************************************************/
    //添加广告
    public boolean addAds(Advert advert){
        hibernateTemplate.save(advert);
        return true;
    }
    //查看所有广告
    public List<Advert> findAllAdvert(){
        String hsql="from Advert ad";
        List<Advert> data=(List<Advert>) hibernateTemplate.find(hsql);
        return data;
    }
    //修改广告
    public boolean updateAds(Advert advert){
        hibernateTemplate.update(advert);
        return true;
    }
    /*****************************************************************广告End******************************************************************/

    /*****************************************************************销售记录Start*****************************************************************/
    //查看所有销售记录
    public List<Sales> findAllSales(String name){
        String hsql="from Sales s where s.goodsId in (select g.goodsId from Goods g where g.goodstypeByGoodstypeid.goodstypename=?)";
        //String hsql="select count(*),s.goodsByGoodsId.goodstypeByGoodstypeid.goodstypename from Sales s group by s.goodsByGoodsId.goodstypeByGoodstypeid.goodstypename";
        List<Sales> data=(List<Sales>) hibernateTemplate.find(hsql,name);
        return data;
    }
    /*****************************************************************销售记录End*****************************************************************/

    /*****************************************************************角色管理Start*****************************************************************/
    //查询所有用户
    public List<Users> findAllUsers(){
        String hsql="from Users u where u.userFlag=1";
        List<Users> data=(List<Users>) hibernateTemplate.find(hsql);
        return data;
    }
    //根据id查询用户
    public Users findUserbyID(int id){
        String hsql="from Users u where u.userId=?";
        List<Users> data=(List<Users>) hibernateTemplate.find(hsql,id);
        return data.get(0);
    }
    //冻结用户
    public boolean updateFreezeUser(Users users){
        hibernateTemplate.update(users);
        return true;
    }
    /*****************************************************************角色管理End*****************************************************************/
}

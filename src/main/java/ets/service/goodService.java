package ets.service;

import ets.pojo.Goods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by YI on 2018/5/10.
 */
@Service
public class goodService {
    @Autowired
    private HibernateTemplate hibernateTemplate;

    //上架商品
    public boolean addGoods(Goods goods){
        hibernateTemplate.save(goods);
        return true;
    }
    //查看所有审核通过的商品
    public List<Goods> findAllGoods(int id){
        String hsql="from Goods g where g.goodsflag=1 and g.userid=?";
        List<Goods> data=(List<Goods>)hibernateTemplate.find(hsql,id);
        return data;
    }
    //查看所有自己的商品
    public List<Goods> findAllGoodsCheck(int id){
        String hsql="from Goods g where g.userid=?";
        List<Goods> data=(List<Goods>)hibernateTemplate.find(hsql,id);
        return data;
    }

}

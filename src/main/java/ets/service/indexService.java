package ets.service;

import ets.pojo.*;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by YI on 2018/5/1.
 */
@Service
public class indexService {
    @Autowired
    private HibernateTemplate hibernateTemplate;
    @Autowired
    private SessionFactory sessionFactory;

    //根据类别查询商品
    public List<Goods>  findGoodsByGoodsType(int goodsTypeID){
        String hsql="from Goods g where g.goodstypeid=? and g.goodsflag=1";
        List<Goods>   data=(List<Goods>) hibernateTemplate.find(hsql,goodsTypeID);

        return data;
    }
   //查询所有类别
    public List<Goodstype>  findALlGoodsType(){
        String Hsql="from Goodstype g where  g.goodstypeflag=1";
        List<Goodstype> data=(List<Goodstype>)hibernateTemplate.find(Hsql);
        return data;
    }
    //模糊查询根据商品的名字查询符合条件的商品

    public  List<Goods> findGoodMoHu(String name){
        String Hsql ="from Goods g  where g.goodsflag=1 and goodsname like '%"+name+"%'";
                List<Goods> Data =(List<Goods>)hibernateTemplate.find(Hsql);
              return  Data;
    }


    //根据商品的id查询某个某个商品的详情
    public  Goods  findgoodsByGoodid(int goodsid){
        String hsql="from  Goods g where g.goodsId=?";
        Goods good =(Goods)hibernateTemplate.find(hsql,goodsid);
        return good;
    }
   //买家下单生成订单
     public Orders orders(Orders orders){
        Orders data =(Orders)hibernateTemplate.save(orders);
        return data;
      }

    //生成购物车
    public Carts carts(Carts carts){
         Carts data =(Carts)hibernateTemplate.save(carts);
         return data;
    }

    //查询所有商品（分页）
    public List<Goods> findAllGoods(int startIndex,int page){
        String hsql="from  Goods g where g.goodsflag=1";
        Session session = sessionFactory.openSession();
        Query query = session.createQuery(hsql);
        query.setFirstResult(startIndex);
        query.setMaxResults(page);
        //List<Goods> data =( List<Goods>)hibernateTemplate.find(hsql);
        return query.list();
    }

    //查询所有商品
    public int findAllGoods(){
        String hsql="from  Goods g where g.goodsflag=1";
        List<Goods> data =( List<Goods>)hibernateTemplate.find(hsql);
        return data.size();
    }

    //查询所有广告
    public List<Advert> findAllAdverts(){
        String hsql="from  Advert ad";
        List<Advert> data =( List<Advert>)hibernateTemplate.find(hsql);
        return data;
    }
}

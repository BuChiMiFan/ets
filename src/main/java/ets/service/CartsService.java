package ets.service;

import ets.pojo.Orders;
import ets.pojo.Users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Service;

/**
 * Created by YI on 2018/3/29.
 */
// 订单标识状态： 0 生成订单  1 订单已经支付  2 订单已经发货    3 货物已经到货  4 删除订单
@Service
public class CartsService {
    @Autowired
    private HibernateTemplate hibernateTemplate;
    //订单已经支付

    public  boolean  updateOrdersss(Orders orders){
       hibernateTemplate.update(orders);
        return true;
    }
    //根据用户的查询他所有的订单

}

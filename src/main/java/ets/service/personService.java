package ets.service;

import ets.pojo.Gonggao;
import ets.pojo.Goods;
import ets.pojo.Orders;
import ets.pojo.Users;

import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 * Created by YI on 2018/5/10.
 */
@Service
// 商品标识状态： 0 商品处于审核  1 商品出售阶段  2 商品处于删除   3商品处于锁定状态 4 未通过审核
// 订单标识状态： 0 生成订单  1 订单已经支付  2 订单已经发货    3 货物已经到账  4 删除订单
public class personService {
    @Autowired
    private HibernateTemplate hibernateTemplate;
    //用户根据用户id查询自己的信息
    public  Users  findUserInfoByID(int id){
        String hsql="from Users u where  u.userId="+id ;
        Users  data=(Users)hibernateTemplate.find(hsql);
        return  data;
    }

    // 查询用户id 用户下面的购买 订单详情  （已支付）
    public List<Orders> findOrdersInfoByUID(int uid){
        String hsql="from Orders o where  o.orderFlag != 0 and o.orderFlag != 4  and  o.userid="+uid;
        List<Orders>  data=(List<Orders>)hibernateTemplate.find(hsql);
        return  data;
    }

    //查询用户id 查询用户自己出售的商品详情
    public List<Goods> findSellGoodInfoByUID(int uid){
        String hsql="from Goods g where  g.goodsflag=1 and g.userid="+uid;
        List<Goods>  data=(List<Goods>)hibernateTemplate.find(hsql);
        return  data;
    }

    //根据商品的id查询购买人的名字
    public List<Orders>  findSEllGood(int uid){
        String hsql="from Orders o where  o.goodsByGoodsid.usersByUserid.userId="+uid+" and o.goodsByGoodsid.goodsflag=1";
        List<Orders> data= (List<Orders>) hibernateTemplate.find(hsql);
       return  data;
    }

    //查询公告
    public  Gonggao  findgonggao(){
       String hsql="from Gonggao g order by g.ggTime desc ";
       List<Gonggao> data =(List<Gonggao>)hibernateTemplate.find(hsql);
        return data.get(0);
    }
}

package ets.service;

import ets.pojo.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by YI on 2018/5/12.
 */
@Service
public class collectService {
    @Autowired
    private HibernateTemplate hibernateTemplate;
    //收藏订单
    public boolean  saveCollection(Collection collection){

        hibernateTemplate.save(collection);

        return  true;
    }
    //查询所有收藏
    public List<Collection>   findAllCollection(){
        String hsql="from  Collection ";
        List<Collection>  data= (List<Collection>) hibernateTemplate.find(hsql);
        return  data;
    }

    //通过收藏的id查询一个收藏
    public Collection   findOneCollectionByCid(int cid){
        String hsql="from Collection c where collectId=?  and  c.collectFlag=1";
        List<Collection> data =(List<Collection>)hibernateTemplate.find(hsql,cid);
        return data.get(0);

    }
    //根据收藏用户查询下面所有的一个用户的收藏
    public  List<Collection>   findALLCollectionByUid(int uid){
        String hsql="from Collection  c where c.collectFlag=1 and c.userId="+uid;
        List<Collection>  data=(List<Collection>)hibernateTemplate.find(hsql);
        return data;
    }
    //根据查询gid和uid查询收藏单是否存在
    public Collection findCollectionByuidgid(int gid,int  uid){
         String hsql="from Collection  c where  c.collectFlag=1 and c.goodsId = "+gid+" and c.userId = "+uid ;
         Collection data=(Collection)hibernateTemplate.find(hsql);
         return data;
    }
    //修改收藏
    public Boolean  updateCollection(Collection c){
        hibernateTemplate.update(c);
        return  true;
    }
}

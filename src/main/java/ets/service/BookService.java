package ets.service;

import ets.pojo.Goods;
import ets.pojo.Orders;
import ets.pojo.Sales;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Service;

import java.security.PublicKey;
import java.util.List;

/**
 * Created by YI on 2018/5/8.
 */
@Service
public class BookService {
    @Autowired
    private HibernateTemplate hibernateTemplate;

    //通过商品的id查询一个商品
    public Goods findGoodByID(int gid){
        String hsql="from Goods g  where g.goodsId ="+gid;
        List<Goods> data = (List<Goods>)hibernateTemplate.find(hsql);
        return  data.get(0);
    }

    //通过用户的id查询用户的所有订单（未支付）
    public List<Orders> findOrdersByUserId(int Uid){
        String hsql="from Orders o  where o.orderFlag=0 and o.userid="+Uid  ;
        List<Orders>  data= (List<Orders>)hibernateTemplate.find(hsql);
        return  data;
    }

    //生成订单Orders
    public Boolean   saveOrder(Orders orders){
        hibernateTemplate.save(orders);
        return true;
    }

    //查询单一订单
    public boolean findSingleOrder(int uid,int goodsid){
        String hsql="from Orders o where o.userid=? and o.goodsid=?";
        List<Orders> data=(List<Orders>)hibernateTemplate.find(hsql,uid,goodsid);
        if (data.size()==0){//不存在该订单，则可以添加到购物车
            return true;
        }else{
            return false;
        }
    }

    //更新订单状态
    public boolean updateOrder(Orders orders){
        hibernateTemplate.update(orders);
        return true;
    }
    //根据ID匹配订单
    public Orders findSingleOrderbyID(int id){
        String hsql="from Orders o where o.orderid=?";
        List<Orders> data=(List<Orders>)hibernateTemplate.find(hsql,id);
        return data.get(0);
    }

    //下架商品
    public boolean updateAGood(Goods goods){
        hibernateTemplate.update(goods);
        return true;
    }

    //添加销量
    public boolean saveGoodsSale(Sales sales){
        hibernateTemplate.save(sales);
        return true;
    }
    //删除订单
    public boolean delOrders(Orders orders){
        hibernateTemplate.delete(orders);
        return true;
    }
}

package ets.service;

import ets.pojo.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by YI on 2018/5/8.
 */
@Service
public class pageService {
    @Autowired
    private HibernateTemplate hibernateTemplate;

    //查询人员的的个数
  public List<Users>  findCount(){
      String hsql = "from Users";
      List<Users>  data= (List<Users>)hibernateTemplate.find(hsql);
      return  data;
  }
}
